package de.maltebehrendt.uppt.junit;

import de.maltebehrendt.uppt.InstanceRunner;
import de.maltebehrendt.uppt.database.Database;
import de.maltebehrendt.uppt.database.impl.ArangoDBImpl;
import de.maltebehrendt.uppt.events.Impl.MessageImpl;
import de.maltebehrendt.uppt.events.Message;
import de.maltebehrendt.uppt.util.EncryptionUtils;
import de.maltebehrendt.uppt.util.FileUtils;
import de.maltebehrendt.uppt.util.ShellUtils;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RunWith(VertxUnitRunner.class)
public abstract class AbstractTest {
    private static Map<String, Map> resourceMap = new HashMap<>();
    protected Vertx vertx = null;
    protected EventBus eventBus = null;
    protected String baseDirectory = null;
    protected String configDirectory = null;
    protected String serviceName = null;
    protected HashMap<String, JsonObject> userSessionMap = new HashMap<>();

    private static String startUpCommand = "echo \"Setting up ArangoDB 3.3.1 in\"\n" +
            "echo $PWD\n" +
            "\n" +
            "if [ ! -d \"ArangoDB-3.3.1\" ]; then\n" +
            "  # download ArangoDB\n" +
            "  echo \"Downloading ArangoDB...\"\n" +
            "  wget https://download.arangodb.com/travisCI/ArangoDB-3.3.1.tar.gz\n" +
            "  echo \"Unpacking ArangoDB...\"\n" +
            "  tar zvxf ArangoDB-3.3.1.tar.gz\n" +
            "  chmod a+x ./ArangoDB-3.3.1/bin/arangod_x86_64\n" +
            "  rm -f ArangoDB-3.3.1.tar.gz\n" +
            "fi\n" +
            "\n" +
            "echo \"Removing previous databases...\"\n" +
            "if [ -d \"databases\" ]; then\n" +
            "    rm -rf ./databases\n" +
            "fi\n" +
            "\n" +
            "echo \"Starting ArangoDB...\"\n" +
            "./ArangoDB-3.3.1/bin/arangod_x86_64 \\\n" +
            "    --database.directory databases \\\n" +
            "    --configuration none \\\n" +
            "    --server.endpoint tcp://127.0.0.1:8529 \\\n" +
            "    --javascript.app-path ./ArangoDB-3.3.1/js/apps \\\n" +
            "    --javascript.startup-directory ./ArangoDB-3.3.1/js \\\n" +
            "    --server.authentication=false &\n" +
            "\n" +
            "sleep 2\n" +
            "\n" +
            "echo \"Checking for arangod process...\"\n" +
            "process=$(ps auxww | grep \"bin/arangod\" | grep -v grep)\n" +
            "\n" +
            "if [ \"x$process\" == \"x\" ]; then\n" +
            "  echo \"no 'arangod' process found\"\n" +
            "  exit 1\n" +
            "fi\n" +
            "\n" +
            "echo \"Waiting until ArangoDB is ready on port 8529\"\n" +
            "while [[ -z `curl -uroot: -s 'http://127.0.0.1:8529/_api/version' ` ]] ; do\n" +
            "  echo -n \".\"\n" +
            "  sleep 2s\n" +
            "done\n" +
            "\n" +
            "PID=$(ps aux | grep \"bin/arangod\" | grep -v grep | awk '{print $2}')\n" +
            "echo \"PID $PID\"\n" +
            "echo \"$PID\" >> arangoDB.pid\n" +
            "\n" +
            "echo \"ArangoDB 3.3.1 is up\"";
    private static String shutdownCommand = "PID=`cat arangoDB.pid`\nkill $PID\nwhile ps -p $PID; do sleep 1;done;\nrm -f arangoDB.pid\nsleep 2s\n";


    // TODO: fix this, teardown is required...
    // static map with all the vars, static setup is called with class name from TestClass
    // before class sets them locally, if null

    public static Map<String, Object> setupTestVertxAndDatabase(String serviceName, TestContext context) {
        Vertx vertx = null;
        EventBus eventBus = null;

        String baseDirectory = null;
        String configDirectory = null;

        Async setupResult = context.async(1);

        // set the base and config directory
        // convention: junit directory equals class name
        baseDirectory = "test" + File.separator + serviceName;
        configDirectory = baseDirectory + File.separator + "config" + File.separator;

        // get available service config files for adding them to the whitelist
        // convention: assume all configured services to be whitelisted
        JsonArray serviceWhitelist = new JsonArray();
        JsonArray serviceLocations = new JsonArray();
        try {
            for (File file : new File(configDirectory + "services").listFiles()) {
                if (!file.getName().endsWith(".json")) {
                    continue;
                }
                JsonObject serviceConfig = new JsonObject(new String(Files.readAllBytes(file.toPath())));
                serviceLocations.add(serviceConfig.getString("serviceLocation"));
                serviceWhitelist.add(file.getName());
            }
        }
        catch(Exception e) {
            context.assertTrue(e == null, "Service config files and database templates are readable: " + e.getMessage());
        }

        // startup a local junit database
        context.assertTrue(startTestDatabase("test", serviceLocations), "Test database is available");

        InstanceRunner.setConfigDirectory(configDirectory);
        vertx = InstanceRunner.vertx(serviceWhitelist, true);
        eventBus = vertx.eventBus();

        // store in Hashmap
        Map<String, Object> testMap = new HashMap<>();
        testMap.put("vertx", vertx);
        testMap.put("eventBus", eventBus);
        testMap.put("baseDirectory", baseDirectory);
        testMap.put("configDirectory", configDirectory);
        testMap.put("serviceLocations", serviceLocations);
        resourceMap.put(serviceName, testMap);

        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.newInstance", message -> {
            // startup complete
            if ("200".equalsIgnoreCase(message.headers().get("statusCode"))) {
                context.assertEquals("200", message.headers().get("statusCode"));
                context.assertNotNull(message.headers().get("correlationID"));
                context.assertNotNull(message.headers().get("origin"));
                setupResult.countDown();
            }
        });

        setupResult.awaitSuccess();
        consumer.unregister();
        return testMap;
    }

    public static void tearDownTestVertxAndCleanupDatabase(String serviceName, TestContext context) {
        Map testMap = resourceMap.get(serviceName);

        if(testMap != null) {
            try {
                Vertx vertx = (Vertx) testMap.get("vertx");
                vertx.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                context.assertTrue(cleanUpTestDatabase((JsonArray) testMap.get("serviceLocations"), null), "Databases for " + serviceName + " were deleted.");
            }
        }
    }

    public static void teardownTestVertxAndShutdownDatabase(String serviceName, TestContext context) {
        Map testMap = resourceMap.get(serviceName);

        if(testMap != null) {
            try {
                Vertx vertx = (Vertx) testMap.get("vertx");
                vertx.close();
                resourceMap.remove(serviceName);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                shutdownTestDatabase("test");
            }
        }
    }

    @Before
    public void ensureVertxAndDatabaseAvailability(TestContext context) {
        // only run if not setup already (ist not @BeforeClass because it is not possible to get the subclass name from a static parent class method)
        if(vertx == null && eventBus == null && baseDirectory == null) {
            String serviceName = castClass().getClass().getSimpleName();
            Map testMap = resourceMap.get(serviceName);
            context.assertNotNull(testMap, "Vertx and database are available. If not, make sure to call 'setupTestVertxAndDatabase' in @BeforeClass");

            this.vertx = (Vertx) testMap.get("vertx");
            this.eventBus = (EventBus) testMap.get("eventBus");
            this.baseDirectory = (String) testMap.get("baseDirectory");
            this.configDirectory = (String) testMap.get("configDirectory");
            this.serviceName = serviceName;
        }
    }

    public MessageConsumer registerConsumer(String domain, String version, String type, Handler<Message> messageHandler) {
        io.vertx.core.eventbus.MessageConsumer consumer = eventBus.consumer(domain + "." + version + "." + type);
        consumer.handler(new Handler<io.vertx.core.eventbus.Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> event) {
                messageHandler.handle(new MessageImpl(event));
            }
        });

        return consumer;
    }

    public String send(String domain, String version, String type, String origin, String correlationID, JsonObject payload) {
        return send(domain, version, type, null, origin, correlationID, payload, null, null);
    }

    public <R> String send(String domain, String version, String type, String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, null, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, String origin,  String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, origin, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, Integer statusCode, String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, statusCode, null, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, null, null, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, Integer statusCode, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, statusCode, null, null, payload, null, replyHandler);
    }
    public <R> String send(String domain, String version, String type, Integer statusCode, String origin, String correlationID, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        if(payload == null) {
            payload = new JsonObject();
        }
        if(options == null) {
            options = new DeliveryOptions();
        }
        if(correlationID == null) {
            correlationID = UUID.randomUUID().toString();
        }
        if(origin == null) {
            origin = serviceName;
        }
        if(statusCode == null) {
            statusCode = 200;
        }
        String address = domain + "." + version + "." + type;
        String cID = correlationID;

        options.addHeader("statusCode", statusCode.toString())
                .addHeader("correlationID", cID)
                .addHeader("origin", origin);

        if(replyHandler != null) {
            eventBus.send(address, payload, options, handler -> {
                if(handler.failed()) {
                    System.err.println("Failed: sending message with correlationID " + cID + " to " + address + ": " + handler.cause());
                    replyHandler.handle(new MessageImpl(cID, handler.cause()));
                }
                else {
                    replyHandler.handle(new MessageImpl(handler.result()));
                }
            });
        }
        else {
            eventBus.send(address, payload, options);
        }
        return cID;
    }

    public <R> String createUserSession(String domain, String version, String type, String pathName, String userID, JsonArray userRoles, Handler<Message<R>> messageHandler, Future<JsonObject> future) {
        if(userID == null) {
            userID = "testUser";
        }
        if(userRoles == null) {
            userRoles = new JsonArray();
        }
        if(messageHandler == null) {
            messageHandler = event -> {
                System.err.println("Message to session address: " + event.result());
            };
        }

        String uID = userID;
        Handler<Message<R>> msgHandler = messageHandler;

        String sessionAddress = UUID.randomUUID().toString();
        eventBus.consumer("out.sessions.1." + sessionAddress, message -> {
            msgHandler.handle(new MessageImpl(message));
        });

        String address = domain + "." + version + "." + type;
        String cID = UUID.randomUUID().toString();

        DeliveryOptions options = new DeliveryOptions()
                .addHeader("statusCode", "200")
                .addHeader("correlationID", cID)
                .addHeader("origin", serviceName)
                .addHeader("userID", userID)
                .addHeader("userRoles", userRoles.encode());

        JsonObject sessionInfo = new JsonObject()
                .put("userID", uID)
                .put("userRoles", userRoles)
                .put("sessionAddress", sessionAddress)
                .put("pathName", pathName)
                .put("traits", new JsonObject());

        eventBus.send(address, new JsonObject().put("sessionInfo", sessionInfo).put("sessionConnected", true), options, handler -> {
            if(handler.failed()) {
                System.err.println("Failed: creating session for user " + uID + " with correlationID " + cID + " to " + address + ": " + handler.cause());
                future.fail(handler.cause());
            }
            else {
                JsonObject result = new MessageImpl(handler.result()).getBodyAsJsonObject();
                userSessionMap.put(uID, result);
                future.complete(result);
            }
        });

        return cID;
    }

    public <R> String sendAsUser(String domain, String version, String type, JsonObject payload, Handler<Message<R>> replyHandler) {
        return sendAsUser(domain, version, type, null, null, null, null, null, payload, null, replyHandler);
    }

    public <R> String sendAsUser(String domain, String version, String type, String userID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return sendAsUser(domain, version, type, null, userID, null, null, null, payload, null, replyHandler);
    }

    public <R> String sendAsUser(String domain, String version, String type, String userID, JsonArray userRoles, JsonObject payload, Handler<Message<R>> replyHandler) {
        return sendAsUser(domain, version, type, null, userID, userRoles, null, null, payload, null, replyHandler);
    }

    public <R> String sendAsUser(String domain, String version, String type, Integer statusCode, String userID, JsonArray userRoles, String origin, String correlationID, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        if(payload == null) {
            payload = new JsonObject();
        }
        if(options == null) {
            options = new DeliveryOptions();
        }
        if(correlationID == null) {
            correlationID = UUID.randomUUID().toString();
        }
        if(origin == null) {
            origin = "webUser";
        }
        if(statusCode == null) {
            statusCode = 200;
        }
        if(userID == null) {
            userID = "testUser";
        }
        if(userRoles == null) {
            userRoles = new JsonArray().add("authenticated");
        }

        String cID = correlationID;
        String uID = userID;
        JsonObject pld = payload;
        options.addHeader("statusCode", statusCode.toString())
                .addHeader("correlationID", cID)
                .addHeader("origin", origin)
                .addHeader("userID", userID)
                .addHeader("userRoles", userRoles.encode());
        DeliveryOptions opt = options;

        Future<JsonObject> sessionFuture = Future.future();
        sessionFuture.setHandler(result -> {
           if(result.failed()) {
               result.cause().printStackTrace();
               return;
           }
            String address = domain + "." + version + "." + type;
            if(replyHandler != null) {
                eventBus.send(address, pld, opt, handler -> {
                    if(handler.failed()) {
                        System.err.println("Failed: sending message as user " + uID + " with correlationID " + cID + " to " + address + ": " + handler.cause());
                        replyHandler.handle(new MessageImpl(cID, handler.cause()));
                    }
                    else {
                        replyHandler.handle(new MessageImpl(handler.result()));
                    }
                });
            }
            else {
                eventBus.send(address, pld, opt);
            }
        });

        if("router".equals(type)) {
            // check that session has been created
            JsonObject sessionInfo = userSessionMap.get(userID);
            if(sessionInfo == null) {
                createUserSession(domain, version, type, "/", userID , userRoles, null, sessionFuture);
            }
            else {
                sessionFuture.complete(sessionInfo);
            }
        }
        else {
            sessionFuture.complete();
        }
        return cID;
    }

    public void getAnonymousSession(Future<JsonObject> future) {
        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    if (200 != sessionReply.statusCode()) {
                        future.fail(sessionReply.getMessage());
                        return;
                    }

                    future.complete(new JsonObject()
                            .put("sessionTokenHash", token)
                            .put("sessionTokenHashConfig", hashConfig));
                });
    }

    public void getNewUserAndAuthenticatedSession(JsonArray userRoles, Future<JsonObject> future) {
        JsonObject userHashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", userHashConfig)
                , sessionReply -> {
                    if(200 != sessionReply.statusCode()) {
                        future.fail(sessionReply.getMessage());
                        return;
                    }

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        if(!registrationResult.succeeded()) {
                            future.fail(registrationResult.cause());
                        }

                        JsonObject user = registrationResult.result();

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", userHashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                                    if(!authReply.succeeded()) {
                                        future.fail(authReply.cause());
                                    }
                                    if(200 != authReply.statusCode()) {
                                        future.fail(authReply.getMessage());
                                        return;
                                    }

                                    JsonObject result = authReply.getBodyAsJsonObject();
                                    user
                                            .put("sessionTokenHash", token)
                                            .put("sessionTokenHashConfig", userHashConfig)
                                            .put("userHashConfig", userHashConfig)
                                            .put("userRoles", result.getJsonArray("userRoles"));

                                    future.complete(user);
                                });
                    });
                    getNewUser(userRoles, userRegistrationFuture);
                });
    }

    public void getNewUser(JsonArray userRoles, Future<JsonObject> future) {
        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String password = UUID.randomUUID().toString();
        String passwordHash = EncryptionUtils.hash(password, authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        if(userRoles == null) {
            userRoles = new JsonArray();
        }

        send("userAuth", "1", "userRegistered", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                        .put("solutionAddresses", new JsonArray().add(new JsonObject().put("domain", "test").put("version", "1").put("type", "dummy")))
                        .put("userRoles", userRoles)
                , reply -> {
                    if(reply.failed()) {
                        future.fail(reply.cause());
                        return;
                    }
                    if(reply.statusCode() != 200) {
                        future.fail(reply.getMessage());
                        return;
                    }

                    JsonObject result = reply.getBodyAsJsonObject();
                    result.put("emailAddress", emailAddress);
                    result.put("passwordHash", passwordHash);
                    result.put("password", password);

                    future.complete(result);
                });
    }

    private String getRandomMailAddress() {
        return UUID.randomUUID().toString() + "@test.de";
    }

    private static boolean isDatabaseAccessible(JsonObject databaseConfig) {
        Database database = null;
        try {
            database = new ArangoDBImpl("de.maltebehrendt.uppt.services.testDBCheck", databaseConfig);
            database.exists();
            database.close();
            return true;
        }
        catch (Exception e) {
            // no database is running on local host
            if(database != null) {
                try {
                    database.close();
                }
                catch(Exception ex) {}
            }
            e.printStackTrace();
            return false;
        }
    }

    private static boolean startTestDatabase(String directory, JsonArray serviceNames) {
        if(isDatabaseAccessible(null)) {
            // make sure the database is clean
            return cleanUpTestDatabase(serviceNames, null);
        }
        else {
            // ensure any local junit db is being shutdown
            if (!shutdownTestDatabase(directory)) {
                return false;
            }
            // delete previous database(s), if existent
            if (FileUtils.isFileExisting(directory + File.separator + "databases")) {
                FileUtils.deleteFolder(directory + File.separator + "databases");
            }

            try {
                ShellUtils.runOnShellBlocking(startUpCommand, directory, null);
                return isDatabaseAccessible(null);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    private static boolean cleanUpTestDatabase(JsonArray serviceNames, JsonObject databaseConfig) {
        try {
            for (int i =0;i<serviceNames.size();i++) {
                ArangoDBImpl database = new ArangoDBImpl(serviceNames.getString(i), databaseConfig);
                if(database.exists()) {
                    database.deleteBlocking();
                }
                database.close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static boolean shutdownTestDatabase(String directory) {
        // try stopping a running local arangodb (not clean, might be race conditions)
        if(FileUtils.isFileExisting(directory + File.separator + "arangoDB.pid")) {
            try {
                int result = ShellUtils.runOnShellBlocking(shutdownCommand, directory, null);
                File pidFile = new File(directory + File.separator + "arangoDB.pid");
                pidFile.delete();
                return result == 0? true : false;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private <T extends AbstractTest> T castClass() {
        return (T) this;
    }
}
