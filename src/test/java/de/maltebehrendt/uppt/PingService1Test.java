package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.events.Impl.MessageImpl;
import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.MessageSource;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.Map;

/**
 * Created by malte on 19.12.16.
 */
public class PingService1Test extends AbstractTest {
    private static String address = "pingpong.1.ping";
    private static ServiceDiscovery serviceDiscovery = null;

    @BeforeClass
    public static void setup(TestContext context) {
        Map<String, Object> map = setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
        Vertx vertx = (Vertx) map.get("vertx");

        Async mockupResult = context.async(1);
        serviceDiscovery = ServiceDiscovery.create(vertx);

        // register and publish a mockup service the ping service depends on (pong)
        JsonObject processorRecord = new JsonObject()
                .put("description", "Mockup Pong Service interface")
                .put("domain", "pingpong")
                .put("version", "1")
                .put("type", "ping")
                .put("provides", new JsonArray())
                .put("requires", new JsonArray());

        // publish endpoint as service
        Record record = MessageSource.createRecord(address, address, JsonObject.class, processorRecord);
        serviceDiscovery.publish(record, publishResult -> {
            context.assertTrue(publishResult.succeeded(), "Published endpoint " + address + " to service discovery");
            mockupResult.countDown();
        });
        mockupResult.awaitSuccess();
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testPingServiceDeployment(TestContext context) {
        Async deployResult = context.async(3);
        JsonObject pingService1Config = new JsonObject()
                .put("serviceLocation", "de.maltebehrendt.uppt.services.PingService1")
                .put("serviceInstances", 1);
        Future<String> pingDeployFuture = Future.future();
        Handler<AsyncResult<String>> pingDeploymentHandler = new Handler<AsyncResult<String>>() {
            @Override
            public void handle(AsyncResult<String> event) {
                context.assertFalse(event.failed(), "PingService deployment did not fail: " + event.cause());
                deployResult.countDown();
                context.assertTrue(event.succeeded(), "PingService deployed.");
                deployResult.countDown();
                context.assertNotNull(event.result(), "Deployment returned a deployment ID.");
                deployResult.countDown();
            }
        };
        pingDeployFuture.setHandler(pingDeploymentHandler);
        InstanceRunner.deployVerticle(pingService1Config, pingDeployFuture);
        deployResult.awaitSuccess();
    }

    @Test
    public void testServiceIsSendingAPing(TestContext context) {
        Async isSendingPingResult = context.async(1);

        MessageConsumer<JsonObject> consumer = eventBus.consumer(address);
        consumer.handler(new Handler<Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                MessageImpl upptMessageImpl = new MessageImpl(message);
                JsonObject payload = upptMessageImpl.getBodyAsJsonObject();
                context.assertTrue(payload != null);
                context.assertNotNull(upptMessageImpl.correlationID());
                context.assertEquals(200, upptMessageImpl.statusCode());

                isSendingPingResult.countDown();
            }
        });
        context.assertTrue(consumer.isRegistered(), "Mockup ping processor is registered");
        isSendingPingResult.awaitSuccess();
    }

    @Test
    public void testDiscoverServiceEntries(TestContext context) {
        Async getRecordResult = context.async(7);

        while(!getRecordResult.isCompleted()) {
            serviceDiscovery.getRecord(new JsonObject().put("name", address), result -> {
                if(result.succeeded() && result.result() != null) {
                    Record record = result.result();
                    context.assertEquals(address, record.getName(), "Record name is correct");
                    getRecordResult.countDown();
                    context.assertEquals(address, record.getLocation().getString("endpoint"), "Record location address is correct");
                    getRecordResult.countDown();
                    context.assertEquals("endpoint", record.getLocation().fieldNames().toArray()[0].toString(), "Record location type is correct");
                    getRecordResult.countDown();
                    context.assertEquals("message-source", record.getType().toString(), "Record type is correct");
                    getRecordResult.countDown();
                    context.assertEquals("class io.vertx.servicediscovery.Record", record.getClass().toString(), "Record class is correct");
                    getRecordResult.countDown();
                    context.assertEquals("UP", record.getStatus().toString(), "Record status is correct");
                    getRecordResult.countDown();

                    JsonObject metadata = record.getMetadata();
                    context.assertNotNull(metadata, "Record metadata not null");
                    getRecordResult.countDown();
                }
            });

            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        getRecordResult.awaitSuccess();
    }
}
