package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.lang.invoke.MethodHandles;

public class AsyncMessageReplyCacheTest extends AbstractTest {

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    //TODO
}
