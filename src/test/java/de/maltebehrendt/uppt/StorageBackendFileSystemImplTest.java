package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import de.maltebehrendt.uppt.storage.Impl.StorageBackendFileSystemImpl;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.zip.ZipInputStream;

public class StorageBackendFileSystemImplTest extends AbstractTest {
    private static String zipFilePath = null;
    private static StorageBackendFileSystemImpl storageBackendFileSystem = null;
    private static String storagePath = null;
    private static Vertx vertx = null;

    //TODO: a) test delete b) test malicious paths

    @BeforeClass
    public static void setup(TestContext context) {
        Map<String, Object> map = setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
        vertx = (Vertx) map.get("vertx");
        zipFilePath = map.get("baseDirectory") + File.separator + "vert.x-3.5.0.zip";
        storagePath = map.get("baseDirectory") + File.separator + "storage" + File.separator + "StorageBackendFileSystemImplTest".replaceAll("[^a-zA-Z0-9]", "_");

        if(vertx.fileSystem().existsBlocking(storagePath)) {
            vertx.fileSystem().deleteRecursiveBlocking(storagePath, true);
        }

        storageBackendFileSystem = new StorageBackendFileSystemImpl(storagePath, vertx);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);

        if(vertx.fileSystem().existsBlocking(storagePath)) {
            vertx.fileSystem().deleteRecursiveBlocking(storagePath, true);
        }
    }

    @Test
    public void testPersist(TestContext context) {
        Async testResult = context.async(1);

        OpenOptions options = new OpenOptions()
                .setCreate(false)
                .setRead(true)
                .setWrite(false);
        vertx.fileSystem().open(zipFilePath, options, openHandler -> {
            context.assertFalse(openHandler.failed());
            AsyncFile asyncFile = openHandler.result();

            Future<Void> persistFuture = Future.future();
            persistFuture.setHandler(handler -> {
                context.assertFalse(handler.failed());
                context.assertTrue(handler.succeeded());

                context.assertTrue(vertx.fileSystem().existsBlocking(storagePath + File.separator + "vertxCopy.zip"));
                File newFile = new File(storagePath + File.separator + "vertxCopy.zip");
                context.assertEquals(78844404l, newFile.length());

                testResult.countDown();
            });

            storageBackendFileSystem.persist("vertxCopy.zip", (ReadStream) asyncFile, persistFuture);
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testRetrieve(TestContext context) {
        Async testResult = context.async(2);

        OpenOptions options = new OpenOptions()
                .setCreate(false)
                .setRead(true)
                .setWrite(false);
        vertx.fileSystem().open(zipFilePath, options, openHandler -> {
            context.assertFalse(openHandler.failed());
            AsyncFile asyncFile = openHandler.result();

            Future<Void> persistFuture = Future.future();
            persistFuture.setHandler(handler -> {
                context.assertFalse(handler.failed());
                context.assertTrue(handler.succeeded());

                context.assertTrue(vertx.fileSystem().existsBlocking(storagePath + File.separator + "vertxCopy2.zip"));
                File newFile = new File(storagePath + File.separator +"vertxCopy2.zip");
                context.assertEquals(78844404l, newFile.length());

                testResult.countDown();

                vertx.fileSystem().open(storagePath + File.separator + "vertxCopy3.zip", new OpenOptions()
                        .setCreate(true)
                        .setWrite(true), openWriteHandler -> {
                    context.assertFalse(openWriteHandler.failed());

                    AsyncFile asyncWriteFile = openWriteHandler.result();
                    Future<Void> retrieveFuture = Future.future();
                    retrieveFuture.setHandler(retrieveHandler -> {
                        context.assertFalse(retrieveHandler.failed());
                        context.assertTrue(retrieveHandler.succeeded());

                        context.assertTrue(vertx.fileSystem().existsBlocking(storagePath + File.separator + "vertxCopy3.zip"));
                        File retrFile = new File(storagePath + File.separator + "vertxCopy3.zip");
                        context.assertEquals(78844404l, retrFile.length());
                        testResult.countDown();
                    });

                    storageBackendFileSystem.retrieve("vertxCopy2.zip", asyncWriteFile, retrieveFuture);
                });
            });

            storageBackendFileSystem.persist("vertxCopy2.zip", asyncFile, persistFuture);
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testUnzipAndPersist(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> unzipFuture = Future.future();
        unzipFuture.setHandler(handler -> {
            context.assertFalse(handler.failed());
            JsonObject extractionResults = (JsonObject) handler.result();
            context.assertEquals(51, extractionResults.getInteger("numberOfFiles"), "Number of extracted files is correct");
            context.assertEquals(87815234L, extractionResults.getLong("totalExtractedSize"), "Total size of extracted files is correct");
            context.assertNotNull(extractionResults.getString("targetDirectory"), "Target directory is returned");
            context.assertEquals("unzip35", extractionResults.getString("targetDirectory"), "target directory is relative");
            context.assertTrue(vertx.fileSystem().existsBlocking(storagePath + File.separator  +extractionResults.getString("targetDirectory")));
            testResult.countDown();
        });

        try {
            storageBackendFileSystem.unzipAndPersist("unzip35", true, new ZipInputStream(new FileInputStream(zipFilePath)), 250L, 10000L, unzipFuture);
        } catch (FileNotFoundException e) {
            context.assertTrue(false, "Zip file not found!");
        }

        testResult.awaitSuccess();
    }

    @Test
    public void testUnzipAndPersistTooManyFiles(TestContext context) {
        Async testResult = context.async(1);
        Future<JsonObject> unzipFuture = Future.future();
        unzipFuture.setHandler(handler -> {
            context.assertTrue(handler.failed());
            context.assertFalse(vertx.fileSystem().existsBlocking(storagePath + File.separator + "unzip351"));
            testResult.countDown();
        });

        try {
            storageBackendFileSystem.unzipAndPersist("unzip35", true, new ZipInputStream(new FileInputStream(zipFilePath)), 250L, 5L, unzipFuture);
        } catch (FileNotFoundException e) {
            context.assertTrue(false, "Zip file not found!");
        }

        testResult.awaitSuccess();
    }

    @Test
    public void testUnzipAndPersistTooLarge(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> unzipFuture = Future.future();
        unzipFuture.setHandler(handler -> {
            context.assertTrue(handler.failed());
            context.assertFalse(vertx.fileSystem().existsBlocking(storagePath + File.separator + "unzip352"));
            testResult.countDown();
        });

        try {
            storageBackendFileSystem.unzipAndPersist("unzip35", true, new ZipInputStream(new FileInputStream(zipFilePath)), 5L, 10000L, unzipFuture);
        } catch (FileNotFoundException e) {
            context.assertTrue(false, "Zip file not found!");
        }

        testResult.awaitSuccess();
    }

}
