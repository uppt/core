package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.logging.LoggerProxy;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

/**
 * Created by malte on 19.12.16.
 */
public class NoopService2 extends AbstractService {
    @Override
    public void prepare(Future<Object> prepareFuture) {
        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {

    }

    public void setVertx(Vertx newVertx) {
        vertx = newVertx;
    }

    public void setLogger(LoggerProxy newLogger) {
        logger = newLogger;
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
    // this service does nothing, it's only here for allowing the instance to start (does not start without at least one service to deploy...)
}
