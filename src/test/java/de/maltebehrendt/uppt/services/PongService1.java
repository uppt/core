package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Date;


/**
 * Created by malte on 24.10.16.
 */
public class PongService1 extends AbstractService {

    @Override
    public void prepare(Future<Object> prepareFuture) {
        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {

    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }

    @Processor(
            domain = "pingpong",
            version = "1",
            type = "ping",
            description = "Replies to a ping with a pong (reply message with status code 200)"
    )
    public void processPing(Message message) {
        JsonObject document = new JsonObject()
                .put("cid", message.correlationID())
                .put("origin", message.origin())
                .put("timestamp", new Date().getTime());


        database.insertDocument("pong", document, Future.future(insertFuture -> {
            if(insertFuture.failed()) {
              logger.error("[" + message.correlationID() + "] Failed to persist ping/pong document!", insertFuture.cause());
              message.fail("Failed to persist ping/pong document!");
              return;
            }

            // build some graph, just for testing
            Future<String> getPingSenderFuture = Future.future();
            getPingSenderFuture.setHandler(pingSenderHandler -> {
                if(pingSenderHandler.failed()) {
                    return;
                }

                String pingSenderKey = pingSenderHandler.result();

                // insert a new pin pong vertex
                JsonObject pingPongObject = new JsonObject()
                        .put("cid", message.correlationID())
                        .put("ping", new JsonObject().put("receiver", this.serviceName + ":" + this.instanceID))
                        .put("pong", new JsonObject().put("sender", message.origin()));

                database.insertVertex("pingPongGraph", "pingPongs", pingPongObject, Future.future(insertVertexHandler -> {
                    if(insertVertexHandler.failed()) {
                        logger.error("[" + message.correlationID() + "] Failed to create ping pong vertex!", insertVertexHandler.cause());
                        message.fail("Failed to create ping pong vertex!");
                        return;
                    }

                    String pingPongKey = insertVertexHandler.result();

                    // create an edge between pingSender and the pingPong vertex
                    database.insertEdge("pingPongGraph", "pingPongEdge", new JsonObject().put("timestamp", document.getLong("timestamp")), pingSenderKey, pingPongKey, Future.future(insertEdgeHandler -> {
                        if(insertEdgeHandler.failed()) {
                            logger.error("[" + message.correlationID() + "] Failed to create ping pong vertex!", insertEdgeHandler.cause());
                            message.fail("Failed to create ping pong vertex!");
                            return;
                        }
                        message.reply(200);
                    }));
                }));
            });


            // get the ping sender vertex key
            Future<JsonArray> queryFuture = Future.future();
            queryFuture.setHandler(getPingSenderHandler -> {
                if(getPingSenderHandler.failed()) {
                    logger.error("[" + message.correlationID() + "] Failed to get ping sender vertex!", getPingSenderHandler.cause());
                    message.fail("Failed to get ping sender vertex!");
                    return;
                }

                JsonArray results = getPingSenderHandler.result();
                if(results.isEmpty()) {
                    // does not exist yet, create pingSender
                    database.insertVertex("pingPongGraph", "pingSenders", new JsonObject()
                                    .put("name", message.origin())
                                    .put("timestamp", document.getLong("timestamp"))
                            , Future.future(createPingSenderHandler -> {
                                if(createPingSenderHandler.failed()) {
                                    logger.error("[" + message.correlationID() + "] Failed to create ping sender vertex!", createPingSenderHandler.cause());
                                    message.fail("Failed to create ping sender vertex!");
                                    return;
                                }
                                getPingSenderFuture.complete(createPingSenderHandler.result());
                            }));
                }
                else {
                    getPingSenderFuture.complete(results.getJsonObject(0).getString("_key"));
                }
            });
            database.query("FOR p IN pingSenders FILTER p.name == \""+ message.origin() + "\" LIMIT 1 RETURN p", null, queryFuture);
        }));
    }

    @Processor(
            domain = "pingpong",
            version = "1",
            type = "missingPongRecords",
            description = "Returns up to 20 ping/pong records from the database"
    )
    public void processMissingPongRecords(Message message) {
        Future<JsonArray> queryFuture = Future.future();
        queryFuture.setHandler(result -> {
            if(result.failed()) {
                logger.error("[" + message.correlationID() + "] Failed to query database!", result.cause());
                message.fail("Failed to query database!");
            }
            else {
                message.reply(200, new JsonObject()
                        .put("results", result.result()));
            }
        });
        database.query("FOR p IN pong LIMIT 20 RETURN p", null, queryFuture);
    }

    @Processor(
            domain = "pingpong",
            version = "1",
            type = "missingPongRecord",
            description = "Return ping/pong record from the database by its key",
            requires = {
                    @Payload(key = "key", type = DataType.STRING, description = "Key of the record/document")
            }
    )
    public void processMissingPongRecord(Message message) {
        Future<JsonObject> readFuture = Future.future();
        readFuture.setHandler(result -> {
           if(result.failed()) {
               logger.error("[" + message.correlationID() + "] Failed to read document from database!", result.cause());
               message.fail("Failed to read document from database!");
           }
           else {
               message.reply(200,  result.result());
           }
        });
        database.readDocument("pong", message.getBodyAsJsonObject().getString("key"), readFuture);
    }

    @Processor(
            domain = "pingpong",
            version = "1",
            type = "missingPingPongVertices",
            description = "Returns all pingPong vertices from the database"
    )
    public void processMissingPingPongVertices(Message message) {
        Future<JsonArray> future = Future.future();
        future.setHandler(result -> {
            if(result.failed()) {
                message.fail("Failed to get pingSender! " + result.cause());
                return;
            }

            Future<JsonArray> getAllFuture = Future.future();
            getAllFuture.setHandler(getAllResult -> {
                if(getAllResult.failed()) {
                    message.fail("Failed to get all ping pong vertices:" + getAllResult.cause());
                    return;
                }
                message.reply(200, new JsonObject().put("results", getAllResult.result()));
            });

            if(result.result().size() != 1) {
                message.fail("Multipe pingSenders were returned - should be only one! Check database template file for correct setup!");
                return;
            }

            String rootKey = result.result().getJsonObject(0).getString("_key");
            database.query("FOR vertex, edge IN OUTBOUND \"pingSenders/"+ rootKey +"\" GRAPH \"pingPongGraph\" RETURN vertex", null, getAllFuture);
        });
        database.query("FOR p IN pingSenders RETURN p", null, future);
    }
}
