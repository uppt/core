package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Customer;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;


/**
 * Created by malte on 24.10.16.
 */
public class PingService1 extends AbstractService {
    private Long periodicID = null;
    private CircuitBreaker circuitBreaker = null;

    @Override
    public void prepare(Future<Object> prepareFuture) {
        circuitBreaker = CircuitBreaker.create("ping-breaker", vertx, new CircuitBreakerOptions()
            .setMaxFailures(2)
            .setMaxRetries(1)
            .setNotificationAddress(null)
        );
        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {
        periodicID = vertx.setPeriodic(1000, event -> {
                ping();
        });
    }


    @Customer(
            domain = "pingpong",
            version = "1",
            type = "ping",
            description = "Sends a ping and expects a pong (reply message with status code 200)"
    )
    public void ping() {
        // in test setting, the multiple vertx instances hinder canceling the periodic ID (some static vertx var issue somewhere?)
        // this circuit breaker is for keeping the error logs from exploding when the pong service is shutdown
        circuitBreaker.execute(future -> {
            send("pingpong", "1", "ping", new JsonObject(), reply -> {
                if(reply.failed()) {
                    logger.error("Failed: sending ping with correlationID " + reply.correlationID());
                    future.fail("Failed: sending ping with correlationID " + reply.correlationID());
                }
                future.complete();
            });
        });
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        if(periodicID != null) {
            vertx.cancelTimer(periodicID);
            periodicID = null;
        }
        shutdownFuture.complete();
    }
}
