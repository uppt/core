package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Authorities;
import de.maltebehrendt.uppt.annotation.Route;
import de.maltebehrendt.uppt.annotation.Router;
import de.maltebehrendt.uppt.enums.Operation;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by malte on 19.02.17.
 */
public class RouterRouteService1 extends AbstractSolution {

    @Override
    public void prepare(Future<Object> prepareFuture) {
        prepareFuture.complete();
    }

    @Router(domain = "in.test",
            version = "1",
            type = "router",
            description = "Exemplary realization of a minimal router",
            authorities = @Authorities(
                    isAuthenticationRequired = false
            )
    )
    public void router(Message message) {
        // the results from the results are in the body's "results" JsonArray
        // you can either perform further operations or just return it
        message.reply(message.getBodyAsJsonObject());
    }

    @Override
    public void handleSessionBecameAvailable(JsonObject sessionInfo) {
        // TODO: example and tests. Right now they are only demonstrated by bridgeWeb
    }

    @Override
    public void handleSessionBecameAuthenticated(JsonObject sessionInfo) {
        // TODO: example and tests. Right now they are only demonstrated by bridgeWeb
    }

    @Override
    public void handleSessionBecameUnavailable(JsonObject sessionInfo) {
        // TODO: example and tests. Right now they are only demonstrated by bridgeWeb
    }

    @Override
    public void processSessionStateMissing(String userID, Future<JsonObject> sessionStateFuture) {
        sessionStateFuture.complete(new JsonObject());
    }

    @Route(
            description = "Performs a get operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "*",
            operation = Operation.GET,
            authorities = @Authorities(
                isAuthenticationRequired = false
            )
    )
    public void routeGet(Message message) {
        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");

        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...
        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("someObject.someArray[1]"))
                .put("values", new JsonArray().add(2))
                .put("origins", new JsonArray().add("RouterRouteService1"))
        );
    }

    @Route(
            description = "Performs a set operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "*",
            operation = Operation.SET,
            authorities = @Authorities(
                    isAuthenticationRequired = false
            )
    )
    public void routeSet(Message message) {
        JsonArray pathSteps = message.getBodyAsJsonObject().getJsonArray("pathSteps");
        JsonArray pathPredicates = message.getBodyAsJsonObject().getJsonArray("pathPredicates");
        Object value = message.getBodyAsJsonObject().getValue("value");
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...
        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("someObject.someArray[1]"))
                .put("values", new JsonArray().add(value))
                .put("origins", new JsonArray().add("RouterRouteService1"))
        );
    }

    @Route(
            description = "Performs a del operation in any path ('*') of this domain/version",
            domain = "test",
            version = "1",
            path = "*",
            operation = Operation.DEL,
            authorities = @Authorities(
                    isAuthenticationRequired = false
            )
    )
    public void routeDel(Message message) {
        // real scenarios: walk/parse steps and predicates
        // here I know what's being requested...
        message.reply(new JsonObject()
                .put("paths", new JsonArray().add("someObject.someArray[1]"))
                .put("values", new JsonArray().add(""))
                .put("origins", new JsonArray().add("RouterRouteService1"))
        );
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
}
