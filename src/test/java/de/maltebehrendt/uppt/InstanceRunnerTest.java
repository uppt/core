package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.Date;

/**
 * Created by malte on 02.11.16.
 */
public class InstanceRunnerTest extends AbstractTest {

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    /**
     * Deploy two services, ping and pong, with dependencies. Make sure the ping service (that depends on pong)
     * does notify of it's waiting status before deploying the pong service.
     */
    public void testServiceDeployment(TestContext context) {
        Async testResult = context.async(3);

        // get notified when services are waiting/deployed
        MessageConsumer<JsonObject> consumer = eventBus.consumer("system.1.registeringNewCustomer", message -> {
            // ping service is waiting for a processor (pong service)
            if("503".equalsIgnoreCase(message.headers().get("statusCode"))
                    && "de.maltebehrendt.uppt.services.PingService1".equalsIgnoreCase(message.body().getString("serviceName"))) {
                context.assertEquals("503", message.headers().get("statusCode"), "Ping consumer is waiting for pong service");
                context.assertNotNull(message.headers().get("correlationID"), "MessageImpl header contains correlationID");
                context.assertNotNull(message.headers().get("origin"), "MessageImpl header contains origin");
                testResult.countDown();

                // deploy pong service
                JsonObject pongService1Config = new JsonObject()
                        .put("serviceLocation", "de.maltebehrendt.uppt.services.PongService1")
                        .put("serviceInstances", 1);

                Future<String> pongDeployFuture = Future.future();
                Handler<AsyncResult<String>> pongDeploymentHandler = new Handler<AsyncResult<String>>() {
                    @Override
                    public void handle(AsyncResult<String> event) {
                        context.assertTrue(event.succeeded(), "PongService deployed.");
                        context.assertFalse(event.failed(), "PongService deployment did not fail.");
                        context.assertNotNull(event.result(), "Deployment returned a deployment ID");
                        testResult.countDown();

                        // TODO check that there are service discovery entries for the processor (pong)
                        testResult.countDown();
                    }
                };
                pongDeployFuture.setHandler(pongDeploymentHandler);
                InstanceRunner.deployVerticle(pongService1Config, pongDeployFuture);
            }
        });

        JsonObject pingService1Config = new JsonObject()
                .put("serviceLocation", "de.maltebehrendt.uppt.services.PingService1")
                .put("serviceInstances", 1)
                .put("database", new JsonObject()
                        .put("engine", "plocal")
                        .put("address", "./junit/instanceRunnerTest/databases/")
                        .put("username", "admin")
                        .put("password", "admin"));
        Future<String> pingDeployFuture = Future.future();
        Handler<AsyncResult<String>> pingDeploymentHandler = new Handler<AsyncResult<String>>() {
            @Override
            public void handle(AsyncResult<String> event) {
                context.assertTrue(event.succeeded(), "PingService deployed.");
                context.assertFalse(event.failed(), "PingService deployment did not fail.");
                context.assertNotNull(event.result(), "Deployment returned a deployment ID");
                testResult.countDown();
            }
        };
        pingDeployFuture.setHandler(pingDeploymentHandler);
        InstanceRunner.deployVerticle(pingService1Config, pingDeployFuture);
        testResult.awaitSuccess();
    }

    @Test
    /**
     * Only checks if default testing log file was created/modified
     */
    public void testLogFileExistence(TestContext context) {
        File logFile = new File("test_instance.log");
        context.assertTrue(logFile.exists(), "Test log file exists");
        context.assertTrue(logFile.lastModified() < (new Date()).getTime() - 1000, "Log file recently modified");
    }
}
