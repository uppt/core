package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.util.JSONPathSplitter;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by malte on 15.02.17.
 */
public class JSONPathSplitterTest {
    // basically only for regression testing

    @Test
    public void testPathSplitsWithoutBracketsOrFilters() throws Exception {
        String path = "$.store..price";
        JsonObject result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        JsonArray steps = result.getJsonArray("steps");
        JsonArray predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"*\",\"price\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[{\"type\":\"wildcard\",\"value\":\"*\"}],[]]", predicates.encode());

        path = "$.store.*";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"*\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[{\"type\":\"wildcard\",\"value\":\"*\"}]]", predicates.encode());

        path = "$.store.*.price..*";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"*\",\"price\",\"*\",\"*\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[{\"type\":\"wildcard\",\"value\":\"*\"}],[],[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"wildcard\",\"value\":\"*\"}]]", predicates.encode());
    }

    @Test
    public void testPathSplitsWithBracketsWithoutFilters() throws Exception {
        String path = "$.store.book[*].author";
        JsonObject result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        JsonArray steps = result.getJsonArray("steps");
        JsonArray predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\",\"author\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"wildcard\",\"value\":\"*\"}],[]]", predicates.encode());

        path = "$..book[2]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"index\",\"value\":2}]]", predicates.encode());

        path = "$..book[0,1]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"indices\",\"value\":[0,1]}]]", predicates.encode());


        path = "$..book[1,2]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"indices\",\"value\":[1,2]}]]", predicates.encode());

        path = "$..book[:2]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"indices\",\"value\":[0,1]}]]", predicates.encode());

        path = "$..book[2:]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[{\"type\":\"fromTail\",\"value\":2}]]", predicates.encode());

        path = "$.['store'][\"book\"][2:]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[{\"type\":\"fromTail\",\"value\":2}]]", predicates.encode());

        path = "$.['store'].book[2][\"author\"]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\",\"author\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[{\"type\":\"index\",\"value\":2}],[]]", predicates.encode());

    }

    @Test
    public void testPathSplitsWithFilters() throws Exception {
        String path = "$..book[?(@.isbn)]";
        JsonObject result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        JsonArray steps = result.getJsonArray("steps");
        JsonArray predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[],[{\"type\":\"filter\",\"operator\":\"hasProperty\",\"argument1\":\"isbn\"}]]", predicates.encode());

        path = "$..book[?(@.isbn.i10.string)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"*\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[{\"type\":\"wildcard\",\"value\":\"*\"}],[],[{\"type\":\"filter\",\"operator\":\"hasProperty\",\"argument1\":\"isbn.i10.string\"}]]", predicates.encode());


        path = "$.store.book[?(@.price < 10)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"}]]", predicates.encode());

        path = "$.store.book[?(@.isbn == 3476837 && @.price < 10)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]]", predicates.encode());

        path = "$.store.book[?(@.isbn == 3476837 || @.price < 10)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]]", predicates.encode());

        path = "$.store.book[?((@.isbn == 3476837 || @.price < 10))]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]]", predicates.encode());

        path = "$.store.book[?((@.isbn == 3476837 || @.price < 10) && @.author)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"hasProperty\",\"argument1\":\"author\"},{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]}]]", predicates.encode());

        path = "$.store.book[?((@.isbn == 3476837 || @.price < 10) || @.author)]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"hasProperty\",\"argument1\":\"author\"},{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]}]]", predicates.encode());

        path = "$.store.book[?(@.author || (@.isbn == 3476837 || @.price < 10))]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]},{\"type\":\"filter\",\"operator\":\"hasProperty\",\"argument1\":\"author\"}]}]]", predicates.encode());


        path = "$.store.book[?((@.isbn == 3476837 || @.price < 10) && (@.isbn == 3473337 || @.price < 5))]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"5\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3473337\",\"argument1\":\"isbn\"}]},{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]}]]", predicates.encode());

        path = "$.store.book[?((@.isbn == 3476837 && @.price < 10) || (@.isbn == 3473337 && @.price < 5))]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"5\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3473337\",\"argument1\":\"isbn\"}]},{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]}]]", predicates.encode());

        path = "$.store.book[?((@.isbn == 3476837 && @.price < 10) || (@.isbn == 3473337 && @.price < 5) || (@.isbn == 3473227 && @.price < 1))]";
        result = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);
        steps = result.getJsonArray("steps");
        predicates = result.getJsonArray("predicates");
        assertFalse(steps.isEmpty());
        assertEquals("[\"store\",\"book\"]", steps.encode());
        assertFalse(predicates.isEmpty());
        assertEquals("[[],[],[{\"type\":\"orClauses\",\"clauses\":[{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"1\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3473227\",\"argument1\":\"isbn\"}]},{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"5\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3473337\",\"argument1\":\"isbn\"}]},{\"type\":\"andClauses\",\"clauses\":[{\"type\":\"filter\",\"operator\":\"<\",\"argument2\":\"10\",\"argument1\":\"price\"},{\"type\":\"filter\",\"operator\":\"==\",\"argument2\":\"3476837\",\"argument1\":\"isbn\"}]}]}]]", predicates.encode());

    }
}
