package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.Vertx;
import io.vertx.core.file.FileProps;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.Map;


public class PublicResourcesTest extends AbstractTest {
    private static Vertx vertx = null;
    private static String webRootPath = null;

    // note: the WebBridge service does further tests
    @BeforeClass
    public static void setup(TestContext context) {
        Map<String, Object> map = setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
        vertx = (Vertx) map.get("vertx");
        webRootPath = map.get("baseDirectory") + File.separator + "storage" + File.separator + "webroot";
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);

        if(vertx.fileSystem().existsBlocking(webRootPath)) {
            vertx.fileSystem().deleteRecursiveBlocking(webRootPath, true);
        }
    }

    @Test
    public void testNoop1ServicePublicHasBeenExtracted(TestContext context) {
        // only check that the content is there
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1"));
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1/vertx"));
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1/vertx/bin"));
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1/vertx/conf"));
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1/vertx/lib"));
        context.assertTrue(vertx.fileSystem().existsBlocking(webRootPath + File.separator + "noop/1/vertx/vertx-stack.json"));

        FileProps props = vertx.fileSystem().propsBlocking(webRootPath + File.separator + "noop/1/vertx/vertx-stack.json");
        context.assertEquals(9805L, props.size());

        props = vertx.fileSystem().propsBlocking(webRootPath + File.separator + "noop/1/vertx/lib/vertx-core-3.3.3.jar");
        context.assertEquals(4094974L, props.size());
    }
}
