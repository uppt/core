package de.maltebehrendt.uppt;

import de.maltebehrendt.uppt.database.Database;
import de.maltebehrendt.uppt.database.impl.ArangoDBImpl;
import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;

public class DatabaseAccessFromServiceTest extends AbstractTest {
    // TODO: those junit are very, very basic. Especially concurrency tests should be added

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testDatabaseExistence(TestContext context) {
        Async testResult = context.async(2);

        // check that DB is available
        try {
            Database database = new ArangoDBImpl("de.maltebehrendt.uppt.services.PongService1", new JsonObject());
            context.assertTrue(database.exists());
            database.close();
            testResult.countDown();

            database = new ArangoDBImpl("de.maltebehrendt.uppt.services.PingService1", new JsonObject());
            context.assertFalse(database.exists());
            database.close();
            testResult.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }

        testResult.awaitSuccess();
    }

    @Test
    public void testQueryingDocumentsViaService(TestContext context) {
        Async testResult = context.async(1);

        // check that there a few database entries from the pong service
        vertx.setTimer(2000, handler -> {
            send("pingpong", "1", "missingPongRecords", new JsonObject(), reply -> {
                JsonObject result = reply.getBodyAsJsonObject();
                JsonArray results = result.getJsonArray("results");

                context.assertTrue(results.size() > 5, "There are at least two database entries");
                JsonObject firstEntry = results.getJsonObject(0);
                context.assertNotNull(firstEntry.getString("_key"));
                context.assertNotNull(firstEntry.getString("_id"));
                context.assertEquals("de.maltebehrendt.uppt.services.PingService1", firstEntry.getString("origin"));
                context.assertNotNull(firstEntry.getString("cid"));
                context.assertNotNull(firstEntry.getLong("timestamp"));

                testResult.countDown();
            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testQueryingGraphViaService(TestContext context) {
        Async testResult = context.async(1);

        // check that there a few database entries from the pong service
        vertx.setTimer(2000, handler -> {
            send("pingpong", "1", "missingPingPongVertices", new JsonObject(), reply -> {
                context.assertTrue(reply.succeeded());

                JsonObject result = reply.getBodyAsJsonObject();
                JsonArray results = result.getJsonArray("results");

                System.err.println(result.encodePrettily());

                testResult.countDown();
            });
        });

        testResult.awaitSuccess();
    }

    @Test
    public void testReadingDocumentViaService(TestContext context) {
        Async testResult = context.async(2);

        // check that there a few database entries from the pong service
        vertx.setTimer(1000, handler -> {
            send("pingpong", "1" ,"missingPongRecords", new JsonObject(), reply -> {
                JsonObject result = reply.getBodyAsJsonObject();
                JsonArray results = result.getJsonArray("results");
                JsonObject firstEntry = results.getJsonObject(0);
                context.assertNotNull(firstEntry.getString("_key"));
                testResult.countDown();

                send("pingpong", "1", "missingPongRecord", new JsonObject().put("key", firstEntry.getString("_key")), readReply -> {
                    JsonObject readResult =  readReply.getBodyAsJsonObject();
                    context.assertNotNull(readResult.getString("_id"));
                    context.assertEquals("de.maltebehrendt.uppt.services.PingService1", readResult.getString("origin"));
                    context.assertNotNull(readResult.getString("cid"));
                    context.assertNotNull(readResult.getLong("timestamp"));

                    testResult.countDown();
                });
            });
        });

        testResult.awaitSuccess();
    }


}
