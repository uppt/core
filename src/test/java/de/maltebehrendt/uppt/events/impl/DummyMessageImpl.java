package de.maltebehrendt.uppt.events.impl;

import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.UUID;

/**
 * Created by malte on 02.02.17.
 */
public class DummyMessageImpl<R> implements Message<R> {

    private String origin = null;
    Integer statusCode = null;
    String correlationID = null;
    JsonObject payload = null;
    Handler<Message<R>> dummyReplyHandler = null;

    private Throwable cause = null;
    private Boolean succeeded = false;

    public DummyMessageImpl(Handler<Message<R>> dummyReplyHandler) {
        this.origin = "unknown";
        this.statusCode = 200;
        this.correlationID = UUID.randomUUID().toString();
        this.payload = new JsonObject();
        this.dummyReplyHandler = dummyReplyHandler;
    }

    public DummyMessageImpl(JsonObject payload, Handler<Message<R>> dummyReplyHandler) {
        this.origin = "unknown";
        this.statusCode = 200;
        this.correlationID = UUID.randomUUID().toString();
        this.payload = payload;
        this.dummyReplyHandler = dummyReplyHandler;
    }

    public DummyMessageImpl(String origin, JsonObject payload, Handler<Message<R>> dummyReplyHandler) {
        this.origin = origin;
        this.statusCode = 200;
        this.correlationID = UUID.randomUUID().toString();
        this.payload = payload;
        this.dummyReplyHandler = dummyReplyHandler;
    }

    public DummyMessageImpl(String origin, Integer statusCode, String correlationID, JsonObject payload, Handler<Message<R>> dummyReplyHandler) {
        this.origin = origin;
        this.statusCode = statusCode;
        this.correlationID = correlationID;
        this.payload = payload;
        this.dummyReplyHandler = dummyReplyHandler;
    }


    public void reply(Integer statusCode) {
        reply(statusCode, null, null, null);
    }

    @Override
    public void reply(JsonObject payload) {
        reply(200, payload, null, null);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload) {
        reply(statusCode, payload, null, null);
    }

    @Override
    public void reply(Integer statusCode, String errorMessage) {
        reply(statusCode, new JsonObject().put("message", errorMessage), null, null);
    }

    @Override
    public void reply(Integer statusCode, Handler<Message<R>> replyHandler) {
        reply(statusCode, null, null, replyHandler);
    }

    @Override
    public void reply(JsonObject payload, Handler<Message<R>> replyHandler) {
        reply(200, payload, null, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, Handler<Message<R>> replyHandler) {
        reply(statusCode, payload, null, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options) {
        reply(statusCode, payload, options, null);
    }

    @Override
    public void reply(JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        reply(200, payload, options, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        if(dummyReplyHandler != null) {
            dummyReplyHandler.handle(new DummyMessageImpl<R>(origin, statusCode, correlationID, payload, replyHandler));
        }
    }

    @Override
    public void fail(String errorMessage) {
        fail(500, errorMessage, null);
    }

    @Override
    public void fail(Integer errorCode, String errorMessage) {
        fail(errorCode, errorMessage, null);
    }

    @Override
    public void fail(Integer errorCode, String errorMessage, Throwable cause) {
        String errorCause = cause == null? "unknown" : cause.getMessage();

        reply(errorCode, new JsonObject().put("message", errorMessage).put("errorCode", errorCode).put("errorCause", errorCause), null, null);
    }

    @Override
    public Object body() {
        return payload;
    }

    @Override
    public String getMessage() {
        return payload.getString("message");
    }

    @Override
    public JsonObject getBodyAsJsonObject() {
        return payload;
    }

    @Override
    public Integer statusCode() {
        return statusCode;
    }

    @Override
    public String correlationID() {
        return correlationID;
    }

    @Override
    public String origin() {
        return origin;
    }

    @Override
    public String userID() {
        return "";
    }

    @Override
    public JsonArray userRoles() {
        return null;
    }

    @Override
    public String sessionAddress() {
        return null;
    }

    @Override
    public Message<R> result() {
        return this;
    }

    @Override
    public Throwable cause() {
        return cause;
    }

    @Override
    public boolean succeeded() {
        return succeeded;
    }

    @Override
    public boolean failed() {
        return !succeeded;
    }
}
