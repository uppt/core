package de.maltebehrendt.uppt.logging;

/**
 * Created by malte on 22.10.16.
 */

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.RootLoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.apache.logging.log4j.core.config.plugins.util.PluginManager;


import java.net.URI;

public class Log4j2ConfigurationFactory  extends ConfigurationFactory {

    private static JsonObject loggingSettings = null;
    private static String instanceID = null;

    public static void setConfiguration(JsonObject appenderSettings, String instance) {
        loggingSettings = appenderSettings;
        instanceID = instance;
    }

    static Configuration createConfiguration(final String name, ConfigurationBuilder<BuiltConfiguration> builder) {
        if(loggingSettings == null) {
            loggingSettings = new JsonObject()
                    .put("console", new JsonObject()
                            .put("level", "WARN"))
                    .put("file", new JsonObject()
                            .put("level", "WARN")
                            .put("location", "instance.log"))
                    .put("graylog", new JsonObject());
        }

        builder.setConfigurationName(name)
                .setStatusLevel(Level.ERROR);

        // apply settings from config
        if(loggingSettings.getJsonObject("console").getBoolean("isEnabled")) {
            AppenderComponentBuilder appenderComponentBuilder = builder.newAppender("Stdout", "CONSOLE")
                    .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
                    .add(builder.newLayout("PatternLayout").addAttribute("pattern", "%d [" +instanceID + ":%t] %-5level: %msg%n%throwable"))
                    .add(builder.newFilter("MarkerFilter", Filter.Result.DENY,Filter.Result.NEUTRAL).addAttribute("marker", "FLOW"));


            builder.add(appenderComponentBuilder);
        }
        if(loggingSettings.getJsonObject("file").getBoolean("isEnabled")) {
            JsonObject fileSettings = loggingSettings.getJsonObject("file");

            AppenderComponentBuilder appenderComponentBuilder = builder.newAppender("File", "FILE")
                    .addAttribute("fileName", fileSettings.getString("location"))
                    .add(builder.newLayout("PatternLayout").addAttribute("pattern", "%d [" +instanceID + ":%t] %-5level: %msg%n%throwable"))
                    .add(builder.newFilter("MarkerFilter", Filter.Result.DENY,Filter.Result.NEUTRAL).addAttribute("marker", "FLOW"));

            builder.add(appenderComponentBuilder);
        }
        if(loggingSettings.getJsonObject("graylog").getBoolean("isEnabled")) {
            JsonObject graylogSettings = loggingSettings.getJsonObject("graylog");

            PluginManager.addPackage("org.graylog2.log4j2");
            AppenderComponentBuilder appenderComponentBuilder = builder.newAppender("Graylog2", "GELF")
                    .addAttribute("server", graylogSettings.getString("server"))
                    .addAttribute("port", graylogSettings.getInteger("port"))
                    .addAttribute("protocol", graylogSettings.getString("protocol"))
                    .addAttribute("hostName", instanceID)
                    .addAttribute("includeSource", graylogSettings.getBoolean("includeSource"))
                    .addAttribute("includeStackTrace", graylogSettings.getBoolean("includeStackTrace"))
                    .addAttribute("includeExceptionCause", graylogSettings.getBoolean("includeExceptionCause"))
                    .add(builder.newLayout("PatternLayout").addAttribute("pattern", "%d [" +instanceID + ":%t] %-5level: %msg%n%throwable"))
                    .add(builder.newFilter("MarkerFilter", Filter.Result.DENY,Filter.Result.NEUTRAL).addAttribute("marker", "FLOW"));

            builder.add(appenderComponentBuilder);
        }

        // add all appenders to the root logger
        RootLoggerComponentBuilder rootLoggerComponentBuilder = builder.newRootLogger(Level.DEBUG);
        if(loggingSettings.getJsonObject("console").getBoolean("isEnabled")) {
            rootLoggerComponentBuilder.add(builder.newAppenderRef("Stdout").addAttribute("level", loggingSettings.getJsonObject("console").getString("level")));
        }
        if(loggingSettings.getJsonObject("file").getBoolean("isEnabled")) {
            rootLoggerComponentBuilder.add(builder.newAppenderRef("File").addAttribute("level", loggingSettings.getJsonObject("file").getString("level")));
        }
        if(loggingSettings.getJsonObject("graylog").getBoolean("isEnabled")) {
            rootLoggerComponentBuilder.add(builder.newAppenderRef("Graylog2").addAttribute("level", loggingSettings.getJsonObject("graylog").getString("level")));
        }
        builder.add(rootLoggerComponentBuilder);

        return builder.build();
    }

    @Override
    public Configuration getConfiguration(LoggerContext loggerContext, ConfigurationSource source) {
        return getConfiguration(loggerContext, source.toString(), null);
    }

    @Override
    public Configuration getConfiguration(final LoggerContext loggerContext, final String name, final URI configLocation) {
        ConfigurationBuilder<BuiltConfiguration> builder = newConfigurationBuilder();
        return createConfiguration(name, builder);
    }

    @Override
    protected String[] getSupportedTypes() {
        return new String[] {"*"};
    }
}
