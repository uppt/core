package de.maltebehrendt.uppt.logging;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class LoggerProxy {
    private Logger logger = null;
    private String serviceName = null;

    public LoggerProxy(Class serviceClass) {
        logger = LoggerFactory.getLogger(serviceClass);
        serviceName = serviceClass.getSimpleName();
    }

    public void security(String correlationID, String source, String userID, Integer errorCode, String message, Throwable throwable) {
        logger.error(correlationID + "@" + serviceName + " [" + source + "][" + userID + "] " + errorCode + ":" + message, throwable);
    }
    public void security(String correlationID, String source, String userID, Integer errorCode, String message) {
        logger.error(correlationID + "@" + serviceName + " [" + source + "[" + userID + "] " + errorCode + ":" + message);
    }
    public void security(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        security(correlationID, source, "unknown", errorCode, message, throwable);
    }
    public void security(String correlationID, String source, Integer errorCode, String message) {
        security(correlationID, source, "unknown", errorCode, message);
    }
    public void security(String correlationID, Integer errorCode, String message, Throwable throwable) {
        security(correlationID, serviceName, "unknown", errorCode, message, throwable);
    }
    public void security(String correlationID, Integer errorCode, String message) {
        security(correlationID, serviceName, "unknown", errorCode, message);
    }
    public void security(String correlationID, String message, Throwable throwable) {
        security(correlationID, serviceName, "unknown", 500, message, throwable);
    }
    public void security(String correlationID, String message) {
        security(correlationID, serviceName, "unknown",  500, message);
    }

    public void settings(String variableName, Object defaultValue) {
        logger.warn(variableName + " of " + serviceName + " configuration invalid or missing. Defaulting to:" + defaultValue);
    }
    public void settings(String variableName, Integer errorCode, String message, Throwable throwable) {
        logger.warn(variableName + " of " + serviceName + " configuration caused error " + errorCode + ":" + message, throwable);
    }
    public void settings(String variableName, Integer errorCode, String message) {
        logger.warn(variableName + " of " + serviceName + " configuration caused error " + errorCode + ":" + message);
    }
    public void settings(String variableName, String message, Throwable throwable) {
        settings(variableName, 900, message, throwable);
    }


    public void fatal(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.fatal(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void fatal(String correlationID, String source, Integer errorCode, String message) {
        logger.fatal(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void fatal(String correlationID, Integer errorCode, String message, Throwable throwable) {
        fatal(correlationID, serviceName, errorCode, message, throwable);
    }
    public void fatal(String correlationID, Integer errorCode, String message) {
        fatal(correlationID, serviceName, errorCode, message);
    }
    public void fatal(String correlationID, String message, Throwable throwable) {
        fatal(correlationID, serviceName, 500, message, throwable);
    }
    public void fatal(String correlationID, String message) {
        fatal(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void fatal(Object message) {
        logger.fatal(message);
    }
    @Deprecated
    public void fatal(Object message, Throwable throwable) {
        logger.fatal(message, throwable);
    }


    public void error(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.error(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void error(String correlationID, String source, Integer errorCode, String message) {
        logger.error(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void error(String correlationID, Integer errorCode, String message, Throwable throwable) {
        error(correlationID, serviceName, errorCode, message, throwable);
    }
    public void error(String correlationID, Integer errorCode, String message) {
        error(correlationID, serviceName, errorCode, message);
    }
    public void error(String correlationID, String message, Throwable throwable) {
        error(correlationID, serviceName, 500, message, throwable);
    }
    public void error(String correlationID, String message) {
        error(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void error(Object message) {
        logger.error(message);
    }
    @Deprecated
    public void error(Object message, Throwable throwable) {
        logger.error(message, throwable);
    }
    @Deprecated
    public void error(Object message, Object ... objects) {
        logger.error(message, objects);
    }
    @Deprecated
    public void error(Object message, Throwable throwable, Object ... objects) {
        logger.error(message, throwable, objects);
    }


    public void warn(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.warn(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void warn(String correlationID, String source, Integer errorCode, String message) {
        logger.warn(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void warn(String correlationID, Integer errorCode, String message, Throwable throwable) {
        warn(correlationID, serviceName, errorCode, message, throwable);
    }
    public void warn(String correlationID, Integer errorCode, String message) {
        warn(correlationID, serviceName, errorCode, message);
    }
    public void warn(String correlationID, String message, Throwable throwable) {
        warn(correlationID, serviceName, 500, message, throwable);
    }
    public void warn(String correlationID, String message) {
        warn(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void warn(Object message) {
        logger.warn(message);
    }
    @Deprecated
    public void warn(Object message, Throwable throwable) {
        logger.warn(message, throwable);
    }
    @Deprecated
    public void warn(Object message, Object ... objects) {
        logger.warn(message, objects);
    }
    @Deprecated
    public void warn(Object message, Throwable throwable, Object ... objects) {
        logger.warn(message, throwable, objects);
    }


    public void info(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.info(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void info(String correlationID, String source, Integer errorCode, String message) {
        logger.info(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void info(String correlationID, Integer errorCode, String message, Throwable throwable) {
        info(correlationID, serviceName, errorCode, message, throwable);
    }
    public void info(String correlationID, Integer errorCode, String message) {
        info(correlationID, serviceName, errorCode, message);
    }
    public void info(String correlationID, String message, Throwable throwable) {
        info(correlationID, serviceName, 500, message, throwable);
    }
    public void info(String correlationID, String message) {
        info(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void info(Object message) {
        logger.info(message);
    }
    @Deprecated
    public void info(Object message, Throwable throwable) {
        logger.info(message, throwable);
    }
    @Deprecated
    public void info(Object message, Object ... objects) {
        logger.info(message, objects);
    }
    @Deprecated
    public void info(Object message, Throwable throwable, Object ... objects) {
        logger.info(message, throwable, objects);
    }


    public void debug(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.debug(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void debug(String correlationID, String source, Integer errorCode, String message) {
        logger.debug(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void debug(String correlationID, Integer errorCode, String message, Throwable throwable) {
        debug(correlationID, serviceName, errorCode, message, throwable);
    }
    public void debug(String correlationID, Integer errorCode, String message) {
        debug(correlationID, serviceName, errorCode, message);
    }
    public void debug(String correlationID, String message, Throwable throwable) {
        debug(correlationID, serviceName, 500, message, throwable);
    }
    public void debug(String correlationID, String message) {
        debug(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void debug(Object message) {
        logger.debug(message);
    }
    @Deprecated
    public void debug(Object message, Throwable throwable) {
        logger.debug(message, throwable);
    }
    @Deprecated
    public void debug(Object message, Object ... objects) {
        logger.debug(message, objects);
    }
    @Deprecated
    public void debug(Object message, Throwable throwable, Object ... objects) {
        logger.debug(message, throwable, objects);
    }


    public void trace(String correlationID, String source, Integer errorCode, String message, Throwable throwable) {
        logger.debug(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message, throwable);
    }
    public void trace(String correlationID, String source, Integer errorCode, String message) {
        logger.debug(correlationID + "@" + serviceName + " [" + source + "] " + errorCode + ":" + message);
    }
    public void trace(String correlationID, Integer errorCode, String message, Throwable throwable) {
        trace(correlationID, serviceName, errorCode, message, throwable);
    }
    public void trace(String correlationID, Integer errorCode, String message) {
        trace(correlationID, serviceName, errorCode, message);
    }
    public void trace(String correlationID, String message, Throwable throwable) {
        trace(correlationID, serviceName, 500, message, throwable);
    }
    public void trace(String correlationID, String message) {
        trace(correlationID, serviceName, 500, message);
    }
    @Deprecated
    public void trace(Object message) {
        logger.trace(message);
    }
    @Deprecated
    public void trace(Object message, Throwable throwable) {
        logger.trace(message, throwable);
    }
    @Deprecated
    public void trace(Object message, Object ... objects) {
        logger.trace(message, objects);
    }
    @Deprecated
    public void trace(Object message, Throwable throwable, Object ... objects) {
        logger.trace(message, throwable, objects);
    }


    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    public boolean isTraceEnabled() {
        return logger.isTraceEnabled();
    }
}
