package de.maltebehrendt.uppt.enums;

/**
 * Created by malte on 13.02.17.
 */
public enum Operation {
    GET,
    SET,
    DEL
}
