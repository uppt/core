package de.maltebehrendt.uppt.enums;

/**
 * Created by malte on 31.10.16.
 */
public enum State {
    AVAILABLE, UNKNOWN, UNAVAILABLE
}
