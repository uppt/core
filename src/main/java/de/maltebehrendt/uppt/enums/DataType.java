package de.maltebehrendt.uppt.enums;

/**
 * Created by malte on 29.10.16.
 */
public enum DataType {
    STRING,
    INTEGER,
    LONG,
    DOUBLE,
    DATELONG,
    JSONObject,
    JSONArray,
    BOOLEAN,
    PREVIOUS_VALUE,
    ASSERT
}
