package de.maltebehrendt.uppt;

import com.amazonaws.auth.BasicAWSCredentials;
import com.hazelcast.config.Config;
import de.maltebehrendt.uppt.database.Database;
import de.maltebehrendt.uppt.database.impl.ArangoDBImpl;
import de.maltebehrendt.uppt.logging.Log4j2ConfigurationFactory;
import de.maltebehrendt.uppt.storage.Impl.StorageBackend;
import de.maltebehrendt.uppt.storage.Storage;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBusOptions;
import io.vertx.core.http.ClientAuth;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.net.JksOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import io.vertx.spi.cluster.ignite.IgniteClusterManager;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.gce.TcpDiscoveryGoogleStorageIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.s3.TcpDiscoveryS3IpFinder;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.apache.ignite.ssl.SslContextFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipInputStream;

public class InstanceRunner {

    private static Logger logger = null;
    private static Vertx vertx = null;
    private static String instanceID = null;
    private static String configDirectory = null;

    public static void main(String[] args) {

            if(args != null && args.length == 1) {
                configDirectory = args[0] + File.separator;
            }

            init(new JsonArray());
    }

    private static void init(JsonArray serviceWhitelist) {
        if(vertx != null) {
            return;
        }

        if(instanceID == null) {
            instanceID = getInstanceID();
        }

        // default to testing configuration, if available
        // otherwise use default production config location
        if(configDirectory == null) {
            configDirectory = "config" + File.separator + "junit" + File.separator + "instanceRunnerTest" + File.separator + "local" + File.separator + "config" + File.separator ;

            File configFolder = new File(configDirectory);

            if(configFolder.exists() && configFolder.isDirectory()) {
                System.err.println("WARN: no config folder provided - defaulting to TEST config at " + configDirectory);
            }
            else {
                configDirectory = "config" + File.separator;
                configFolder = new File(configDirectory);

                if(configFolder.exists() && configFolder.isDirectory()) {
                    System.err.println("WARN: no config folder provided - defaulting to PRODUCTIVE config at " + configDirectory);
                }
                else {
                    System.err.println("FATAL: no config folder provided or detected! Aborting....");
                    System.exit(-1);
                }
            }

        }

        try {
            // read the systemConfig synchronously...
            JsonObject systemConfig = new JsonObject(new String(Files.readAllBytes(Paths.get(configDirectory + "system.json"))));

            if(systemConfig == null || systemConfig.isEmpty() || !systemConfig.containsKey("logging") || !systemConfig.containsKey("clustering")) {
                System.out.println("FATAL: invalid or incomplete system config file: " + configDirectory + "system.json! Aborting....");
                System.exit(-1);
            }

            // setup logging
            if(logger == null) {
                setupLogging(systemConfig.getJsonObject("logging"));
            }

            // startup the vertx instance
            setupClustering(systemConfig.getJsonObject("clustering"), result -> {
                if(result.failed()) {
                    logger.fatal("Failed to setup clustering/start up vertx instance!", result.cause());
                    System.exit(-1);
                }
                vertx = result.result();

                // check config DB
                JsonObject databaseConfig = systemConfig.getJsonObject("database");
                if(databaseConfig == null || databaseConfig.isEmpty()) {
                    logger.warn("NO DATABASE CONFIG provided in system.json - therefore services will throw error or STATEFUL operation only");
                }

                // check config storage
                JsonObject storageConfig = systemConfig.containsKey("storage")? systemConfig.getJsonObject("storage") : new JsonObject();
                if(storageConfig.isEmpty()) {
                    systemConfig.put("storage", storageConfig);
                    logger.warn("Storage config: 'storage' not set/empty in system.json - therefore services will throw error or STATEFUL operation only. Using defaults.");
                }

                if(!storageConfig.containsKey("defaultStoragePath")) {
                    Path rootPath = Paths.get("storage").toAbsolutePath();
                    storageConfig.put("defaultStoragePath", rootPath.toString());
                    logger.warn("Storage config: 'defaultStoragePath' in 'storage' (system.json) not set, defaulting to " + rootPath + ". Unless services have set own separate storage configurations in their config files, everything will be stored there.");
                }
                if(!storageConfig.containsKey("sharedStorage") || !storageConfig.getJsonObject("sharedStorage").containsKey("path")) {
                    JsonObject sharedStorage = new JsonObject();
                    String path = storageConfig.getString("defaultStoragePath") + File.separator + "shared";
                    sharedStorage.put("path", path);
                    storageConfig.put("sharedStorage", sharedStorage);
                    logger.warn("Storage config: 'sharedStorage' in 'storage' (system.json) not set, defaulting sharedStorage.path to " + path);
                }
                if(!storageConfig.containsKey("usersStorage") || !storageConfig.getJsonObject("usersStorage").containsKey("path")) {
                    JsonObject usersStorage = new JsonObject();
                    String path = storageConfig.getString("defaultStoragePath") + File.separator + "users";
                    usersStorage.put("path", path);
                    storageConfig.put("usersStorage", usersStorage);
                    logger.warn("Storage config: 'usersStorage' in 'storage' (system.json) not set, defaulting usersStorage.path to " + path);
                }
                if(!storageConfig.containsKey("publicStorage") || !storageConfig.getJsonObject("publicStorage").containsKey("path")) {
                    JsonObject publicStorage = new JsonObject();
                    String path = storageConfig.getString("defaultStoragePath") + File.separator + "webroot";
                    publicStorage.put("path", path);
                    storageConfig.put("publicStorage", publicStorage);
                    logger.warn("Storage config: 'publicStorage' in 'storage' (system.json) not set, defaulting publicStorage.path to " + path);
                }

                // get all the service's config files and configure db/public files (as it's only necessary once per instance)
                List<Future> dataFutures = new LinkedList<>();
                JsonArray serviceConfigs = new JsonArray();
                try {
                    for (File file : new File(configDirectory + "services").listFiles()) {
                        if (!file.getName().endsWith(".json")) {
                            continue;
                        }
                        if ((serviceWhitelist != null && !serviceWhitelist.contains(file.getName()))) {
                            logger.info("Skipping " + file.getName() + " as it's not in the provided whitelist.");
                            continue;
                        }

                        JsonObject serviceConfig = new JsonObject(new String(Files.readAllBytes(file.toPath())));
                        if (!serviceConfig.getBoolean("isEnabled") || !serviceConfig.containsKey("serviceLocation") || !serviceConfig.containsKey("serviceInstances")) {
                            logger.info("Skipping config file " + file.getPath() + ": either isEnabled = false or 'serviceLocation' or 'serviceInstances' not provided");
                            continue;
                        }

                        JsonObject serviceDatabaseConfig = serviceConfig.containsKey("database") ? serviceConfig.getJsonObject("database") : new JsonObject();
                        if(serviceDatabaseConfig.isEmpty()) {
                            serviceConfig.put("database", serviceDatabaseConfig);
                        }

                        if(!serviceDatabaseConfig.containsKey("isDatabaseDisabled") || serviceDatabaseConfig.getBoolean("isDatabaseDisabled") == false) {
                            if(databaseConfig != null) {
                                for (String key : databaseConfig.fieldNames()) {
                                    if (!serviceDatabaseConfig.containsKey(key)) {
                                        serviceDatabaseConfig.put(key, databaseConfig.getValue(key));
                                    }
                                }
                            }
                            serviceDatabaseConfig.put("isDatabaseDisabled", false);
                        }
                        else {
                            serviceDatabaseConfig.put("isDatabaseDisabled", true);
                        }

                        JsonObject serviceStorageConfig = serviceConfig.containsKey("storage") ? serviceConfig.getJsonObject("storage") : new JsonObject();
                        if(serviceStorageConfig.isEmpty()) {
                            serviceConfig.put("storage", serviceStorageConfig);
                        }
                        if(!serviceStorageConfig.containsKey("isStorageDisabled") || serviceStorageConfig.getBoolean("isStorageDisabled") == false) {
                            serviceStorageConfig.put("sharedStorage", storageConfig.getJsonObject("sharedStorage"));
                            serviceStorageConfig.put("usersStorage", storageConfig.getJsonObject("usersStorage"));
                            serviceStorageConfig.put("publicStorage", storageConfig.getJsonObject("publicStorage"));

                            if(!serviceStorageConfig.containsKey("ownStorage")) {
                                String serviceLocation = serviceConfig.getString("serviceLocation");
                                String path = storageConfig.getString("defaultStoragePath") + File.separator + "services" + File.separator + serviceLocation;
                                JsonObject ownStorage = new JsonObject()
                                        .put("path", path);
                                serviceStorageConfig.put("ownStorage", ownStorage);
                                logger.info("Storage config: service configured in " + file.getName() + " does not define own storage config. Defaulting its storage to: " + path);
                            }
                            serviceStorageConfig.put("isStorageDisabled", false);
                        }
                        else {
                            serviceStorageConfig.put("isStorageDisabled", true);
                        }

                        serviceConfig.put("serviceConfigFile", file.getPath());
                        serviceConfigs.add(serviceConfig);

                        final String serviceLocation = serviceConfig.getString("serviceLocation");

                        // take care of the service's database setup
                        if (serviceDatabaseConfig.getBoolean("isDatabaseDisabled") == false) {
                            Future databaseFuture = Future.future();
                            dataFutures.add(databaseFuture);

                            // only once per cluster
                            vertx.sharedData().getLock(serviceLocation + "_dbConfig", lockHandler -> {
                                // prepare the database
                                vertx.executeBlocking(handler -> {
                                    try {
                                        Database database = new ArangoDBImpl(serviceLocation, serviceDatabaseConfig);

                                        if (!database.exists()) {
                                            if (!database.createBlocking()) {
                                                logger.error("Creating database for " + serviceLocation + " failed!");
                                                handler.fail("Creating database for " + serviceLocation + " failed!");
                                            }

                                            // apply template, if available/configured
                                            if (serviceDatabaseConfig.containsKey("templateScriptPath")) {
                                                String templateScriptPath = serviceDatabaseConfig.getString("templateScriptPath");
                                                logger.info("Applying template database script " + templateScriptPath + " for service " + serviceLocation);

                                                vertx.executeBlocking(blockingImport -> {
                                                    database.importScriptFromFile(configDirectory + templateScriptPath, blockingImport);
                                                }, importResult -> {
                                                    if (importResult.failed()) {
                                                        handler.fail(importResult.cause());
                                                    } else {
                                                        handler.complete();
                                                    }
                                                    database.close();
                                                });

                                            } else {
                                                database.close();
                                                handler.complete();
                                            }
                                        } else {
                                            // assuming the database is already set up completely
                                            handler.complete();
                                        }
                                    } catch (Exception e) {
                                        logger.fatal("Failed to setup database for " + serviceLocation, e);
                                        handler.fail(e);
                                    }
                                    finally {
                                        lockHandler.result().release();
                                    }
                                }, databaseFuture);
                            });
                        }
                        else {
                            serviceConfig.put("database", new JsonObject());
                            serviceConfig.getJsonObject("database").put("isDatabaseDisabled", true);
                        }

                        // take care of public resources
                        if(!serviceConfig.containsKey("hasPublicFiles")) {
                            serviceConfig.put("hasPublicFiles", false);
                        }
                        if (serviceConfig.getBoolean("hasPublicFiles") == true && serviceConfig.containsKey("publicFiles")) {
                            Future publicFilesFuture = Future.future();
                            dataFutures.add(publicFilesFuture);

                            // only once at a time per cluster - does not necessarily prevent overwriting multiple times except for the timestamp below but at least stretches the I/O-load
                            vertx.sharedData().getLock(serviceLocation + "_publicFilesSetup", lockHandler -> {
                                try {
                                    StorageBackend storageBackend = Storage.getBackend(storageConfig.getJsonObject("publicStorage"), vertx);
                                    List<Future> extractionFutures = new LinkedList();

                                    JsonObject publicResourcesConfig = serviceConfig.getJsonObject("publicFiles");
                                    for (String fileName : publicResourcesConfig.fieldNames()) {
                                        Future<JsonObject> extractFuture = Future.future();
                                        extractionFutures.add(extractFuture);
                                        JsonObject fileConfig = publicResourcesConfig.getJsonObject(fileName);
                                        String filePath = fileConfig.getString("filePath");
                                        String relativeExtractionPath = fileConfig.getString("relativeExtractionPath");

                                        Future<JsonObject> checkFuture = Future.future();
                                        checkFuture.setHandler(handler -> {
                                            Boolean overwrite;
                                            if (handler.failed()) {
                                                // target directory probably does not exist, thus extract and overwrite
                                                overwrite = true;
                                            }
                                            else {
                                                Long creationTime = handler.result().getLong("creationTime");
                                                creationTime = creationTime == null ? 0L : creationTime;

                                                if (creationTime + 60000L < System.currentTimeMillis()) {
                                                    // is older than one minute, overwrite in case something changed
                                                    overwrite = true;
                                                }
                                                else {
                                                    overwrite = false;
                                                }
                                            }
                                            try {
                                                ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(filePath));
                                                storageBackend.unzipAndPersist(relativeExtractionPath, overwrite, zipInputStream, 250L, 5000L, extractFuture);
                                            } catch (FileNotFoundException e) {
                                                extractFuture.fail("Defined public resource file " + filePath + " defined in " + file.getName() + " not found or not accessible!");
                                            }
                                        });
                                        storageBackend.retrieveMetaData(relativeExtractionPath, checkFuture);
                                    }

                                    CompositeFuture.all(extractionFutures).setHandler(extractionHandler -> {
                                       if(extractionHandler.failed()) {
                                           publicFilesFuture.fail(extractionHandler.cause());
                                           return;
                                       }

                                       publicFilesFuture.complete();
                                    });
                                }
                                catch(IOException e) {
                                    publicFilesFuture.fail("Probably getting the storage backend failed. Check logs and config: " + e.getMessage());
                                }
                                catch(Exception e) {
                                    publicFilesFuture.fail(e);
                                }
                                finally {
                                    lockHandler.result().release();
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    logger.fatal("Invalid service config file(s)! Probably there is an issue with a storage definition.", e);
                    System.exit(-1);
                }

                CompositeFuture.all(dataFutures).setHandler(databaseResult -> {
                    if(databaseResult.failed()) {
                        logger.fatal("Failed to setup database!", databaseResult.cause());
                        System.exit(-1);
                    }

                    if(serviceConfigs.isEmpty()) {
                        logger.fatal("Failed: identifying services to start! Make sure to place service config files in: " + configDirectory + "services");
                        System.exit(-1);
                    }

                    // deploy services according to configuration...
                    List<Future> deploymentFutures = new LinkedList<>();
                    for(int i=0;i<serviceConfigs.size();i++) {
                        Future<String> deploymentFuture = Future.future();
                        deploymentFutures.add(deploymentFuture);
                        deployVerticle(serviceConfigs.getJsonObject(i), deploymentFuture);
                    }

                    CompositeFuture.all(deploymentFutures).setHandler(deploymentResult -> {
                        if(deploymentResult.failed()) {
                            logger.fatal("Failed: deployment of services (verticles) on instance " + instanceID, deploymentResult.cause());
                            System.exit(-1);
                        }

                        logger.info("Success: all services (verticles) of instance " + instanceID + " are deployed!");

                        DeliveryOptions options = new DeliveryOptions()
                                .addHeader("statusCode", "200")
                                .addHeader("correlationID", UUID.randomUUID().toString())
                                .addHeader("origin", "Instance: " + instanceID);

                        vertx.eventBus().publish("system.1.newInstance", new JsonObject().put("origin", instanceID), options);
                    });
                });
            });
        } catch (IOException e) {
            if(logger != null ) {
                logger.fatal("Invalid config file(s)! Make sure to place it at the relative path \"" + configDirectory + "system.json\" and in the directory 'services' for each service", e);
            }
            System.err.println("Invalid config file! Make sure to place it at the relative path \"" + configDirectory + "system.json\" and in the directory 'services' for each service");
            e.printStackTrace();
            System.exit(-1);
        } catch(Exception e) {
            if(logger != null) {
                logger.fatal("Failed to startup instance " + instanceID +"!", e);
            }
            System.err.println("Failed to startup instance " + instanceID +"!");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void deployVerticle(JsonObject serviceConfig, Future<String> deploymentFuture) {
        if(serviceConfig == null || serviceConfig.isEmpty()) {
            deploymentFuture.fail("Missing a service config JSON Object (serviceLocation, serviceInstances, serviceConfigFile)!");
            return;
        }

        String serviceLocation = serviceConfig.getString("serviceLocation");
        Integer serviceInstances = serviceConfig.getInteger("serviceInstances");
        String serviceConfigFile = serviceConfig.getString("serviceConfigFile");

        if(serviceLocation == null || serviceLocation.isEmpty()) {
            deploymentFuture.fail("Service config JSON Object has no entry serviceLocation!");
            return;
        }
        if(serviceInstances == null || serviceInstances < 1) {
            deploymentFuture.fail("Service config JSON Object has no valid entry serviceInstances!");
            return;
        }


        logger.info("Deploying: " + serviceInstances + " * " + serviceLocation + " with config from " + serviceConfigFile + " on instance " + instanceID);

        DeploymentOptions deploymentOptions = new DeploymentOptions()
                .setInstances(serviceInstances)
                .setConfig(serviceConfig);

        vertx.deployVerticle(serviceLocation, deploymentOptions, deploymentFuture.completer());
    }

    private static void setupClustering(JsonObject clusteringSettings, Handler<AsyncResult<Vertx>> handler) {
        VertxOptions options = new VertxOptions()
                .setHAEnabled(false);
                //.setBlockedThreadCheckInterval(1200000) // TODO move into system.json, is 20m currently
               // .setMaxWorkerExecuteTime(1200000); // TODO move into system.json, is 20m currently

        // enable SSL encryption of the event bus, if desired
        Boolean isSSLEnabled = clusteringSettings.getBoolean("isSSLEnabled");
        if(isSSLEnabled) {
            options.setEventBusOptions(new EventBusOptions()
                    .setSsl(true)
                    .setKeyStoreOptions(new JksOptions()
                            .setPath(clusteringSettings.getJsonObject("ssl").getString("keystorePath"))
                            .setPassword(clusteringSettings.getJsonObject("ssl").getString("keystorePassword")))
                    .setTrustStoreOptions(new JksOptions()
                            .setPath(clusteringSettings.getJsonObject("ssl").getString("truststorePath"))
                            .setPassword(clusteringSettings.getJsonObject("ssl").getString("truststorePassword")))
                    .setClientAuth(ClientAuth.REQUIRED));

                /*
                    keytool -genkey -alias upptExample -keyalg RSA -keystore eventBusKeystore.jks -keysize 2048 -validity 365 -storepass strongPassword
                    keytool -export -alias keystore -file upptService.crt -keystore eventBusKeystore.jks -storepass strongPassword
                    keytool -import -trustcacerts -alias eventBusTruststore -file upptService.crt -keystore eventBusTruststore.jks -storepass strongPassword
                 */
        }


        if("ignite".equalsIgnoreCase(clusteringSettings.getString("clusterManager"))) {
            JsonObject modeSettings = clusteringSettings.getJsonObject("ignite").getJsonObject("modeConfig");
            IgniteConfiguration igniteConfiguration = new IgniteConfiguration();


            if(isSSLEnabled) {
                SslContextFactory sslContextFactory = new SslContextFactory();
                sslContextFactory.setKeyStoreFilePath(clusteringSettings.getJsonObject("ssl").getString("keystorePath"));
                sslContextFactory.setKeyStorePassword(clusteringSettings.getJsonObject("ssl").getString("keystorePassword").toCharArray());
                sslContextFactory.setTrustStoreFilePath(clusteringSettings.getJsonObject("ssl").getString("truststorePath"));
                sslContextFactory.setTrustStorePassword(clusteringSettings.getJsonObject("ssl").getString("truststorePassword").toCharArray());
                igniteConfiguration.setSslContextFactory(sslContextFactory);
            }

            // configure the node discovery
            TcpDiscoverySpi tcpDiscoverySpi = new TcpDiscoverySpi();
            String clusterHost = getClusterHost(clusteringSettings.getString("clusterInterfacePrefix"));
            switch(clusteringSettings.getJsonObject("ignite").getString("mode")) {
                case "multicast":
                    TcpDiscoveryMulticastIpFinder tcpDiscoveryMulticastIpFinder = new TcpDiscoveryMulticastIpFinder();
                    tcpDiscoveryMulticastIpFinder.setMulticastGroup(modeSettings.getJsonObject("multicast").getString("group"));
                    tcpDiscoveryMulticastIpFinder.setLocalAddress(clusterHost);
                    tcpDiscoverySpi.setIpFinder(tcpDiscoveryMulticastIpFinder);
                    break;
                case "tcpip":
                    TcpDiscoveryVmIpFinder tcpDiscoveryVmIpFinder = new TcpDiscoveryVmIpFinder();
                    tcpDiscoveryVmIpFinder.setAddresses(modeSettings.getJsonObject("tcpip").getJsonArray("members").getList());
                    tcpDiscoverySpi.setIpFinder(tcpDiscoveryVmIpFinder);
                    break;
                case "aws":
                    TcpDiscoveryS3IpFinder tcpDiscoveryS3IpFinder = new TcpDiscoveryS3IpFinder();
                    tcpDiscoveryS3IpFinder.setBucketName(modeSettings.getJsonObject("aws").getString("bucket"));
                    BasicAWSCredentials awsCredentials = new BasicAWSCredentials(modeSettings.getJsonObject("aws").getString("accessKey"), modeSettings.getJsonObject("aws").getString("secretKey"));
                    tcpDiscoveryS3IpFinder.setAwsCredentials(awsCredentials);
                    tcpDiscoverySpi.setIpFinder(tcpDiscoveryS3IpFinder);
                    break;
                case "gce":
                    TcpDiscoveryGoogleStorageIpFinder tcpDiscoveryGoogleStorageIpFinder = new TcpDiscoveryGoogleStorageIpFinder();
                    tcpDiscoveryGoogleStorageIpFinder.setBucketName(modeSettings.getJsonObject("gce").getString("bucket"));
                    tcpDiscoveryGoogleStorageIpFinder.setProjectName(modeSettings.getJsonObject("gce").getString("project"));
                    tcpDiscoveryGoogleStorageIpFinder.setServiceAccountId(modeSettings.getJsonObject("gce").getString("serviceID"));
                    tcpDiscoveryGoogleStorageIpFinder.setServiceAccountP12FilePath(modeSettings.getJsonObject("gce").getString("serviceP12Path"));
                    tcpDiscoverySpi.setIpFinder(tcpDiscoveryGoogleStorageIpFinder);
                    break;
            }

            igniteConfiguration.setDiscoverySpi(tcpDiscoverySpi);
            ClusterManager clusterManager = new IgniteClusterManager(igniteConfiguration);

            // find out which network interface is being used for clustering/eventbus so that Vertx isn't using the wrong one...
            options.setClusterHost(getClusterHost(clusteringSettings.getString("clusterInterfacePrefix")));
            options.setClusterPort(clusteringSettings.getInteger("clusterPort"));

            logger.info("Starting up: instance " + instanceID + " with Apache Ignite clustering and EventBus encryption is " + isSSLEnabled);
            options.setClusterManager(clusterManager);
            Vertx.clusteredVertx(options, handler);
        }
        else if("hazelcast".equalsIgnoreCase(clusteringSettings.getString("clusterManager"))) {
            JsonObject modeSettings = clusteringSettings.getJsonObject("hazelcast").getJsonObject("modeConfig");
            Config hazelcastConfig = new Config();

            // prepare Hazelcast configuration
            switch(clusteringSettings.getJsonObject("hazelcast").getString("mode")) {
                case "tcpip":
                    hazelcastConfig.getNetworkConfig().getJoin().getTcpIpConfig().setMembers(modeSettings.getJsonObject("tcpip").getJsonArray("members").getList());
                    hazelcastConfig.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(true);
                    break;
                case "aws":
                    JsonObject awsSettings = modeSettings.getJsonObject("aws");
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setAccessKey(awsSettings.getString("accessKey"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setSecretKey(awsSettings.getString("secretKey"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setRegion(awsSettings.getString("region"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setHostHeader(awsSettings.getString("hostHeader"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setSecurityGroupName(awsSettings.getJsonObject("aws").getString("securityGroupName"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setTagKey(awsSettings.getString("tagKey"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setTagValue(awsSettings.getString("tagValue"));
                    hazelcastConfig.getNetworkConfig().getJoin().getAwsConfig().setEnabled(true);
                    break;
                case "multicast":
                default:
                    hazelcastConfig.getNetworkConfig().getJoin().getMulticastConfig().setMulticastGroup(modeSettings.getJsonObject("multicast").getString("group"));
                    hazelcastConfig.getNetworkConfig().getJoin().getMulticastConfig().setMulticastPort(modeSettings.getJsonObject("multicast").getInteger("port"));
                    hazelcastConfig.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(true);
                    break;
            }

            // prepare vertx clustering config
            ClusterManager manager = new HazelcastClusterManager(hazelcastConfig);

            // find out which network interface is being used for clustering/eventbus so that Vertx isn't using the wrong one...
            options.setClusterHost(getClusterHost(clusteringSettings.getString("clusterInterfacePrefix")));
            options.setClusterPort(clusteringSettings.getInteger("clusterPort"));

            logger.info("Starting up: instance " + instanceID + " with hazelcast clustering in "+ clusteringSettings.getJsonObject("hazelcast").getString("mode") + " mode and EventBus encryption is " + isSSLEnabled);
            options.setClusterManager(manager);
            Vertx.clusteredVertx(options, handler);
        }
        else {
            logger.info("Starting up: instance " + instanceID + " without clustering");

            handler.handle(new AsyncResult<Vertx>() {
                @Override
                public Vertx result() {
                    return Vertx.vertx(options);
                }

                @Override
                public Throwable cause() {
                    return null;
                }

                @Override
                public boolean succeeded() {
                    return true;
                }

                @Override
                public boolean failed() {
                    return false;
                }
            });
        }
    }

    private static void setupLogging(JsonObject loggingSettings) {
        if(loggingSettings == null || loggingSettings.isEmpty() || !loggingSettings.containsKey("appenders") || !loggingSettings.containsKey("componentLevels")) {
            System.err.println("Logging configuration entries missing, empty, or incomplete! Please check your system.json!");
            System.exit(-1);
        }

        // redirect all logging from JUL and SLF4J to Log4j2
        Log4j2ConfigurationFactory configurationFactory = new Log4j2ConfigurationFactory();
        configurationFactory.setConfiguration(loggingSettings.getJsonObject("appenders"), instanceID);
        System.setProperty("java.util.logging.manager","org.apache.logging.log4j.jul.LogManager");
        ConfigurationFactory.setConfigurationFactory(configurationFactory);
        LoggerFactory.initialise();
        // get a logger for the runner
        logger = LoggerFactory.getLogger(InstanceRunner.class);

        // configure the log levels for components
        JsonObject componentLogLevels = loggingSettings.getJsonObject("componentLevels");
        for(String key : componentLogLevels.fieldNames()) {
            Configurator.setAllLevels(key, Level.getLevel(componentLogLevels.getString(key)));
        }
    }

    private static String getInstanceID() {
        if(instanceID != null) {
            return instanceID;
        }

        Map<String, String> env = System.getenv();
        if(env.containsKey("COMPUTERNAME")) {
            return env.get("COMPUTERNAME");
        }
        else if(env.containsKey("HOSTNAME")) {
            return env.get("HOSTNAME");
        }

        try {
            // TODO: handle multiple available network interfaces...
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) { }

        return UUID.randomUUID().toString();
    }

    private static String getClusterHost(String interfacePrefix) {
        if (interfacePrefix != null && !interfacePrefix.isEmpty()) {
            try {
                Enumeration<NetworkInterface> networkInterfaceEnumeration = NetworkInterface.getNetworkInterfaces();
                for (NetworkInterface networkInterface : Collections.list(networkInterfaceEnumeration)) {
                    Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                    for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                        String ipAddress = inetAddress.toString().substring(1);
                        if (ipAddress.startsWith(interfacePrefix)) {
                            return ipAddress;
                        }
                    }
                }
            } catch (SocketException e) {
                logger.fatal("Failed: recognition of the network interface.", e);
            }
            logger.error("Failed: recognition of the cluster's network interface. Check your system.json's 'clusterInterfacePrefix'. If you have multiple interfaces and you don't specify the correct IP prefix, then your cluster will probably not work.");
        }
        return "localhost";
    }


    public static String getConfigDirectory() {
        return configDirectory;
    }

    public static void setConfigDirectory(String configDirectory) {
        InstanceRunner.configDirectory = configDirectory;
    }

    public static Vertx vertx() {
        return vertx(null);
    }

    public static Vertx vertx(boolean forceNewInstance) {
        return vertx(null, false);
    }

    public static Vertx vertx(JsonArray serviceWhitelist) {
        return vertx(serviceWhitelist, false);
    }

    public static Vertx vertx(JsonArray serviceWhitelist, boolean forceNewInstance) {
        return vertx(serviceWhitelist, forceNewInstance, false);
    }

    public static Vertx vertx(JsonArray serviceWhitelist, boolean forceNewInstance, boolean dontClosePreviousInstance) {
        if(vertx != null) {
            if(!forceNewInstance) {
                return vertx;
            }
            else if(!dontClosePreviousInstance) {
                vertx.close(result -> {
                    if (result.failed()) {
                        logger.fatal("Failed to close preexisting vertx instance!", result.cause());
                    }
                    // TODO never returns, unknown why...
                });
                vertx = null;
                init(serviceWhitelist);
            }
            else {
                vertx = null;
                init(serviceWhitelist);
            }
        }
        else {
            init(serviceWhitelist);
        }

        //TODO: extremely bad... but due to the interface requirements I can't/won't work with futures...
        // better ideas?
        while(vertx == null) {
            try {
                System.err.println("Vertx waiting...");
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return vertx;
    }
}
