package de.maltebehrendt.uppt.cache;

import de.maltebehrendt.uppt.events.Message;
import de.maltebehrendt.uppt.services.AbstractService;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.util.LinkedList;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

// TODO: tests
public class AsyncMessageReplyCache {
    private final ConcurrentHashMap<String, Holder> cache = new ConcurrentHashMap<>();
    private Vertx vertx = null;
    public AbstractService abstractService = null;


    public AsyncMessageReplyCache(Vertx vertx, AbstractService abstractService) {
        this.vertx = vertx;
        this.abstractService = abstractService;
    }

    public void get(String domain, String version, String type, String origin, String correlationID, JsonObject payload, Long ttlInMS, Future<Message> future) {
        get(domain + "." + version + "." + type + ":" + origin + ":" + payload.encode(), domain, version, type, origin, correlationID, payload, ttlInMS, future);
    }

    public void get(String key, String domain, String version, String type, String origin, String correlationID, JsonObject payload, Long ttlInMS, Future<Message> future) {
        if(ttlInMS < 0) {
            throw new IllegalArgumentException("ttlInMS must be positive: " + ttlInMS);
        }

        if(cache.containsKey(key)) {
            cache.get(key).getResult(future);
        }
        else {
            final Holder holder = new Holder();
            holder.getResult(future);
            cache.put(key, holder);

            abstractService.send(domain, version, type, origin, correlationID, payload, reply -> {
                if(reply.failed()) {
                    cache.remove(key);
                    holder.fail(500, reply.cause().getMessage());
                    return;
                }
                if(reply.statusCode() != 200) {
                    cache.remove(key);
                    holder.fail(reply.statusCode(), reply.getMessage());
                    return;
                }
                holder.complete(reply);
                holder.setTimerID(vertx.setTimer(ttlInMS, handler -> {
                    cache.remove(key);
                }));
            });
        }
    }

    public boolean isCached(String domain, String version, String type, String origin, JsonObject payload) {
        return isCached(domain + "." + version + "." + type + ":" + origin + ":" + payload.encode());
    }

    public boolean isCached(String key) {
        return cache.containsKey(key);
    }

    public void remove(String key) {
        Holder holder = cache.remove(key);

        if(holder != null) {
            long timerID = holder.getTimerID();
            if(timerID != 0L) {
                vertx.cancelTimer(timerID);
            }
        }
    }

    public void clear() {
        cache.keySet().forEach(key -> {
            long timerID = cache.get(key).getTimerID();

            if(timerID != 0L) {
                vertx.cancelTimer(timerID);
            }
        });
        cache.clear();
    }

    private static class Holder<Message> {
        private Message reply = null;
        private long timerID = 0L;
        private LinkedList<Future<Message>> futureList = new LinkedList<>();

        Holder() {}

        public void setTimerID(long timerID) {
            this.timerID = timerID;
        }

        public long getTimerID() {
            return this.timerID;
        }

        public void complete(Message reply) {
            Objects.requireNonNull(reply);

            this.reply = reply;

            for(Future<Message> future : futureList) {
                future.complete(reply);
            }
        }

        public void fail(int statusCode, String message) {
            String errorMessage = statusCode + ": " + message;

            for(Future<Message> future : futureList) {
                future.fail(errorMessage);
            }
        }

        public void getResult(Future<Message> future) {
            if(reply != null) {
                future.complete(reply);
            }
            else {
                futureList.add(future);
            }
        }
    }
}
