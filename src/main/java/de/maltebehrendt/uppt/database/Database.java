package de.maltebehrendt.uppt.database;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface Database {
    public boolean exists() throws ExecutionException, InterruptedException;

    public void create(Future<Void> future);
    public boolean createBlocking();
    public void delete(Future<Void> future);
    public boolean deleteBlocking();

    public void createCollection(String collectionName, Future<String> future);
    public void createEdgeCollection(String collectionName, Future<String> future);
    public void deleteCollection(String collectionName, Future<Void> future);

    public void insertDocument(String collectionName, JsonObject document, Future<String> future);
    public void existsDocument(String collectionName, String documentKey, Future<Boolean> future);
    public void updateDocument(String collectionName, String documentKey, JsonObject updatedDocument, Future<String> future);
    public void replaceDocument(String collectionName, String documentKey, JsonObject replacementDocument, Future<String> future);
    public void readDocument(String collectionName, String documentKey, Future<JsonObject> future);
    abstract void deleteDocument(String collectionName, String documentKey, Future<Void> future);

    public void createGraph(String name, JsonArray edgeDefinitions, Future<String> future);
    public void insertVertex(String graphName, String vertxCollectionName, Future<String> future);
    public void insertVertex(String graphName, String vertxCollectionName, JsonObject vertxAttributes, Future<String> future);
    public void deleteVertex(String graphName, String vertxCollectionName, String vertxKey, Future<Void> future);
    public void insertEdge(String graphName, String edgeCollectionName, String fromVertexID, String toVertexID, Future<String> future);
    public void insertEdge(String graphName, String edgeCollectionName, JsonObject edgeAttributes, String fromVertexID, String toVertexID, Future<String> future);
    public void deleteEdge(String graphName, String edgeCollectionName, String edgeKey, Future<Void> future);

    public void query(String query, Map parameterMap, Future<JsonArray> future);

    public void querySequentially(JsonArray queries, Future<JsonArray> future);

    public void querySequentially(JsonArray queries, List<Map> parametersList, Future<JsonArray> future);

    public void importScriptFromFile(String filePath, Future<Object> future);

    public Map<String, Object> jsonObjectToParameterMap(JsonObject jsonObject);

    public Object[] jsonArrayToParameterArray(JsonArray jsonArray);

    public boolean close();
}
