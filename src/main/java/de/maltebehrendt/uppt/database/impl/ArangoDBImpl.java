package de.maltebehrendt.uppt.database.impl;

import com.arangodb.ArangoCursorAsync;
import com.arangodb.ArangoDBAsync;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.CollectionType;
import com.arangodb.entity.EdgeDefinition;
import com.arangodb.entity.LoadBalancingStrategy;
import com.arangodb.model.CollectionCreateOptions;
import com.arangodb.model.GraphCreateOptions;
import de.maltebehrendt.uppt.database.Database;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;


public class ArangoDBImpl implements Database {
    private ArangoDBAsync arangoDB = null;
    private String databaseName = null;

    public ArangoDBImpl(String serviceName, JsonObject databaseConfig) throws Exception {
        if(serviceName == null || serviceName.isEmpty()) {
            throw new Exception("Must provide the serviceName (will be the database name)!");
        }
        else {
            // make sure the database name is valid
            serviceName = serviceName.replaceAll("[^a-zA-Z0-9]", "_");
        }
        this.databaseName = serviceName;

        if(databaseConfig == null) {
            databaseConfig = new JsonObject();
        }

        ArangoDBAsync.Builder builder = new ArangoDBAsync.Builder();

        if(databaseConfig.containsKey("hosts")) {
            JsonArray hosts = databaseConfig.getJsonArray("hosts");

            if(hosts == null || hosts.isEmpty()) {
                throw new Exception("hosts configuration is provided but empty");
            }

            for(int i=0;i<hosts.size();i++) {
                JsonObject host = hosts.getJsonObject(i);
                builder.host(host.getString("address"), host.getInteger("port"));
            }
        }
        else {
            builder.host("127.0.0.1", 8529);
        }
        if(databaseConfig.containsKey("acquireHostList")) {
            builder.acquireHostList(databaseConfig.getBoolean("acquireHostList"));
        }
        if(databaseConfig.containsKey("username")) {
            builder.user(databaseConfig.getString("username"));
        }
        else {
            builder.user("root");
        }
        if(databaseConfig.containsKey("password")) {
            builder.password(databaseConfig.getString("password"));
        }
        else {
            builder.password("");
        }
        if(databaseConfig.containsKey("chunksizeInBytes")) {
            builder.chunksize(databaseConfig.getInteger("chunksizeInBytes"));
        }
        else {
            builder.chunksize(30000);
        }
        if(databaseConfig.containsKey("poolSize")) {
            builder.maxConnections(databaseConfig.getInteger("poolSize"));
        }
        else {
            builder.maxConnections(1);
        }
        if(databaseConfig.containsKey("loadBalancingStrategy")) {
            String loadBalancingStrategy = databaseConfig.getString("loadBalancingStrategy");

            if("roundRobin".equalsIgnoreCase(loadBalancingStrategy)) {
                builder.loadBalancingStrategy(LoadBalancingStrategy.ROUND_ROBIN);
            }
            else if("random".equalsIgnoreCase(loadBalancingStrategy)) {
                builder.loadBalancingStrategy(LoadBalancingStrategy.ONE_RANDOM);
            }
            else {
                throw new Exception("loadBalancingStrategy '" + loadBalancingStrategy + "' is not known/supported. Valid values are: roundRobin and random!");
            }

        }
        arangoDB = builder.build();
    }

    @Override
    public boolean exists() throws ExecutionException, InterruptedException {
        return arangoDB.db(databaseName).exists().get();
    }

    @Override
    public void create(Future<Void> future) {
        arangoDB.createDatabase(databaseName).handle((result, exception) -> {
            if(result != null && result == true) {
                future.complete();
            }
            else {
                future.fail("Failed to create database '" + databaseName + "': " + exception);
            }
            return true;
        });
    }

    @Override
    public boolean createBlocking() {
        try {
            return arangoDB.createDatabase(databaseName).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void delete(Future<Void> future) {
        arangoDB.db(databaseName).drop().handle((result, exception) -> {
            if(result != null && result == true) {
                future.complete();
            }
            else {
                future.fail("Failed to delete database '" + databaseName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public boolean deleteBlocking() {
        try {
            return arangoDB.db(databaseName).drop().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void createCollection(String collectionName, Future<String> future) {
        arangoDB.db(databaseName).collection(collectionName).exists().handle((isExisting, exception) -> {
            if(exception != null) {
                future.fail(exception);
                return true;
            }

            if(isExisting) {
                future.complete(collectionName);
            }
            else {
                arangoDB.db(databaseName).createCollection(collectionName).handle((result, createException) -> {
                    if(createException != null) {
                        if(createException instanceof ArangoDBException && ((ArangoDBException) createException).getErrorNum() == 1207) {
                            // was created simultaneously - check paralellism implementation!
                            future.complete(collectionName);
                        }
                        else {
                            future.fail("Failed to create collection '" + collectionName + "': " +  createException);
                        }
                    }
                    else {
                        future.complete(collectionName);
                    }
                    return true;
                });
            }
            return true;
        });
    }

    @Override
    public void createEdgeCollection(String collectionName, Future<String> future) {
        arangoDB.db(databaseName).collection(collectionName).exists().handle((isExisting, exception) -> {
            if (exception != null) {
                future.fail(exception);
                return true;
            }

            if(isExisting) {
                future.complete(collectionName);
            }
            else {
                arangoDB.db(databaseName).createCollection(collectionName, new CollectionCreateOptions().type(CollectionType.EDGES)).handle((result, createException) -> {
                    if(createException != null) {
                        if(createException instanceof ArangoDBException && ((ArangoDBException) createException).getErrorNum() == 1207) {
                            // was created simultaneously - check paralellism implementation!
                            future.complete(collectionName);
                        }
                        else {
                            future.fail("Failed to create edge collection '" + collectionName + "': " +  createException);
                        }
                    }
                    else {
                        future.complete(result.getName());
                    }
                    return true;
                });
            }
            return true;
        });
    }

    @Override
    public void deleteCollection(String collectionName, Future<Void> future) {
        arangoDB.db(databaseName).collection(collectionName).drop().handle((result, exception) -> {
            if(exception == null) {
                future.complete();
            }
            else {
                future.fail("Failed to delete collection '" + collectionName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void insertDocument(String collectionName, JsonObject document, Future<String> future) {
        arangoDB.db(databaseName).collection(collectionName).insertDocument(document.encode()).handle((result, exception) -> {
            if(exception == null) {
                future.complete(result.getKey());
            }
            else {
                future.fail("Failed to insert document into collection '" + collectionName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void existsDocument(String collectionName, String documentKey, Future<Boolean> future) {
        arangoDB.db(databaseName).collection(collectionName).documentExists(documentKey).handle((result, exception) -> {
           if(exception == null) {
                future.complete(result);
           }
           else {
               future.fail("Failed to check document with key " + documentKey + " existence in collection '" + collectionName + "': " +  exception);
           }
           return true;
        });
    }

    @Override
    public void updateDocument(String collectionName, String documentKey, JsonObject updatedDocument, Future<String> future) {
        arangoDB.db(databaseName).collection(collectionName).updateDocument(documentKey, updatedDocument.encode()).handle((result, exception) -> {
            if(exception == null) {
                future.complete(result.getKey());
            }
            else {
                future.fail("Failed to update document with key '" + documentKey + "' in collection '" + collectionName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void replaceDocument(String collectionName, String documentKey, JsonObject replacementDocument, Future<String> future) {
        arangoDB.db(databaseName).collection(collectionName).replaceDocument(documentKey, replacementDocument.encode()).handle((result, exception) -> {
            if(exception == null) {
                future.complete(result.getKey());
            }
            else {
                future.fail("Failed to replace document with key '" + documentKey + "' in collection '" + collectionName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void readDocument(String collectionName, String documentKey, Future<JsonObject> future) {
        arangoDB.db(databaseName).collection(collectionName).getDocument(documentKey, String.class).handle((result, exception) -> {
            if(exception == null && result != null) {
                future.complete(new JsonObject(result));
            }
            else {
                future.fail("Failed to read document with key '" + documentKey + "' from collection '" + collectionName + "': " + exception);
            }
            return true;
        });
    }

    @Override
    public void deleteDocument(String collectionName, String documentKey, Future<Void> future) {
        arangoDB.db(databaseName).collection(collectionName).deleteDocument(documentKey).handle((result, exception) -> {
            if(exception == null) {
                future.complete();
            }
            else {
                future.fail("Failed to delete document with key '" + documentKey + "' in collection '" + collectionName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void createGraph(String name, JsonArray edgeDefinitions, Future<String> future) {
        if(name == null || name.isEmpty()) {
            future.fail("Must provide an non-empty name for the graph!");
            return;
        }
        if(edgeDefinitions == null || edgeDefinitions.isEmpty()) {
            future.fail("Must provide non-empty edge definitions!");
            return;
        }

        Collection<EdgeDefinition> edges = new ArrayList<>();
        List<Future> edgeFutures = new LinkedList<>();
        for(int i=0;i<edgeDefinitions.size();i++) {
            Future edgeFuture = Future.future();
            edgeFutures.add(edgeFuture);
            JsonObject edgeDefinition = edgeDefinitions.getJsonObject(i);

            // create edge collection
            String edgeName = edgeDefinition.getString("name");
            Future edgeCollectionFuture = Future.future().setHandler(edgeCollectionResult -> {
                if(edgeCollectionResult.failed()) {
                    edgeFuture.fail("Failed to created edge collection " + edgeName + ": " + edgeCollectionResult.cause());
                    return;
                }

                JsonArray toCollections = edgeDefinition.getJsonArray("toCollections");
                JsonArray fromCollections = edgeDefinition.getJsonArray("fromCollections");

                if(toCollections == null || toCollections.isEmpty()) {
                    edgeFuture.fail("Must provide fromCollections for edge definition of " + edgeName);
                    return;
                }
                if(fromCollections == null || fromCollections.isEmpty()) {
                    edgeFuture.fail("Must provide toCollections for edge definition of " + edgeName);
                    return;
                }

                // make sure all required collections exist
                JsonArray requiredCollections = new JsonArray();
                requiredCollections.addAll(toCollections);
                requiredCollections.addAll(fromCollections);

                List<Future> collectionFutures = new LinkedList<>();
                for(int a=0;a<requiredCollections.size();a++) {
                    Future collectionFuture = Future.future();
                    collectionFutures.add(collectionFuture);

                    String collectionName = requiredCollections.getString(a);
                    createCollection(collectionName, collectionFuture);
                }

                CompositeFuture.all(collectionFutures).setHandler(collectionResult -> {
                    if(collectionResult.failed()) {
                        edgeFuture.fail("Failed to create required collections: " + collectionResult.cause());
                        return;
                    }
                    EdgeDefinition edge = new EdgeDefinition();
                    edge.collection(edgeName);

                    String toCollectionNames[] = new String[toCollections.size()];
                    String fromCollectionNames[] = new String[fromCollections.size()];
                    for(int b=0;b<toCollections.size();b++) {
                        toCollectionNames[b] = toCollections.getString(b);
                    }
                    for(int b=0;b<fromCollections.size();b++) {
                        fromCollectionNames[b] = fromCollections.getString(b);
                    }
                    edge.to(toCollectionNames);
                    edge.from(fromCollectionNames);
                    edges.add(edge);
                    edgeFuture.complete();
                });
            });
            createEdgeCollection(edgeName, edgeCollectionFuture);
        }

        CompositeFuture.all(edgeFutures).setHandler(edgeResults -> {
            if(edgeResults.failed()) {
                future.fail("Failed to define edge collection: " + edgeResults.cause());
                return;
            }
            GraphCreateOptions options = new GraphCreateOptions();
            arangoDB.db(databaseName).createGraph(name, edges, options).whenComplete((graphResult, exception) -> {
                if(exception != null || graphResult == null) {
                    future.fail("Failed to create graph: " + exception);
                    return;
                }
                future.complete(name);
            });
        });
    }

    @Override
    public void insertVertex(String graphName, String vertxCollectionName, Future<String> future) {
        arangoDB.db(databaseName).graph(graphName).vertexCollection(vertxCollectionName).insertVertex("{}").handle((result, exception) -> {
            if(exception == null) {
                future.complete(result.getKey());
            }
            else {
                future.fail("Failed to insert vertx of " + vertxCollectionName + " into graph '" + graphName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void insertVertex(String graphName, String vertxCollectionName, JsonObject vertxAttributes, Future<String> future) {
        arangoDB.db(databaseName).graph(graphName).vertexCollection(vertxCollectionName).insertVertex(vertxAttributes.encode()).handle((result, exception) -> {
            if(exception == null) {
                future.complete(result.getKey());
            }
            else {
                future.fail("Failed to insert vertx of " + vertxCollectionName + " into graph '" + graphName + "': " +  exception);
            }
            return true;
        });
    }

    @Override
    public void deleteVertex(String graphName, String vertxCollectionName, String vertxKey, Future<Void> future) {
        arangoDB.db(databaseName).graph(graphName).vertexCollection(vertxCollectionName).deleteVertex(vertxKey).handle((deleteResult, exception) -> {
            if(exception == null) {
                future.complete();
            }
            else {
                future.fail("Failed to delete vertex " + vertxKey + " of " + vertxCollectionName + " in graph " + graphName +": " + exception);
            }
            return true;
        });
    }

    @Override
    public void insertEdge(String graphName, String edgeCollectionName, String fromVertexID, String toVertexID, Future<String> future) {
        arangoDB.db(databaseName).graph(graphName).edgeCollection(edgeCollectionName).insertEdge("{ \"_from\": \"" + fromVertexID + "\", \"_to\": \"" + toVertexID + "\"}").handle((insertResult, exception) -> {
            if(exception == null) {
                future.complete(insertResult.getKey());
            }
            else {
                future.fail("Failed to insert edge of type " + edgeCollectionName + " from " + fromVertexID + " to " + toVertexID + " in graph " + graphName + ": " + exception);
            }
            return true;
        });
    }

    @Override
    public void insertEdge(String graphName, String edgeCollectionName, JsonObject edgeAttributes, String fromVertexID, String toVertexID, Future<String> future) {
        edgeAttributes.put("_from", fromVertexID);
        edgeAttributes.put("_to", toVertexID);
        arangoDB.db(databaseName).graph(graphName).edgeCollection(edgeCollectionName).insertEdge(edgeAttributes.encode()).handle((insertResult, exception) -> {
            if(exception == null) {
                future.complete(insertResult.getKey());
            }
            else {
                future.fail("Failed to insert edge of type " + edgeCollectionName + " from " + fromVertexID + " to " + toVertexID + " in graph " + graphName + ": " + exception);
            }
            return true;
        });
    }

    @Override
    public void deleteEdge(String graphName, String edgeCollectionName, String edgeKey, Future<Void> future) {
        arangoDB.db(databaseName).graph(graphName).edgeCollection(edgeCollectionName).deleteEdge(edgeKey).handle((deleteResult, exception) -> {
            if(exception == null) {
                future.complete();
            }
            else {
                future.fail("Failed to delete edge of type " + edgeCollectionName + " with key " + edgeKey + " in graph " + graphName + ": " + exception);
            }
            return true;
        });
    }

    @Override
    public void query(String query, Map parameterMap, Future<JsonArray> future) {
        arangoDB.db(databaseName).query(query, parameterMap, null, String.class).handle((cursor, exception) -> {
            JsonArray results = new JsonArray();
            if(exception != null) {
                future.fail("Failed to query database: " +  exception);
            }
            else if(cursor != null) {
                ((ArangoCursorAsync<String>) cursor).forEachRemaining(result -> {
                    if(result != null && !"null".equals(result)) {
                        results.add(new JsonObject(result));
                    }
                });
                future.complete(results);
            }
            else {
                future.complete(results);
            }
            return true;
        });
    }

    @Override
    public void querySequentially(JsonArray queries, Future<JsonArray> future) {
        querySequentially(queries, null, future);
    }

    @Override
    public void querySequentially(JsonArray queries, List<Map> parametersList, Future<JsonArray> future) {
        JsonArray results = new JsonArray();
        if(queries == null || queries.isEmpty()) {
            future.complete(results);
            return;
        }
        if(parametersList == null) {
            parametersList = new LinkedList<>();
        }
        List<Map> paramsList = parametersList;
        if(paramsList.isEmpty()) {
            queries.forEach(query -> paramsList.add(new HashMap()));
        }

        int queryCount = queries.size();
        Future<JsonArray> finalFuture = Future.future();
        finalFuture.setHandler(queryResults -> {
            if(queryResults.failed()) {
                future.fail(queryResults.cause());
                return;
            }
            results.add(queryResults.result());
            future.complete(results);
        });

        if(queryCount > 2) {
            Future<JsonArray> firstFuture = Future.future();
            Future<JsonArray> tmpFuture = null;
            for(int i=1;i<queryCount-1;i++) {
                final String query = queries.getString(i);
                final Map params = paramsList.get(i);
                tmpFuture = firstFuture.compose(queryResults -> {
                    results.add(queryResults);
                    Future<JsonArray> nextFuture = Future.future();
                    query(query, params, nextFuture);
                    return nextFuture;
                });
            }
            tmpFuture.compose(queryResults -> {
                results.add(queryResults);
                query(queries.getString(queryCount-1), paramsList.get(queryCount-1), finalFuture);
            }, finalFuture);
            query(queries.getString(0), paramsList.get(0), firstFuture);
        }
        else if(queryCount == 2) {
            Future<JsonArray> firstFuture = Future.future();
            firstFuture.compose(v-> {
                query(queries.getString(1), paramsList.get(1), finalFuture);
            }, finalFuture);
            query(queries.getString(0), paramsList.get(0), firstFuture);
        }
        else {
            query(queries.getString(0), paramsList.get(0), finalFuture);
        }
    }



    @Override
    public void importScriptFromFile(String filePath, Future<Object> future) {
        if(filePath == null || !filePath.endsWith(".aqlTemplate")) {
            future.fail("Provided file path is either empty or does not seem to be an AQL file (ends with '.aqlTemplate')");
            return;
        }

        try {
            JsonObject template = new JsonObject(new String(Files.readAllBytes(Paths.get(filePath))));

            // create all required collections first
            List<Future> collectionFutures = new LinkedList<>();
            if(template.containsKey("documents")) {
                JsonArray collections = template.getJsonArray("documents");

                for(int i=0;i<collections.size();i++) {
                    Future collectionFuture = Future.future();
                    collectionFutures.add(collectionFuture);
                    createCollection(collections.getString(i), collectionFuture);
                }
            }

            CompositeFuture.all(collectionFutures).setHandler(collectionsResult -> {
               if(collectionsResult.failed()) {
                   future.fail("Failed to create document collections: " + collectionsResult.cause());
                   return;
               }

                // then create the named graphs
                List<Future> graphFutures = new LinkedList<>();
                if(template.containsKey("graphs")) {
                    JsonObject graphDefinitions = template.getJsonObject("graphs");

                    for(String graphName : graphDefinitions.fieldNames()) {
                        Future graphFuture = Future.future();
                        graphFutures.add(graphFuture);

                        JsonArray edgeDefinitions = new JsonArray();

                        JsonObject graphEdges = graphDefinitions.getJsonObject(graphName);
                        for(String edgeName : graphEdges.fieldNames()) {
                            edgeDefinitions.add(graphEdges.getJsonObject(edgeName).put("name", edgeName));
                        }

                        createGraph(graphName, edgeDefinitions, graphFuture);
                    }
                }

                CompositeFuture.all(graphFutures).setHandler(graphsResult -> {
                    if(graphsResult.failed()) {
                        future.fail("Failed to create graphs: " + graphsResult.cause());
                        return;
                    }

                    // finally, execute the remaining queries sequentially
                    if(template.containsKey("queries")) {
                        JsonArray queries = template.getJsonArray("queries");
                        Future<JsonArray> queriesFuture = Future.future();
                        queriesFuture.setHandler(queriesResult -> {
                            if(queriesResult.failed()) {
                                future.fail("Failed to execute queries: " + queriesResult.cause());
                                return;
                            }
                            future.complete();
                        });
                        querySequentially(queries, queriesFuture);
                    }
                    else {
                        future.complete();
                    }
                });
            });
        }
        catch (IOException e) {
            e.printStackTrace();
            future.fail("Failed to open the template file at '" + filePath + "': " + e.getMessage());
        }
    }

    @Override
    public Map<String, Object> jsonObjectToParameterMap(JsonObject jsonObject) {
        Map<String, Object> map = new HashMap<>();

        for(String key : jsonObject.fieldNames()) {
            Object value = jsonObject.getValue(key);

            if(value instanceof JsonObject) {
                map.put(key, jsonObjectToParameterMap((JsonObject) value));
            }
            else if(value instanceof JsonArray) {
                map.put(key, jsonArrayToParameterArray((JsonArray) value));
            }
            else {
                map.put(key, value);
            }
        }

        return map;
    }

    @Override
    public Object[] jsonArrayToParameterArray(JsonArray jsonArray) {
        int arraySize = jsonArray.size();
        Object[] array = new Object[arraySize];

        for(int i=0;i<arraySize;i++) {
            Object value = jsonArray.getValue(i);

            if(value instanceof JsonObject) {
                array[i] = jsonObjectToParameterMap((JsonObject) value);
            }
            else if(value instanceof JsonArray) {
                array[i] = jsonArrayToParameterArray((JsonArray) value);
            }
            else {
                array[i] = value;
            }
        }

        return array;
    }

    @Override
    public boolean close() {
        arangoDB.shutdown();
        return true;
    }
}
