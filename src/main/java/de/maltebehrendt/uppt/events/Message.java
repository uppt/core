package de.maltebehrendt.uppt.events;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Created by malte on 02.02.17.
 */
public interface Message<R> extends AsyncResult<Message<R>> {

    public void reply(Integer statusCode);
    public void reply(JsonObject payload);
    public void reply(Integer statusCode, JsonObject payload);
    public void reply(Integer statusCode, String errorMessage);
    public void reply(Integer statusCode, Handler<Message<R>> replyHandler);
    public void reply(JsonObject payload, Handler<Message<R>> replyHandler);
    public void reply(Integer statusCode, JsonObject payload, Handler<Message<R>> replyHandler);
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options);
    public void reply(JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler);
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler);
    public void fail(String errorMessage);
    public void fail(Integer errorCode, String errorMessage);
    public void fail(Integer errorCode, String errorMessage, Throwable cause);

    public Object body();
    public String getMessage();
    public JsonObject getBodyAsJsonObject();
    public Integer statusCode();
    public String correlationID();
    public String origin();
    public String userID();
    public JsonArray userRoles();
    public String sessionAddress();

    @Override
    public Message<R> result();

    @Override
    public Throwable cause();

    @Override
    public boolean succeeded();

    @Override
    public boolean failed();
}
