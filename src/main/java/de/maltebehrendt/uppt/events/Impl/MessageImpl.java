package de.maltebehrendt.uppt.events.Impl;

import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;

import java.util.UUID;

/**
 * Created by malte on 30.10.16.
 */
public class MessageImpl<R> implements Message<R> {
    private io.vertx.core.eventbus.Message<R> vertxMessage;
    private Throwable cause = null;
    private Boolean succeeded = false;
    private String correlationID = null;
    private String origin = null;

    public MessageImpl(io.vertx.core.eventbus.Message<R> vertxMessage) {
        this.vertxMessage = vertxMessage;
        this.correlationID = vertxMessage.headers().get("correlationID");
        this.origin = vertxMessage.headers().get("origin") != null? vertxMessage.headers().get("origin") : "unknown";
        this.succeeded = true;
    }

    public MessageImpl(String correlationID, Throwable cause) {
        this.cause = cause;
        this.correlationID = correlationID;
        this.origin = "unknown";
    }

    @Override
    public void reply(Integer statusCode) {
        this.reply(statusCode, null, null, null);
    }

    @Override
    public void reply(JsonObject payload) {
        this.reply(null, payload, null, null);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload) {
        this.reply(statusCode, payload, null, null);
    }

    @Override
    public void reply(Integer statusCode, String errorMessage) {
        this.reply(statusCode, new JsonObject().put("message", errorMessage), null, null);
    }

    @Override
    public void reply(Integer statusCode, Handler<Message<R>> replyHandler) {
        this.reply(statusCode, null, null, replyHandler);
    }

    @Override
    public void reply(JsonObject payload, Handler<Message<R>> replyHandler) {
        this.reply(null, payload, null, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, Handler<Message<R>> replyHandler) {
        this.reply(statusCode, payload, null, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options) {
        this.reply(statusCode, payload, options, null);
    }

    @Override
    public void reply(JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        this.reply(null, payload, options, replyHandler);
    }

    @Override
    public void reply(Integer statusCode, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        if(statusCode == null) {
            statusCode = 200;
        }
        if(payload == null) {
            payload = new JsonObject();
        }
        if(options == null) {
            options = new DeliveryOptions();
        }

        options.addHeader("statusCode", statusCode.toString())
                .addHeader("correlationID", correlationID())
                .addHeader("origin", origin());

        if(replyHandler != null) {
            vertxMessage.reply(payload, options, handler -> {
                if(handler.failed()) {
                    LoggerFactory.getLogger(this.getClass()).warn("Failed: replying to message with correlation ID " + this.correlationID(), handler.cause());
                    replyHandler.handle(new MessageImpl(correlationID(), handler.cause()));
                }
                else {
                    replyHandler.handle(new MessageImpl(handler.result()));
                }
            });
        }
        else {
            vertxMessage.reply(payload, options);
        }
    }

    @Override
    public void fail(String errorMessage) {
        fail(500, errorMessage, null);
    }

    @Override
    public void fail(Integer errorCode, String errorMessage) {
        fail(errorCode, errorMessage, null);
    }

    @Override
    public void fail(Integer errorCode, String errorMessage, Throwable cause) {
        String errorCause = cause == null? "unknown" : cause.getMessage();

        reply(errorCode, new JsonObject().put("message", errorMessage).put("errorCode", errorCode).put("errorCause", errorCause), null, null);
    }

    @Override
    public Object body() {
        return vertxMessage.body();
    }

    @Override
    public JsonObject getBodyAsJsonObject() {
        if(vertxMessage.body() instanceof JsonObject) {
            return (JsonObject) vertxMessage.body();
        }
        return new JsonObject();
    }

    @Override
    public String getMessage() {
        if(vertxMessage.body() instanceof JsonObject) {
            return ((JsonObject) vertxMessage.body()).getString("message");
        }
        return "";
    }

    @Override
    public Integer statusCode() {
        String statusCode = vertxMessage.headers().get("statusCode");
        if(statusCode != null) {
            return Integer.parseInt(statusCode);
        }
        return -1;
    }

    @Override
    public String correlationID() {
        return correlationID != null? correlationID : UUID.randomUUID().toString();
    }

    @Override
    public String origin() {
        return origin != null? origin:"unknown";
    }

    @Override
    public String userID() {
        return vertxMessage.headers().get("userID");
    }

    @Override
    public JsonArray userRoles() {
        return vertxMessage.headers().get("userRoles") != null? new JsonArray(vertxMessage.headers().get("userRoles")):new JsonArray();
    }

    @Override
    public String sessionAddress() {
        return vertxMessage.headers().get("sessionAddress");
    }

    @Override
    public Message<R> result() {
        return this;
    }

    @Override
    public Throwable cause() {
        return cause;
    }

    @Override
    public boolean succeeded() {
        return succeeded;
    }

    @Override
    public boolean failed() {
        return !succeeded;
    }
}
