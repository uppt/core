package de.maltebehrendt.uppt.storage;

import de.maltebehrendt.uppt.storage.Impl.StorageBackend;
import de.maltebehrendt.uppt.storage.Impl.StorageBackendFileSystemImpl;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.core.streams.WriteStream;

import java.util.zip.ZipInputStream;

public class Storage {
    private Vertx vertx = null;
    private StorageBackend ownStorage = null;
    private StorageBackend sharedStorage = null;
    private StorageBackend publicStorage = null;
    private StorageBackend usersStorage = null;

    public Storage(JsonObject ownStorageConfig, JsonObject sharedStorageConfig, JsonObject usersStorageConfig, JsonObject publicStorageConfig, Vertx vertx) throws Exception {
        this.vertx = vertx;
//TODO: disable serving from classpath (at least for public and user storage)!
        ownStorage = getBackend(ownStorageConfig, vertx);
        sharedStorage = getBackend(sharedStorageConfig, vertx);
        usersStorage = getBackend(usersStorageConfig, vertx);
        publicStorage = getBackend(publicStorageConfig, vertx);
    }

    public static StorageBackend getBackend(JsonObject storageConfig, Vertx vertx) throws Exception {
        if(storageConfig == null || storageConfig.isEmpty()) {
            throw new Exception("Must provide a non-empty storage config!");
        }
        if(!storageConfig.containsKey("path")) {
            throw new Exception("Must define the path/url to the storage via 'path'!");
        }

        String path = storageConfig.getString("path");

        // later: check the path for identifying the backend to use
        // for now only FS is supported anyways...

        return new StorageBackendFileSystemImpl(path, vertx);
    }
    
    public <T> void persist(String path, ReadStream<T> readStream, Future<Void> future) {
        ownStorage.persist(path, readStream, future);
    }

    
    public <T> void persist(String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future) {
        ownStorage.persist(path, overwrite, readStream, future);
    }

    
    public void unzipAndPersist(String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future) {
        ownStorage.unzipAndPersist(folderPath, overwrite, zipInputStream, maxTotalExtractedSizeInMB, maxNumberOfFiles, future);
    }

    
    public <T> void getWriteStream(String path, Boolean overwrite, Future<WriteStream<T>> future) {
        ownStorage.getWriteStream(path, overwrite, future);
    }

    
    public <T> void retrieve(String path, WriteStream<T> writeStream, Future<Void> future) {
        ownStorage.retrieve(path, writeStream, future);
    }

    
    public void retrieveMetaData(String path, Future<JsonObject> future) {
        ownStorage.retrieveMetaData(path, future);
    }

    
    public <T> void getReadStream(String path, Future<ReadStream<T>> future) {
        ownStorage.getReadStream(path, future);
    }

    public void delete(String path, Boolean recursive, Future<Void> future) {
        ownStorage.delete(path, recursive, future);
    }


    public <T> void persistToPublic(String path, ReadStream<T> readStream, Future<Void> future) {
        publicStorage.persist(path, readStream, future);
    }

    
    public <T> void persistToPublic(String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future) {
        publicStorage.persist(path, overwrite, readStream, future);
    }

    
    public void unzipAndPersistToPublic(String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future) {
        publicStorage.unzipAndPersist(folderPath, overwrite, zipInputStream, maxTotalExtractedSizeInMB, maxNumberOfFiles, future);
    }

    
    public <T> void getWriteStreamToPublic(String path, Boolean overwrite, Future<WriteStream<T>> future) {
        publicStorage.getWriteStream(path, overwrite, future);
    }

    
    public <T> void retrieveFromPublic(String path, WriteStream<T> writeStream, Future<Void> future) {
        publicStorage.retrieve(path, writeStream, future);
    }

    
    public void retrieveMetaDataFromPublic(String path, Future<JsonObject> future) {
        publicStorage.retrieveMetaData(path, future);
    }

    
    public <T> void getReadStreamFromPublic(String path, Future<ReadStream<T>> future) {
        publicStorage.getReadStream(path, future);
    }

    public void deleteFromPublic(String path, Boolean recursive, Future<Void> future) {
        publicStorage.delete(path, recursive, future);
    }

    
    public <T> void persistToService(String serviceName, String path, ReadStream<T> readStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void persistToService(String serviceName, String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public void unzipAndPersistToService(String serviceName, String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void getWriteStreamToService(String serviceName, String path, Boolean overwrite, Future<WriteStream<T>> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void retrieveFromService(String serviceName, String path, WriteStream<T> writeStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public void retrieveMetaDataFromService(String serviceName, String Path, Future<JsonObject> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void getReadStreamFromService(String serviceName, String path, Future<ReadStream<T>> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void persistToUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String path, ReadStream<T> readStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void persistToUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public void unzipAndPersistToUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void getWriteStreamToUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String path, Boolean overwrite, Future<WriteStream<T>> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void retrieveFromUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String path, WriteStream<T> writeStream, Future<Void> future) {
        future.fail("Not implemented yet!");
    }

    
    public void retrieveMetaDataFromUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String Path, Future<JsonObject> future) {
        future.fail("Not implemented yet!");
    }

    
    public <T> void getReadStreamFromUsers(JsonArray userIdWhitelist, JsonArray userRolesWhitelist, String path, Future<ReadStream<T>> future) {
        future.fail("Not implemented yet!");
    }
}
