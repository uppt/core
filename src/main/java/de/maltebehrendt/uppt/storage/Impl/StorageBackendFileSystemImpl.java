package de.maltebehrendt.uppt.storage.Impl;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.FileProps;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.Pump;
import io.vertx.core.streams.ReadStream;
import io.vertx.core.streams.WriteStream;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class StorageBackendFileSystemImpl implements StorageBackend {
    private Vertx vertx = null;
    private FileSystem fileSystem = null;
    private Path rootPath = null;
    private String rootParentPath = null;

    public StorageBackendFileSystemImpl(String rootFolderPath, Vertx vertx) {
        this.vertx = vertx;
        this.fileSystem = vertx.fileSystem();
        this.rootParentPath = rootFolderPath;

        this.rootPath = Paths.get(rootFolderPath).toAbsolutePath();

        if(!fileSystem.existsBlocking(rootPath.toString())) {
            fileSystem.mkdirsBlocking(rootPath.toString());
        }
    }

    @Override
    public <T> void persist(String path, ReadStream<T> readStream, Future<Void> future) {
        persist(path, null, readStream, future);
    }

    @Override
    public <T> void persist(String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future) {
        if(overwrite == null) {
            overwrite = false;
        }

        Future<WriteStream<T>> writeStreamFuture = Future.future();
        writeStreamFuture.setHandler(handler -> {
            if(handler.failed()) {
                future.fail(handler.cause());
                return;
            }

            Pump.pump(readStream, handler.result()).start();
            readStream.endHandler((result) -> {
                future.complete();
            });
        });
        getWriteStream(path, overwrite, writeStreamFuture);
    }

    @Override
    public void unzipAndPersist(String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future) {
        if(zipInputStream == null) {
            future.fail("Must provide zipInputStream!");
            return;
        }
        if(overwrite == null) {
            overwrite = false;
        }
        if(maxTotalExtractedSizeInMB == null) {
            maxTotalExtractedSizeInMB = 500L;
        }
        if(maxNumberOfFiles == null) {
            maxNumberOfFiles = 10000L;
        }

        Path targetPath = Paths.get(rootPath.toString() + File.separator + folderPath).toAbsolutePath();

        if(!targetPath.startsWith(rootPath)) {
            future.fail("Target path escapes storage directory!");
            return;
        }

        unzip(targetPath, overwrite,  rootPath, zipInputStream, maxTotalExtractedSizeInMB, maxNumberOfFiles, 4096, future);
    }


    public void unzip(Path targetPath, Boolean overwrite, Path basePath, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Integer bufferSizeInBytes, Future<JsonObject> future) {
        vertx.executeBlocking(handler -> {
            try {
                ZipEntry entry = zipInputStream.getNextEntry();
                long maxTotalExtractedSize = maxTotalExtractedSizeInMB * 1000000L;
                long totalExtractedSize = 0L;
                long numberOfFiles = 0l;
                boolean isTargetPathCreated = false;

                if(fileSystem.existsBlocking(targetPath.toString())) {
                    if(overwrite) {
                        fileSystem.deleteRecursiveBlocking(targetPath.toString(), true);
                        fileSystem.mkdirsBlocking(targetPath.toString());
                        isTargetPathCreated = true;
                    }
                }
                else {
                    fileSystem.mkdirsBlocking(targetPath.toString());
                    isTargetPathCreated = true;
                }

                // iterates over entries in the zip file
                while (entry != null) {
                    Path targetFilePath = Paths.get(targetPath.toString() + File.separator + entry.getName()).toAbsolutePath();
                    if(!targetFilePath.startsWith(targetPath)) {
                        handler.fail("Target file path escapes storage directory: " + entry.getName());
                        return;
                    }

                    if (entry.isDirectory()) {
                        if(!fileSystem.existsBlocking(targetFilePath.toString())) {
                            fileSystem.mkdirsBlocking(targetFilePath.toString());
                        }
                    } else {
                        if(!fileSystem.existsBlocking(targetFilePath.getParent().toString())) {
                            fileSystem.mkdirsBlocking(targetFilePath.getParent().toString());
                        }
                        if(fileSystem.existsBlocking(targetFilePath.toString())) {
                            // skip this file (if overwrite, then the target directory had already been deleted)
                            entry = zipInputStream.getNextEntry();
                            continue;
                        }

                        OpenOptions options = new OpenOptions()
                                .setCreate(true)
                                .setWrite(true);
                        AsyncFile asyncFile = fileSystem.openBlocking(targetFilePath.toString(), options);

                        byte[] bytesIn = new byte[bufferSizeInBytes];
                        Buffer buffer = Buffer.buffer(bufferSizeInBytes);
                        int read = 0;
                        while ((totalExtractedSize + bufferSizeInBytes <= maxTotalExtractedSize) && (read = zipInputStream.read(bytesIn)) != -1) {
                            buffer.setBytes(0, bytesIn, 0, read);
                            asyncFile.write(buffer);
                            totalExtractedSize += read;
                        }
                        asyncFile.flush();
                        asyncFile.close();
                    }
                    zipInputStream.closeEntry();
                    numberOfFiles++;

                    if (numberOfFiles > maxNumberOfFiles) {
                        zipInputStream.close();
                        if(isTargetPathCreated == true || overwrite == true) {
                            fileSystem.deleteRecursiveBlocking(targetPath.toString(), true);
                        }
                        handler.fail("Failed to unzip: numberOfFiles > maxNumberOfFiles (" + maxNumberOfFiles + "). File: " + entry.getName());
                        return;
                    }
                    if (totalExtractedSize + bufferSizeInBytes > maxTotalExtractedSize) {
                        zipInputStream.close();
                        if(isTargetPathCreated == true || overwrite == true) {
                            fileSystem.deleteRecursiveBlocking(targetPath.toString(), true);
                        }
                        handler.fail("Failed to unzip: totalExtractedSize > maxTotalExtractedSize (" + maxTotalExtractedSize + "). File: " + entry.getName());
                        return;
                    }

                    entry = zipInputStream.getNextEntry();
                }
                zipInputStream.close();

                handler.complete(new JsonObject()
                        .put("numberOfFiles", numberOfFiles)
                        .put("totalExtractedSize", totalExtractedSize)
                        .put("targetDirectory", basePath.relativize(targetPath).toString())
                );
            }
            catch (IOException e) {
                handler.fail("IO Exception: " + e.getMessage());
            }
            catch (Exception e) {
                handler.fail("Exception: " + e.getMessage());
            }
        }, result -> {
            if(result.failed()) {
                result.cause().printStackTrace();
                future.fail(result.cause());
                return;
            }
            future.complete((JsonObject) result.result());
        });
    }

    @Override
    public <T> void getWriteStream(String path, Boolean overwrite, Future<WriteStream<T>> future) {
        Path targetPath = Paths.get(rootPath.toString() + File.separator + path).toAbsolutePath();

        if(!targetPath.startsWith(rootPath)) {
            future.fail("Target path escapes storage directory!");
            return;
        }

        if(overwrite == null) {
            overwrite = false;
        }
        if(overwrite) {
            if(fileSystem.existsBlocking(targetPath.toString())) {
                fileSystem.deleteRecursiveBlocking(targetPath.toString(), true);
            }
        }
        if(!fileSystem.existsBlocking(targetPath.getParent().toString())) {
            fileSystem.mkdirsBlocking(targetPath.getParent().toString());
        }

        OpenOptions options = new OpenOptions()
                .setCreate(true)
                .setWrite(true);
        fileSystem.open(targetPath.toString(), options, openHandler -> {
            if(openHandler.failed()) {
                future.fail("Failed to open target path " + targetPath + " for writing: " + openHandler.cause());
                return;
            }

            AsyncFile asyncFile = openHandler.result();
            future.complete((WriteStream<T>) asyncFile);
        });
    }

    @Override
    public <T> void retrieve(String path, WriteStream<T> writeStream, Future<Void> future) {
        Future<ReadStream<T>> readStreamFuture = Future.future();
        readStreamFuture.setHandler(handler -> {
            if(handler.failed()) {
                future.fail(handler.cause());
                return;
            }
            ReadStream<T> readStream = handler.result();

            Pump.pump(readStream, writeStream).start();
            readStream.endHandler((result) -> {
                future.complete();
            });
        });
        getReadStream(path, readStreamFuture);
    }

    @Override
    public void retrieveMetaData(String path, Future<JsonObject> future) {
        Path targetPath = Paths.get(rootPath.toString() + File.separator + path).toAbsolutePath();

        if(!targetPath.startsWith(rootPath)) {
            future.fail("Target path escapes storage directory!");
            return;
        }

        fileSystem.props(targetPath.toString(), handler -> {
            if(handler.failed()) {
                future.fail("Failed to retrieve file's meta-data: " + handler.cause());
                return;
            }
            FileProps fileProps = handler.result();
            JsonObject metaData = new JsonObject()
                    .put("name", targetPath.getFileName().toString())
                    .put("creationTime", fileProps.creationTime())
                    .put("lastAccessTime", fileProps.lastAccessTime())
                    .put("lastModifiedTime", fileProps.lastModifiedTime())
                    .put("size", fileProps.size())
                    .put("isDirectory", fileProps.isDirectory())
                    .put("isOther", fileProps.isOther())
                    .put("isRegularFile", fileProps.isRegularFile())
                    .put("isSymbolicLink", fileProps.isSymbolicLink());
            future.complete(metaData);
        });
    }

    @Override
    public <T> void getReadStream(String path, Future<ReadStream<T>> future) {
        Path targetPath = Paths.get(rootPath.toString() + File.separator + path).toAbsolutePath();

        if(!targetPath.startsWith(rootPath)) {
            future.fail("Target path escapes storage directory!");
            return;
        }

        OpenOptions options = new OpenOptions()
                .setCreate(false)
                .setRead(true)
                .setWrite(false);
        fileSystem.open(targetPath.toString(), options, openHandler -> {
            if(openHandler.failed()) {
                future.fail("Failed to open target path " + targetPath + " for reading: " + openHandler.cause());
                return;
            }

            AsyncFile asyncFile = openHandler.result();
            future.complete((ReadStream<T>) asyncFile);
        });
    }

    @Override
    public void delete(String path, Boolean recursive, Future<Void> future) {
        Path targetPath = Paths.get(rootPath.toString() + File.separator + path).toAbsolutePath();

        if(!targetPath.startsWith(rootPath)) {
            future.fail("Target path escapes storage directory!");
            return;
        }

        String target = targetPath.toString();

        fileSystem.props(target, propsHandler -> {
            if(propsHandler.failed()) {
                // assume file does not exist
                future.complete();
                return;
            }

            if(propsHandler.result().isDirectory()) {
                fileSystem.deleteRecursive(target, recursive != null? recursive : false, delHandler -> {
                    if(delHandler.failed()) {
                        future.fail(delHandler.cause());
                        return;
                    }
                    future.complete();
                });
            }
            else {
                fileSystem.delete(target, delHandler -> {
                    if(delHandler.failed()) {
                        future.fail(delHandler.cause());
                        return;
                    }
                    future.complete();
                });
            }
        });
    }
}
