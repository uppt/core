package de.maltebehrendt.uppt.storage.Impl;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.streams.ReadStream;
import io.vertx.core.streams.WriteStream;

import java.util.zip.ZipInputStream;

public interface StorageBackend {
    // to own storage
    public <T> void persist(String path, ReadStream<T> readStream, Future<Void> future);
    public <T> void persist(String path, Boolean overwrite, ReadStream<T> readStream, Future<Void> future);
    public void unzipAndPersist(String folderPath, Boolean overwrite, ZipInputStream zipInputStream, Long maxTotalExtractedSizeInMB, Long maxNumberOfFiles, Future<JsonObject> future);
    public <T> void getWriteStream(String path, Boolean overwrite, Future<WriteStream<T>> future);

    public <T> void retrieve(String path, WriteStream<T> writeStream, Future<Void> future);
    public void retrieveMetaData(String path, Future<JsonObject> future);
    public <T> void getReadStream(String path, Future<ReadStream<T>> future);

    public void delete(String path, Boolean recursive, Future<Void> future);
}
