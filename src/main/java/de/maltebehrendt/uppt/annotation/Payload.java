package de.maltebehrendt.uppt.annotation;

import de.maltebehrendt.uppt.enums.DataType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by malte on 29.10.16.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Payload {
    String key();
    DataType type() default DataType.STRING;
    String description() default "Not provided";
    String value() default "";
}
