package de.maltebehrendt.uppt.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by malte on 13.02.17.
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Router {
    String domain();
    String version() default "1";
    String type() default "router";
    String description() default "Not provided";
    Authorities authorities();
    Route[] routes() default {};
}
