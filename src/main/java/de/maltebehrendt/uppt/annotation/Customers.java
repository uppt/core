package de.maltebehrendt.uppt.annotation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface  Customers {
    Customer[] value();
}
