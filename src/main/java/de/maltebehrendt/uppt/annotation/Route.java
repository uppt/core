package de.maltebehrendt.uppt.annotation;

import de.maltebehrendt.uppt.enums.Operation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by malte on 13.02.17.
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Route {
    String domain();
    String version() default "1";
    String description() default "Not provided";
    String path() default "*";
    Operation operation();
    Authorities authorities() default @Authorities(isAuthenticationRequired = true);
    Customer[] customers() default {};
}
