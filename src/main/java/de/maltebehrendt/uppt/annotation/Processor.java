package de.maltebehrendt.uppt.annotation;



import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Processor {
    String domain();
    String version() default "1";
    String type() default "event";
    String description() default "Not provided";
    String enablingConditionKey() default "none";
    Payload[] requires() default {};
    Payload[] optional() default {};
    Payload[] provides() default {};
}
