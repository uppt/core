package de.maltebehrendt.uppt.annotation.impl;

import de.maltebehrendt.uppt.annotation.Customer;
import de.maltebehrendt.uppt.annotation.Payload;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.lang.annotation.Annotation;

/**
 * Created by malte on 29.10.16.
 */
public class CustomerImpl implements Customer {


    private String description;

    private String domain;
    private String version;
    private String type;

    private Payload[] provides;
    private Payload[] requires;

    private Boolean requiresProcessor;
    private String enablingConditionKey;

    public CustomerImpl(String description, String domain, String version, String type, Boolean requiresProcessor, String enablingConditionKey) {
        this.description = description;
        this.domain = domain;
        this.version = version;
        this.type = type;
        this.requiresProcessor = requiresProcessor;
        this.enablingConditionKey = enablingConditionKey;
    }

    public JsonObject toJson() {
        return toJson(this);
    }

    public static JsonObject toJson(Customer customer) {
        JsonObject customerJson = new JsonObject()
                .put("description", customer.description())
                .put("domain", customer.domain())
                .put("version", customer.version())
                .put("type", customer.type())
                .put("provides", new JsonArray())
                .put("requires", new JsonArray())
                .put("requiresProcessor", customer.requiresProcessor())
                .put("enablingConditionKey", customer.enablingConditionKey());

        for(Payload provides : customer.provides()) {
            customerJson.getJsonArray("provides")
                    .add(new JsonObject()
                        .put("key", provides.key())
                        .put("description", provides.description())
                        .put("type", provides.type().toString())
                        .put("value", provides.value()));
        }
        for(Payload requires : customer.requires()) {
            customerJson.getJsonArray("requires")
                    .add(new JsonObject()
                            .put("key", requires.key())
                            .put("description", requires.description())
                            .put("type", requires.type().toString())
                            .put("value", requires.value()));
        }


        return customerJson;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String domain() {
        return domain;
    }

    @Override
    public String version() {
        return version;
    }

    @Override
    public String type() {
        return type;
    }

    @Override
    public boolean requiresProcessor() { return requiresProcessor; }

    @Override
    public String enablingConditionKey() {
        return enablingConditionKey;
    }

    @Override
    public Payload[] provides() {
        return provides;
    }

    @Override
    public Payload[] requires() {
        return requires;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRequiresProcessor(Boolean requiresProcessor) {
        this.requiresProcessor = requiresProcessor;
    }

    public void setProvides(Payload[] provides) {
        this.provides = provides;
    }

    public void setRequires(Payload[] requires) {
        this.requires = requires;
    }
}
