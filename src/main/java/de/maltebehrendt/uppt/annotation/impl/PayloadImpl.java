package de.maltebehrendt.uppt.annotation.impl;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.enums.DataType;
import io.vertx.core.json.JsonObject;

import java.lang.annotation.Annotation;

/**
 * Created by malte on 29.10.16.
 */
public class PayloadImpl implements Payload {
    private String key;
    private String value;
    private String description;
    private DataType type;

    public PayloadImpl(DataType type, String key, String value, String description) {
        this.type = type;
        this.key = key;
        this.value = value;
        this.description = description;
    }
    public JsonObject toJson() {
        return toJson(this);
    }

    public static JsonObject toJson(Payload payload) {
        JsonObject payloadJson = new JsonObject()
                .put("key", payload.key())
                .put("value", payload.value())
                .put("description", payload.description())
                .put("type", payload.type().toString());

        return payloadJson;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public DataType type() {
        return type;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        return null;
    }
}
