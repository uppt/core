package de.maltebehrendt.uppt.annotation;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by malte on 29.10.16.
 */


@Repeatable(Customers.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Customer {
    String description() default "Not provided";

    String domain();
    String version() default "1";
    String type() default "event";
    boolean requiresProcessor() default true;
    String enablingConditionKey() default "none";

    Payload[] provides() default {};
    Payload[] requires() default {};
}

