package de.maltebehrendt.uppt.annotation;

/**
 * Created by malte on 19.02.17.
 */
public @interface Authorities {
    boolean isAuthenticationRequired() default true;
    String[] userRoles() default {};
    String[] userIDs() default {};
}
