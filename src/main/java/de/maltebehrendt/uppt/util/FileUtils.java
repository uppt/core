package de.maltebehrendt.uppt.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;

/**
 * Created by malte on 05.02.17.
 */
public class FileUtils {

    public static String calcMD5(String filePath) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] buffer = new byte[1024];
            int numRead;
            InputStream fis = Files.newInputStream(Paths.get(filePath));

            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    md.update(buffer, 0, numRead);
                }
            } while (numRead != -1);

            byte[] digest = md.digest();

            String result = "";

            for (int i=0; i < digest.length; i++) {
                result += Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
            }

            return result;
        }
        catch(Exception e) {
            return null;
        }
    }

    public static boolean deleteFolder(String folderPath) {
        try {
            Files.walkFileTree(Paths.get(folderPath), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }

            });
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean isFileExisting(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }
}
