package de.maltebehrendt.uppt.util;


import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public class EncryptionUtils {
    public static String hash(final String password, final String keyFactory, final String saltString, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( keyFactory );
            byte[] salt = saltString.getBytes("UTF-8");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return Base64.getEncoder().encodeToString(res);

        } catch( NoSuchAlgorithmException | InvalidKeySpecException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String hashForKey(final String password, final String keyFactory, final String saltString, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance( keyFactory );
            byte[] salt = saltString.getBytes("UTF-8");
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            return Base64.getEncoder().encodeToString(res).replace("/", "_");

        } catch( NoSuchAlgorithmException | InvalidKeySpecException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
