package de.maltebehrendt.uppt.util;

import io.vertx.core.logging.Logger;

import java.io.*;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class ShellUtils {
    public static int runOnShellBlocking(String command, String directoryPath, Logger logger) throws InterruptedException, IOException {
        ProcessBuilder processBuilder = new ProcessBuilder();

        if(isWindows()) {
            processBuilder.command("cmd.exe", "/c", command);
        }
        else {
            processBuilder.command("sh", "-c", command);
        }

        if(directoryPath == null || directoryPath.isEmpty()) {
            directoryPath = System.getProperty("java.io.tmpdir");

            if(directoryPath == null || directoryPath.isEmpty()) {
                directoryPath = System.getProperty("user.home");
            }
        }
        processBuilder.directory(new File(directoryPath));
        Process process = processBuilder.start();

        StreamReader streamReader = new StreamReader(process.getInputStream(), output -> {
            if(logger != null) {
                logger.info("Shell output: " + output);
            }
            else {
                System.err.println("Shell output: " + output);
            }
        });
        Executors.newSingleThreadExecutor().submit(streamReader);

        int result = process.waitFor();

        if(result != 0) {
            if(logger != null) {
                logger.error("Failed to execute shell command '" + command + "'! Please check the logs for shell output (info-level)");
            }
            else {
                System.err.println("Failed to execute shell command '" + command + "'!");
            }
        }
        return result;
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().startsWith("windows");
    }


    private static class StreamReader implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamReader(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            new BufferedReader(new InputStreamReader(inputStream)).lines()
                    .forEach(consumer);
        }
    }
}
