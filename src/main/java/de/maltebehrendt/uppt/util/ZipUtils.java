package de.maltebehrendt.uppt.util;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by malte on 04.02.17.
 */
public class ZipUtils {
    static final int BUFFER_SIZE = 4096;

    private static String validateFilename(String filename, String intendedDir) throws java.io.IOException {
        File f = new File(filename);
        String canonicalPath = f.getCanonicalPath();

        File iD = new File(intendedDir);
        String canonicalID = iD.getCanonicalPath();

        if (canonicalPath.startsWith(canonicalID)) {
            return canonicalPath;
        }
        else {
            throw new IllegalStateException("File is outside extraction target directory.");
        }
    }

    // note: not yet async, to be fixed in the future
    public static void UNZIP(String targetDirectoryPath, String zipFilePath, long maxTotalExtractedSizeInMB, int maxNumberOfFiles, Future<Object> future) {
        try {
            File destDir = new File(targetDirectoryPath);
            if (!destDir.exists()) {
                destDir.mkdirs();
            }

            ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
            ZipEntry entry = zipIn.getNextEntry();
            long maxTotalExtractedSize = maxTotalExtractedSizeInMB * 1000000L;
            long totalExtractedSize = 0L;
            int numberOfFiles = 0;

            // iterates over entries in the zip file
            while (entry != null) {
                String filePath = validateFilename(targetDirectoryPath + File.separator + entry.getName(), targetDirectoryPath);
                if (entry.isDirectory()) {
                    File dir = new File(filePath);
                    dir.mkdir();
                }
                else {
                    File newFile = new File(filePath);
                    new File(newFile.getParent()).mkdirs();

                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
                    byte[] bytesIn = new byte[BUFFER_SIZE];
                    int read = 0;
                    while ((totalExtractedSize + BUFFER_SIZE <= maxTotalExtractedSize) && (read = zipIn.read(bytesIn)) != -1) {
                        bos.write(bytesIn, 0, read);
                        totalExtractedSize += read;
                    }
                    bos.close();
                }
                zipIn.closeEntry();
                numberOfFiles++;

                if (numberOfFiles > maxNumberOfFiles) {
                    future.fail("Failed to unzip: numberOfFiles > maxNumberOfFiles (" + maxNumberOfFiles + "). File: " + zipFilePath);
                    zipIn.close();
                    return;
                }
                if (totalExtractedSize + BUFFER_SIZE > maxTotalExtractedSize) {
                    future.fail("Failed to unzip: totalExtractedSize > maxTotalExtractedSize (" + maxTotalExtractedSize + "). File: " + zipFilePath);
                    zipIn.close();
                    return;
                }

                entry = zipIn.getNextEntry();
            }
            zipIn.close();

            future.complete(new JsonObject()
                .put("numberOfFiles", numberOfFiles)
                .put("totalExtractedSize", totalExtractedSize)
                .put("targetDirectory", targetDirectoryPath)
            );
        }
        catch (IOException e) {
            e.printStackTrace();
            future.fail("Failed to unzip: " + zipFilePath + " Cause: " + e.getMessage());
            return;
        }
    }
}
