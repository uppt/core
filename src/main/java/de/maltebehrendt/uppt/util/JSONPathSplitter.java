package de.maltebehrendt.uppt.util;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Stack;

/**
 * Created by malte on 14.02.17.
 */
public class JSONPathSplitter {
    private static final char DOC_CONTEXT = '$';
    private static final char EVAL_CONTEXT = '@';

    private static final char OPEN_SQUARE_BRACKET = '[';
    private static final char CLOSE_SQUARE_BRACKET = ']';
    private static final char OPEN_PARENTHESIS = '(';
    private static final char CLOSE_PARENTHESIS = ')';
    private static final char OPEN_BRACE = '{';
    private static final char CLOSE_BRACE = '}';

    private static final char WILDCARD = '*';
    private static final char PERIOD = '.';
    private static final char BEGIN_FILTER = '?';
    private static final char COMMA = ',';
    private static final char SPLIT = ':';
    private static final char SINGLE_QUOTE = '\'';
    private static final char DOUBLE_QUOTE = '"';
    private static final char SPACE = ' ';

    private static final char AND = '&';
    private static final char OR = '|';
    private static final char GREATER = '>';
    private static final char LESS = '<';
    private static final char EQUAL = '=';
    private static final char TILDE = '~';
    private static final char EXCLAMATION = '!';

    public static JsonObject splitPathIntoStepsAndPredicates(String path) throws Exception {
        JsonArray pathSteps = new JsonArray();
        JsonArray pathPredicates = new JsonArray();

        path = path.trim();
        if(path.charAt(0) != DOC_CONTEXT && path.charAt(0) != EVAL_CONTEXT) {
            path = "$." + path;
        }

        char[] pathChars = path.toCharArray();
        int length = pathChars.length;

        int position = 1;
        while(position < length) {
            position = addNextToken(pathChars, position, pathSteps, pathPredicates);
        }
        return new JsonObject().put("path", path).put("steps", pathSteps).put("predicates", pathPredicates);
    }

    // warning: this method expects well formated JSONPaths + only supports a SUBSET!
    // is not intended for multi-threading
    private static int addNextToken(char[] pathChars, int position, JsonArray steps, JsonArray predicates) throws Exception {
        char currentChar = pathChars[position];
        int length = pathChars.length;

        if(currentChar == PERIOD) {
            position = position + 1;
            currentChar = pathChars[position];

            switch(currentChar) {
                case PERIOD: // is second period = wildcard
                    steps.add("*");
                    predicates.add(new JsonArray().add(new JsonObject().put("type", "wildcard").put("value", "*")));
                    break;
                case WILDCARD:
                    steps.add("*");
                    predicates.add(new JsonArray().add(new JsonObject().put("type", "wildcard").put("value", "*")));
                    position = position + 1;
                    break;
                case OPEN_SQUARE_BRACKET:
                    // force next token analysis, this is likely a bracket-notated child
                    break;
                default:
                    String name = "";
                    while (currentChar != 0 && currentChar != OPEN_SQUARE_BRACKET && currentChar != PERIOD) {
                        name += currentChar;
                        position = position +1;
                        currentChar = position < length? pathChars[position] : 0;
                    }
                    steps.add(name);
                    predicates.add(new JsonArray());
                    break;
            }
        }
        else if (currentChar == OPEN_SQUARE_BRACKET) {
            position = position + 1;
            currentChar = pathChars[position];

            if(currentChar == SINGLE_QUOTE || currentChar == DOUBLE_QUOTE) {
                // bracket-notated child
                position = position + 1;
                currentChar = pathChars[position];
                String name = "";

                while(currentChar != 0 && currentChar != SINGLE_QUOTE && currentChar != DOUBLE_QUOTE) {
                    name += currentChar;
                    position = position + 1;
                    currentChar = position < length? pathChars[position] : 0;
                }
                steps.add(name);
                predicates.add(new JsonArray());
                position = position + 2;
            }
            else if(currentChar == WILDCARD) {
                predicates.add(new JsonArray().add(new JsonObject().put("type", "wildcard").put("value", "*")));
                position = position + 2;
                currentChar = pathChars[position];
            }
            else if(currentChar == BEGIN_FILTER) {
                // filter
                // for now only extract without further analysis/break-down
                position = position + 1;
                currentChar = pathChars[position];
                if(currentChar != OPEN_PARENTHESIS) {
                    throw new Exception("Illegal character at position " + position + ". The start of a filter must be followed by a '('. Actual: " + currentChar);
                }
                String filter = "";
                do {
                    filter += currentChar;
                    position = position + 1;
                    currentChar = pathChars[position];
                } while(currentChar != CLOSE_SQUARE_BRACKET);

                // Break down further...
                predicates.add(evaluateParenthesis(filter));
                position = position + 1;
            }
            else {
                // check substring until close square bracket
                boolean isIndices = false;
                boolean isSlice = false;
                int startPosition = position;
                while(currentChar != CLOSE_SQUARE_BRACKET) {
                    if(currentChar == COMMA) {
                        // array indices
                        isIndices = true;
                    }
                    else if(currentChar == SPLIT) {
                        // array slice
                        isSlice = true;
                    }

                    position = position + 1;
                    currentChar = pathChars[position];
                }
                if(isIndices) {
                    String[] indicesString = new String(pathChars, startPosition, position-startPosition).split(",");
                    JsonArray indices = new JsonArray();
                    for(String indexString : indicesString) {
                        int index = Integer.parseInt(indexString);
                        indices.add(index);
                    }
                    predicates.getJsonArray(predicates.size()-1).add(new JsonObject().put("type", "indices").put("value", indices));
                }
                else if(isSlice) {
                    String[] indicesString = new String(pathChars, startPosition, position-startPosition).split(":");
                    JsonArray indices = new JsonArray();
                    if(indicesString.length == 2 && !indicesString[0].isEmpty()) {
                        int start = Integer.parseInt(indicesString[0]);
                        int end = Integer.parseInt(indicesString[1]);
                        for(int i=start;i<end;i++) {
                            indices.add(i);
                        }
                    }
                    else {
                        if(pathChars[startPosition] == SPLIT) {
                            // is from start
                            int end = Integer.parseInt(indicesString[1]);
                            for(int i=0;i<end;i++) {
                                indices.add(i);
                            }
                            predicates.getJsonArray(predicates.size()-1).add(new JsonObject().put("type", "indices").put("value", indices));
                        }
                        else {
                            // is from tail
                            int fromTail = Integer.parseInt(indicesString[0]);
                            predicates.getJsonArray(predicates.size()-1).add(new JsonObject().put("type", "fromTail").put("value", fromTail));
                        }
                    }
                }
                else {
                    String indexString = new String(pathChars, startPosition, position-startPosition);
                    int index = Integer.parseInt(indexString);
                    predicates.getJsonArray(predicates.size()-1).add(new JsonObject().put("type", "index").put("value", index));
                }
                position = position + 1;
            }
        }
        else {
            throw new Exception("Illegal character at position " + position + ". Expected '.' or '['. Is: " + currentChar);
        }

        return position;
    }

    // very difficult to read and recursive... just bad -> to be improved!
    private static JsonArray evaluateParenthesis(String parenthesisFilter) throws Exception {
        Stack<String> values = new Stack<>();
        Stack<String> operators = new Stack<>();
        JsonArray predicates = new JsonArray();
        Stack<JsonObject> predicateStack = new Stack<>();
        Stack<JsonObject> orStack = new Stack<>();
        Stack<JsonObject> andStack = new Stack<>();

        for(int i=0;i<parenthesisFilter.length();i++) {
            Character c = parenthesisFilter.charAt(i);

            if(c == DOC_CONTEXT) {
                throw new Exception("Illegal character $ (DOC_CONTEXT) at position " + i + ". Only @ (EVAL_CONTEXT) is allowed.");
            }
            else if(c == EVAL_CONTEXT && (i+1)<parenthesisFilter.length() && parenthesisFilter.charAt(i+1) == PERIOD) {
                // drop it
                i++;
                continue;
            }
            else if(Character.isLetter(c) || Character.isDigit(c)) {
                StringBuffer stringBuffer = new StringBuffer();
                if(c == PERIOD) {
                    i++;
                }

                while(i<parenthesisFilter.length() && (Character.isLetter(parenthesisFilter.charAt(i)) || Character.isDigit(parenthesisFilter.charAt(i)) || parenthesisFilter.charAt(i) == PERIOD)) {
                    stringBuffer.append(parenthesisFilter.charAt(i));
                    i=i+1;
                }
                values.push(stringBuffer.toString());
                i=i-1;
            }
            else if(c == OPEN_PARENTHESIS) {
                if(i == 0) {
                    operators.push(c.toString());
                }
                else {
                    // is a subparenthesis
                    int startChar = i;
                    while(i<parenthesisFilter.length() && parenthesisFilter.charAt(i) != CLOSE_PARENTHESIS) {
                        i++;
                    }
                    JsonArray subPredicates = evaluateParenthesis(parenthesisFilter.substring(startChar, i+1));
                    if(!andStack.empty()) {
                        andStack.push(subPredicates.getJsonObject(0));
                    }
                    else if(!orStack.empty()) {
                        orStack.push(subPredicates.getJsonObject(0));
                    }
                    else {
                        predicateStack.add(subPredicates.getJsonObject(0));
                    }
                }
            }
            else if(c == CLOSE_PARENTHESIS) {
                while(!operators.empty() && !"(".equals(operators.peek()) && !values.empty()) {
                    if(values.size() == 1) {
                        if(!andStack.empty()) {
                            andStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                        }
                        else if(!orStack.isEmpty()) {
                            orStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                        }
                        else {
                            predicateStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                        }
                    }
                    else if(values.size() > 1) {
                        if(!andStack.empty()) {
                            andStack.add(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                        }
                        else if(!orStack.isEmpty()) {
                            orStack.add(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                        }
                        else {
                            predicateStack.add(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                        }
                    }
                }
                if(!operators.empty() && "(".equals(operators.peek()) && values.size() == 1) {
                    if(!andStack.empty()) {
                        andStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                    }
                    else if(!orStack.isEmpty()) {
                        orStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                    }
                    else {
                        predicateStack.add(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                    }
                    operators.pop();
                }
                else {
                    operators.pop();
                }

                if(!andStack.empty()) {
                    JsonArray andClauses = new JsonArray();
                    while(!andStack.isEmpty()) {
                        andClauses.add(andStack.pop());
                    }
                    predicateStack.push(new JsonObject().put("type", "andClauses").put("clauses", andClauses));
                }
                if(!orStack.empty()) {
                    JsonArray orClauses = new JsonArray();
                    while(!orStack.isEmpty()) {
                        orClauses.add(orStack.pop());
                    }
                    predicateStack.push(new JsonObject().put("type", "orClauses").put("clauses", orClauses));
                }
            }
            else if(c == GREATER || c == LESS || c == EQUAL || c == TILDE || c==EXCLAMATION) {
                String operator = c.toString();
                if(i+1 < parenthesisFilter.length()) {
                    Character nextC = parenthesisFilter.charAt(i+1);
                    if(nextC == GREATER || nextC == LESS || nextC == EQUAL || nextC == TILDE || nextC==EXCLAMATION) {
                        operator += nextC.toString();
                        i++;
                    }
                }
                while(!operators.empty() && hasPrecedence(operator, operators.peek())) {
                    predicateStack.push(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                }
                operators.push(operator);
            }
            else if(c == AND && (i+1)<parenthesisFilter.length() && parenthesisFilter.charAt(i+1) == AND) {
                if(!operators.empty() && values.size() == 1) {
                    andStack.push(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));
                }
                else if(!operators.empty() && values.size() > 1) {
                    andStack.push(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                }
                else if(values.size() == 0 && !predicateStack.empty()) {
                    // previous predicate belongs to this one
                    andStack.push(predicateStack.pop());
                }
                i++;
            }
            else if(c == OR && (i+1)<parenthesisFilter.length() && parenthesisFilter.charAt(i+1) == OR) {
                if(!operators.empty() && values.size() == 1) {
                    // only a exist operator
                    orStack.push(new JsonObject().put("type", "filter").put("operator", "hasProperty").put("argument1", values.pop()));

                }
                else if(!operators.empty() && values.size() > 1) {
                    orStack.push(new JsonObject().put("type", "filter").put("operator", operators.pop()).put("argument2", values.pop()).put("argument1", values.pop()));
                }
                else if(values.size() == 0 && !predicateStack.empty()) {
                    // previous predicate belongs to this one
                    orStack.push(predicateStack.pop());
                }
                i++;
            }
        }

        for(JsonObject object : predicateStack) {
            predicates.add(object);
        }

        return predicates;
    }

    private static boolean hasPrecedence(String operator1, String operator2) {
        if("(".equals(operator2) || ")".equals(operator2)) {
            return false;
        }
        return true;
    }
}
