package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.*;
import de.maltebehrendt.uppt.annotation.impl.CustomerImpl;
import de.maltebehrendt.uppt.annotation.impl.PayloadImpl;
import de.maltebehrendt.uppt.database.Database;
import de.maltebehrendt.uppt.database.impl.ArangoDBImpl;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Impl.MessageImpl;
import de.maltebehrendt.uppt.events.Message;
import de.maltebehrendt.uppt.logging.LoggerProxy;
import de.maltebehrendt.uppt.storage.Storage;
import de.maltebehrendt.uppt.util.JSONPathSplitter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.MessageSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by malte on 26.10.16.
 */
public abstract class AbstractService extends AbstractVerticle {
    protected static LoggerProxy logger = null;
    protected EventBus eventBus = null;
    protected ServiceDiscovery serviceDiscovery = null;
    protected JsonObject customers = null;
    protected JsonObject processors = null;
    protected JsonObject routes = null;
    protected JsonObject routers = null;
    protected String serviceName = null;
    protected String instanceID = null;
    protected String serviceID = null;
    protected Object serviceInstance = null;
    protected Database database = null;
    protected Storage storage = null;
    private LinkedList<MessageConsumer> processorConsumers = null;

    //TODO clean up variables (probably too many stay in RAM)

    @Override
    public void start(Future<Void> startFuture) {
        serviceInstance = castClass();
        serviceName = serviceInstance.getClass().getCanonicalName();
        serviceID = UUID.randomUUID().toString();
        eventBus = vertx.eventBus();
        logger = new LoggerProxy(serviceInstance.getClass());
        serviceDiscovery = ServiceDiscovery.create(vertx);
        customers = new JsonObject();
        processors = new JsonObject();
        processorConsumers = new LinkedList<>();
        routers = new JsonObject();
        routes = new JsonObject();
        instanceID = config().getString("instanceID");

        // setup the database
        if(config().containsKey("database")) {
            JsonObject databaseSettings = config().getJsonObject("database");
            if(!databaseSettings.isEmpty() && (!databaseSettings.containsKey("isDatabaseDisabled") || databaseSettings.getBoolean("isDatabaseDisabled") == false)) {
                try {
                    this.database = new ArangoDBImpl(serviceName, databaseSettings);
                    //this.orientDB = new OrientDB(databaseSettings.getString("engine") + ":" + databaseSettings.getString("address"), databaseSettings.getString("username"), databaseSettings.getString("password"), OrientDBConfig.defaultConfig());
                    logger.info(serviceID, serviceName, 200, "Prepared database access using user " + databaseSettings.getString("username"));
                } catch (Exception e) {
                    startFuture.fail("Preparing database access for " + serviceName + " on " + instanceID + " using user " + databaseSettings.getString("username") + "failed: " + e.getMessage());
                    return;
                }
            }
        }

        // setup storage
        if(config().containsKey("storage")) {
            JsonObject storageConfig = config().getJsonObject("storage");
            if(!storageConfig.isEmpty() && (!storageConfig.containsKey("isStorageDisabled") || storageConfig.getBoolean("isStorageDisabled") == false)) {
                try {
                    this.storage = new Storage(storageConfig.getJsonObject("ownStorage"), storageConfig.getJsonObject("sharedStorage"), storageConfig.getJsonObject("usersStorage"), storageConfig.getJsonObject("publicStorage"), vertx);
                } catch (Exception e) {
                    startFuture.fail("Preparing storage access for " + serviceName + " on " + instanceID + "failed: " + e.getMessage());
                    return;
                }
            }
        }

        // call the service's own init code
        vertx.executeBlocking(future -> {
            this.prepare(future);
        }, prepareResult -> {
            if(prepareResult.failed()) {
                startFuture.fail("Failed: starting up " + serviceName + " due to prepare failure: " + prepareResult.cause());
                return;
            }

            // first register message processors without dependencies on other processors
            // and make sure the routes are known for later router registration
            List<Future> routeFutures = new LinkedList<>();
            for(Method method : serviceInstance.getClass().getMethods()) {
                if (method.isAnnotationPresent(Processor.class)) {
                    registerProcessor(method, serviceInstance);
                }
                if(method.isAnnotationPresent(Route.class)) {
                    Route route = method.getAnnotation(Route.class);
                    Future<Void> routeFuture = Future.future();
                    routeFutures.add(routeFuture);
                    ensureProcessorAvailability(new JsonObject()
                                    .put("domain",route.domain())
                                    .put("version",route.version())
                                    .put("type", "route:" + route.operation().toString() + ":" + route.path())
                                    .put("requiresProcessor", true)
                                    .put("description",route.description())
                                    .put("path",route.path())
                            , null, routeFuture);
                }
            }


            // then register customers which depend on processors and aggregators (processors with dependencies)
            List<Future> futures = new LinkedList<>();
            for(Method method : serviceInstance.getClass().getMethods()) {
                if(method.isAnnotationPresent(Customers.class)) {
                    Customers customerAnnotations = method.getAnnotation(Customers.class);
                    for(Customer customerAnnotation : customerAnnotations.value()) {
                        Future<Void> availabilityFuture = Future.future();
                        futures.add(availabilityFuture);
                        ensureProcessorAvailability(CustomerImpl.toJson(customerAnnotation), null, availabilityFuture);
                    }
                }
                if(method.isAnnotationPresent(Customer.class)) {
                    for(Customer customerAnnotation : method.getAnnotationsByType(Customer.class)) {
                        Future<Void> availabilityFuture = Future.future();
                        futures.add(availabilityFuture);
                        ensureProcessorAvailability(CustomerImpl.toJson(customerAnnotation), null, availabilityFuture);
                    }
                }
                if(method.isAnnotationPresent(Aggregator.class)) {
                    Future<Void> availabilityFuture = Future.future();
                    futures.add(availabilityFuture);

                    Aggregator aggregatorAnnotation = method.getAnnotation(Aggregator.class);

                    List<Future> customerFutures = new LinkedList<>();
                    for(Customer customer : aggregatorAnnotation.customers()) {
                        Future<Void> customerFuture = Future.future();
                        customerFutures.add(customerFuture);
                        ensureProcessorAvailability(CustomerImpl.toJson(customer), null, customerFuture);
                    }

                    if(!customerFutures.isEmpty()) {
                        CompositeFuture.all(customerFutures).setHandler(result -> {
                            if (result.succeeded()) {
                                registerProcessor(method, serviceInstance);
                                availabilityFuture.complete();
                            } else {
                                logger.fatal(serviceID, serviceName,500, "Failed: registering aggregator " + method.getName(), result.cause());
                                availabilityFuture.fail(result.cause());
                            }
                        });
                    }
                    else {
                        registerProcessor(method, serviceInstance);
                        availabilityFuture.complete();
                    }
                }
                if(method.isAnnotationPresent(Route.class)) {
                    Future<Void> availabilityFuture = Future.future();
                    futures.add(availabilityFuture);

                    Route routeAnnotation = method.getAnnotation(Route.class);

                    // make sure all required processors are available
                    List<Future> customerFutures = new LinkedList<>();
                    for(Customer customer : routeAnnotation.customers()) {
                        Future<Void> customerFuture = Future.future();
                        customerFutures.add(customerFuture);
                        ensureProcessorAvailability(CustomerImpl.toJson(customer), null, customerFuture);
                    }

                    if(!customerFutures.isEmpty()) {
                        CompositeFuture.all(customerFutures).setHandler(result -> {
                            if(result.succeeded()) {
                                registerRoute(method, routeAnnotation);
                                availabilityFuture.complete();
                            }
                            else {
                                logger.fatal(serviceID, serviceName, 500, "Failed: registering route " + method.getName(), result.cause());
                                availabilityFuture.fail(result.cause());
                            }
                        });
                    }
                    else {
                        registerRoute(method, routeAnnotation);
                        availabilityFuture.complete();
                    }

                }
                if(method.isAnnotationPresent(Router.class)) {
                    // check if required methods are available
                    try {
                        serviceInstance.getClass().getMethod("handleSessionBecameAvailable", JsonObject.class);
                        serviceInstance.getClass().getMethod("handleSessionBecameAuthenticated", JsonObject.class);
                        serviceInstance.getClass().getMethod("handleSessionBecameUnavailable", JsonObject.class);
                        serviceInstance.getClass().getMethod("processSessionStateMissing", String.class, Future.class);
                    } catch (NoSuchMethodException e) {
                        startFuture.fail("Router annotation was used in Class which does not extend AbstractSolution (i.e. implements handleSessionBecameAvailable and handleSessionBecameAuthenticated)!");
                        return;
                    }

                    Router routerAnnotation = method.getAnnotation(Router.class);
                    if(!routerAnnotation.domain().startsWith("in.")) {
                        logger.fatal(serviceID, serviceName, 400, "Domain for router " + method.getName() + " does not start with 'in.'!");
                        startFuture.fail("Domain for router " + method.getName() + " does not start with 'in.'!");
                        return;
                    }

                    Future<Void> availabilityFuture = Future.future();
                    futures.add(availabilityFuture);
                    List<Future> dependenciesFuture = new LinkedList<>();

                    // make sure webbridge is available
                    Future<Void> bridgeFuture = Future.future();
                    dependenciesFuture.add(bridgeFuture);
                    ensureProcessorAvailability(new JsonObject()
                                .put("domain", "bridge")
                                .put("version", "1")
                                .put("type", "bridgeMissing")
                                .put("requiresProcessor", true)
                            , null, bridgeFuture);

                    // make sure all external (additional/optional) routes are available
                    for(Route route : routerAnnotation.routes()) {
                        Future<Void> routeFuture = Future.future();
                        dependenciesFuture.add(routeFuture);
                        ensureProcessorAvailability(new JsonObject()
                                .put("domain",route.domain())
                                .put("version",route.version())
                                .put("type", "route:" + route.operation().toString() + ":" + route.path())
                                .put("requiresProcessor", true)
                                .put("description",route.description())
                                .put("path",route.path())
                                , null, routeFuture);
                    }
                    // make sure routes defined in this service are available
                    dependenciesFuture.addAll(routeFutures);

                    CompositeFuture.all(dependenciesFuture).setHandler(result -> {
                        if(result.succeeded()) {
                            registerRouter(method, routerAnnotation);
                            availabilityFuture.complete();
                        }
                        else {
                            logger.fatal(serviceID, serviceName, 500, "Failed: registering router " + method.getName(), result.cause());
                            availabilityFuture.fail(result.cause());
                        }
                    });
                }
            }

            if(config().containsKey("clientSideTraitsConfig") && !config().getJsonArray("clientSideTraitsConfig").isEmpty()) {
                // wait for processor (bridgeWeb) availability and ensure error message if client-side traits are not enabled
                Future<Void> availabilityFuture = Future.future();
                futures.add(availabilityFuture);

                ensureProcessorAvailability(new JsonObject()
                                .put("domain", "sessions")
                                .put("version", "1")
                                .put("type", "clientTraitConfigMissing")
                                .put("requiresProcessor", true)
                                .put("description", "Sets client-side traits")
                        , null, availabilityFuture);
            }

            if(config().containsKey("clientSideTraits") && !config().getJsonArray("clientSideTraits").isEmpty()) {
                // wait for processor (bridgeWeb) availability and ensure error message if client-side traits are not enabled
                Future<Void> availabilityFuture = Future.future();
                futures.add(availabilityFuture);

                ensureProcessorAvailability(new JsonObject()
                                .put("domain", "sessions")
                                .put("version", "1")
                                .put("type", "clientTraitConfigMissing")
                                .put("requiresProcessor", true)
                                .put("description", "Sets client-side traits")
                        , null, availabilityFuture);
            }

            if(config().containsKey("publicFilesAliases") && !config().getJsonArray("publicFilesAliases").isEmpty()) {
                // wait for processor (bridgeWeb) availability and ensure error message if client-side traits are not enabled
                Future<Void> availabilityFuture = Future.future();
                futures.add(availabilityFuture);

                // not entirely precise, as the processors for other alias types are not required - but the bridge should provide all so if one is available so are the others
                ensureProcessorAvailability(new JsonObject()
                                .put("domain", "publicFiles")
                                .put("version", "1")
                                .put("type", "missingAlias")
                                .put("requiresProcessor", true)
                                .put("description", "Sets aliases for accessing public files")
                        , null, availabilityFuture);
            }

            CompositeFuture.all(futures).setHandler(result -> {
                if(result.failed()) {
                    logger.fatal(serviceID, serviceName, 500, "Failed: starting up service!", result.cause());
                    startFuture.fail("Failed: starting up " + serviceName + "!");
                    return;
                }
                // publish processors and customers of this service
                this.publishProcessorsAndCustomers();

                List<Future> configFutures = new LinkedList<>();

                // distribute the required traits
                JsonArray clientSideTraitsConfig = config().containsKey("clientSideTraits") ? config().getJsonArray("clientSideTraits") : null;
                if(clientSideTraitsConfig != null && !clientSideTraitsConfig.isEmpty()) {
                    for(int i=0;i<clientSideTraitsConfig.size();i++) {
                        Future<Void> configFuture = Future.future();
                        configFutures.add(configFuture);

                        JsonObject clientSideTrait = clientSideTraitsConfig.getJsonObject(i);

                        // check that provided patterns are valid...
                        clientSideTrait.fieldNames().forEach(key -> {
                            if(key.endsWith("Regex")) {
                                String pattern = clientSideTrait.getString(key);
                                try {
                                    Pattern.compile(pattern);
                                } catch (PatternSyntaxException exception) {
                                    logger.fatal(serviceID, serviceName, 400, "Client-side trait config contains invalid regex. Key: " + key, exception);
                                    configFuture.fail("Client-side trait config contains invalid regex. Key: " + key + ". Message: " + exception.getDescription());
                                }
                            }
                        });
                        send("sessions","1","clientTraitConfigMissing", clientSideTrait, reply -> {
                            if(reply.failed()) {
                                configFuture.fail(reply.cause());
                                return;
                            }
                            if(reply.statusCode() != 200) {
                                configFuture.fail(reply.getMessage());
                                return;
                            }
                            configFuture.complete();
                        });
                    }
                }

                // register the required aliases
                JsonArray aliasesConfig = config().containsKey("publicFilesAliases") ? config().getJsonArray("publicFilesAliases") : null;
                if(aliasesConfig != null && !aliasesConfig.isEmpty()) {
                    for(int i=0;i<aliasesConfig.size();i++) {
                        Future<Void> configFuture = Future.future();
                        configFutures.add(configFuture);

                        JsonObject aliasConfig = aliasesConfig.getJsonObject(i);
                        String aliasType = null;

                        // determine the type of alias
                        if(aliasConfig.containsKey("aliasRegexPrefix")) {
                            // regex alias
                            aliasType = "missingRegexAlias";
                        }
                        else if(aliasConfig.containsKey("isClearanceRequired") && aliasConfig.getBoolean("isClearanceRequired")) {
                            aliasType = "missingAliasWithClearance";
                        }
                        else if(aliasConfig.containsKey("isDynamicPath") && aliasConfig.getBoolean("isDynamicPath")) {
                            aliasType = "missingAliasWithDynamicPath";
                        }
                        else if(aliasConfig.containsKey("aliasParameterPrefix")) {
                            aliasType = "missingAliasWithNotification";
                        }
                        else if(aliasConfig.containsKey("alias")) {
                            aliasType = "missingAlias";

                        }
                        else {
                            configFuture.fail("Unable to determine alias type of " + aliasConfig.encode());
                            return;
                        }

                        send("publicFiles", "1", aliasType, aliasConfig, reply -> {
                            if(reply.failed()) {
                                configFuture.fail(reply.cause());
                                return;
                            }
                            if(reply.statusCode() != 200) {
                                configFuture.fail(reply.getMessage());
                                return;
                            }
                            configFuture.complete();
                        });
                    }
                }

                CompositeFuture.all(configFutures).setHandler(configResult -> {
                    if(configResult.failed()) {
                        logger.fatal(serviceID, serviceName, 500, "Failed: starting up service!", configResult.cause());
                        startFuture.fail("Failed: starting up " + serviceName + "!");
                        return;
                    }

                    // notify runner that start up is complete
                    startFuture.complete();
                    // everything ready for consumption
                    this.startConsuming();
                });

            });
        });
    }

    @Customer(
            description = "Publishes processors and customers used/implemented by this service",
            domain = "system",
            version = "1",
            type= "pacUpdate",
            requiresProcessor = false,
            provides = {
                    @Payload(key = "serviceName", description = "Name of the customer's service (canonical name)", type = DataType.STRING),
                    @Payload(key = "instanceID", description = "Instance running the customer's service", type = DataType.STRING),
                    @Payload(key = "processors", description = "List of processors implemented by this service", type = DataType.JSONArray),
                    @Payload(key = "customers", description = "List of customers implemented by this service", type = DataType.JSONArray),
                    @Payload(key = "routes", description = "List of routes provided by this service", type = DataType.JSONArray),
                    @Payload(key = "routers", description = "List of routers provided by this service", type = DataType.JSONArray)
            })
    public void publishProcessorsAndCustomers() {
        this.publish("system", "1", "pacUpdate", new JsonObject()
                .put("serviceName", serviceName)
                .put("instanceID", instanceID)
                .put("processors", processors)
                .put("customers", customers)
                .put("routes", routes)
                .put("routers", routers)
        );
    }

    //TODO also check if requires/provides matches
    private void ensureProcessorAvailability(JsonObject customerInfo, String correlationID, Future<Void> availabilityFuture) {
        String address = customerInfo.getString("domain") + "." + customerInfo.getString("version") + "." + customerInfo.getString("type");

        // if there is a config condition don't enforce processor availability if condition is not met
        String configConditionKey = customerInfo.getString("configConditionKey");
        if(configConditionKey != null && !"none".equalsIgnoreCase(configConditionKey) && !config().getBoolean(configConditionKey)) {
            logger.info(serviceID, serviceName, 201, "Processor availability for address " + address + " not enforced as configConditionKey is not set to true");
            availabilityFuture.complete();
            return;
        }

        serviceDiscovery.getRecord(new JsonObject().put("name", address), result -> {
            if((result.failed() || result.result() == null) && customerInfo.getBoolean("requiresProcessor")) {
                String currentCID = correlationID != null? correlationID:UUID.randomUUID().toString();
                String message = "Processor for address " + address + " not (yet?) available: \n\tCause: " + result.cause() + "\n\tRetrying in 2500ms...";
                logger.warn(serviceID, serviceName, 300, message);

                publish("system", "1", "registeringNewCustomer","503", currentCID, new JsonObject().put("serviceName", serviceName).put("address", address).put("message", message));

                vertx.setTimer(2500, retry -> {
                    ensureProcessorAvailability(customerInfo, currentCID, availabilityFuture);
                });
            }
            else {

                JsonArray addressCustomers = customers.getJsonArray(address);
                if(addressCustomers == null) {
                    addressCustomers = new JsonArray();
                    customers.put(address, addressCustomers);
                }

                boolean isUnknown = true;
                for(int i=0;i<addressCustomers.size();i++) {
                    JsonObject addressCustomer = addressCustomers.getJsonObject(i);

                    if(addressCustomer.getString("domain").equalsIgnoreCase(customerInfo.getString("domain"))
                            && addressCustomer.getString("type").equalsIgnoreCase(customerInfo.getString("type"))
                            && addressCustomer.getString("version").equalsIgnoreCase(customerInfo.getString("version"))
                            ) {
                        isUnknown = false;
                        break;
                    }
                }
                if(isUnknown) {
                    addressCustomers.add(customerInfo);
                }

                String currentCID = correlationID != null? correlationID:UUID.randomUUID().toString();
                logger.info(currentCID, serviceName, 201, "Registered customer for address " + address + ".");
                publish("system", "1", "registeringNewCustomer","200", currentCID, new JsonObject().put("serviceName", serviceName).put("address", address));
                availabilityFuture.complete();
            }
        });
    }

    private void registerRouter(Method method, Router routerAnnotation) {
        final String address = routerAnnotation.domain() + "." + routerAnnotation.version() + "." + routerAnnotation.type();

        // prepare WebBridge bridge request(s)
        JsonObject authorities = new JsonObject();
        JsonArray roles = new JsonArray();
        JsonArray users = new JsonArray();

        String[] userRoles = routerAnnotation.authorities().userRoles();
        String[] userIDs = routerAnnotation.authorities().userIDs();
        if(userRoles != null && userRoles.length > 0) {
            for(String role : userRoles) {
                roles.add(role);
            }
        }
        if(routerAnnotation.authorities().isAuthenticationRequired() && roles.isEmpty()) {
           roles.add("authenticated");
        }

        if(userIDs != null && userIDs.length > 0) {
            for(String user : userIDs) {
                users.add(user);
            }
        }
        if(!roles.isEmpty()) {
            authorities.put("roles", roles);
        }
        if(!users.isEmpty()) {
            authorities.put("users", users);
        }


        // convert annotation into JSON Object
        JsonObject routerRecord = new JsonObject()
                .put("description", routerAnnotation.description())
                .put("domain", routerAnnotation.domain())
                .put("version", routerAnnotation.version())
                .put("type", routerAnnotation.type())
                .put("category", "Router")
                .put("authorities", authorities)
                .put("routes", new JsonObject());

        // compile a map of all routed routes...
        JsonArray routeArray = new JsonArray();

        // ...external/optional routes...
        for(Route route : routerAnnotation.routes()) {
            String type = "route:" + route.operation().toString() + ":" + route.path();

            JsonArray customers = new JsonArray();
            for(Customer customer : route.customers()) {
                customers.add(CustomerImpl.toJson(customer));
            }

            routeArray.add(new JsonObject()
                    .put("description", route.description())
                    .put("domain", route.domain())
                    .put("version", route.version())
                    .put("operation", route.operation().toString())
                    .put("type", type)
                    .put("category", "Route")
                    .put("path", route.path())
                    .put("customers", customers));
        }
        // ...class internal routes...
        String actualDomain = routerRecord.getString("domain").substring(3);
        for(String key : routes.fieldNames()) {
            JsonObject route = routes.getJsonObject(key);
            // add those with the same domain/version
            if(actualDomain.equals(route.getString("domain"))
                    && routerRecord.getString("version").equals(route.getString("version"))) {
                routeArray.add(route);
            }
        }

        // register the routes
        for(int i=0;i<routeArray.size();i++) {
            JsonObject route = routeArray.getJsonObject(i);
            String domain = route.getString("domain");
            String version = route.getString("version");
            String operation = route.getString("operation");
            String path = route.getString("path");
            String type = route.getString("type");

            JsonObject routeMap = routerRecord.getJsonObject("routes");
            JsonArray pathSteps = new JsonArray();
            String[] pathStepArray = path.split("\\.");

            for(int a=0;a<pathStepArray.length;a++) {
                pathSteps.add(pathStepArray[a]);
            }

            // fill the routeMap for later routing
            // JsonObjects are probably not the most efficient choice. To be improved later
            String addressPrefix = domain + "." + version + "." + operation;
            if(!routeMap.containsKey(addressPrefix)) {
                routeMap.put(addressPrefix, new JsonArray());
            }
            routeMap.getJsonArray(addressPrefix).add(new JsonObject()
                    .put("pathSteps", pathSteps)
                    .put("path", path)
                    .put("type", type)
            );
        }

        //logger.info("Registered routes for router " + routerAnnotation.domain() + ": " + routeArray.encodePrettily());

        // request a new bridge to the outside world using the annotated authorities
        requestRouterBridge(routerAnnotation.domain(), routerAnnotation.version(), routerAnnotation.type(), true, false, authorities);

        // this is a very naive implementation and needs to be optimized
        io.vertx.core.eventbus.MessageConsumer consumer = eventBus.consumer(address);
        consumer.handler(new Handler<io.vertx.core.eventbus.Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                MessageImpl upptMessageImpl;

                try {
                    upptMessageImpl = new MessageImpl(message);
                    JsonObject body = upptMessageImpl.getBodyAsJsonObject();

                    // is request from client
                    if(body.containsKey("requests")) {
                        JsonArray requests = body.getJsonArray("requests");
                        if (requests == null || requests.isEmpty()) {
                            String errorMessage = "Missing required payload element JSONArray requests. Must contain: domain, version, path, operation, (value).";
                            upptMessageImpl.fail(400, errorMessage);
                            logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                            return;
                        }

                        // 1. Normalize the requested paths and build up the route Infos
                        List<Future> routeResultFutures = new LinkedList<>();
                        Pattern illegalSetPathPattern = Pattern.compile("[*&()<>\\+\\?\\|]");
                        for (int i = 0; i < requests.size(); i++) {
                            JsonObject request = requests.getJsonObject(i);
                            String path = request.getString("path");
                            String domain = request.getString("domain");
                            String version = request.getString("version");
                            String operation = request.getString("operation");
                            String elementOrigin = request.getString("origin") == null ? upptMessageImpl.origin() : request.getString("origin");
                            Object value = request.getValue("value");

                            if (path == null || path.isEmpty()) {
                                String errorMessage = "Missing required payload element String path in request " + i;
                                upptMessageImpl.fail(400, errorMessage);
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                return;
                            }
                            if (operation == null || operation.isEmpty()) {
                                String errorMessage = "Missing required payload element operation in request " + i;
                                upptMessageImpl.fail(400, errorMessage);
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                return;
                            }
                            if (domain == null || domain.isEmpty()) {
                                String errorMessage = "Missing required payload element String domain in request " + i;
                                upptMessageImpl.fail(400, errorMessage);
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                return;
                            }
                            if (version == null || version.isEmpty()) {
                                String errorMessage = "Missing required payload element String version in request " + i;
                                upptMessageImpl.fail(400, errorMessage);
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                return;
                            }

                            String addressPrefix = domain + "." + version + "." + operation;
                            JsonArray routerRoutes = routers.getJsonObject(address).getJsonObject("routes").getJsonArray(addressPrefix);

                            if(routerRoutes == null) {
                                routerRoutes = routers.getJsonObject(address).getJsonObject("routes").getJsonArray(domain + "." + version + ".ANY");
                            }
                            if(routerRoutes == null) {
                                String errorMessage = "Operation " + operation + " in version " + version + " of domain " + domain + " of path " + path + " is not routed by this router!";
                                upptMessageImpl.fail(404, errorMessage);
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                return;
                            }
                            if ("SET".equals(operation)) {
                                if (illegalSetPathPattern.matcher(path).find()) {
                                    String errorMessage = "Set operation only allows absolute paths! Illegal characters detected in " + path;
                                    upptMessageImpl.fail(400, errorMessage);
                                    logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                    return;
                                } else if (value == null) {
                                    String errorMessage = "Set operation requires a value to be provided";
                                    logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                    upptMessageImpl.fail(400, errorMessage);
                                    return;
                                }
                            }

                            JsonObject pathInfo = JSONPathSplitter.splitPathIntoStepsAndPredicates(path);

                            // 2. match routes to paths
                            JsonArray pathSteps = pathInfo.getJsonArray("steps");
                            JsonObject matchedRoute = null;
                            for(int b=0;b<routerRoutes.size() && matchedRoute == null;b++) {
                                JsonArray routeSteps = routerRoutes.getJsonObject(b).getJsonArray("pathSteps");
                                if (pathSteps.size() > routeSteps.size()) {
                                    for (int a = 0; a < routeSteps.size(); a++) {
                                        if ("*".equals(routeSteps.getString(a))) {
                                            if (a == routeSteps.size() - 1) {
                                                // it's the last route step, thus done
                                                matchedRoute = routerRoutes.getJsonObject(b);
                                            }
                                            // otherwise continue checking the next steps
                                        } else if (!pathSteps.getString(a).equals(routeSteps.getString(a))) {
                                            matchedRoute = null;
                                            break;
                                        }
                                    }
                                }
                                else if(pathSteps.size() == routeSteps.size()) {
                                    matchedRoute = routerRoutes.getJsonObject(b);
                                    for (int a = 0; a < routeSteps.size(); a++) {
                                        if ("*".equals(routeSteps.getString(a))) {
                                            // continue checking the next steps
                                        } else if (!pathSteps.getString(a).equals(routeSteps.getString(a))) {
                                            matchedRoute = null;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    // definitely no path match and no wildcard
                                    matchedRoute = null;
                                }
                            }

                            if (matchedRoute == null) {
                                String errorMessage = "Operation " + operation + " in version " + version + " of domain " + domain + " of path " + path + " could not be matched by this router!";
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, "Received invalid message for routing: " + errorMessage);
                                upptMessageImpl.fail(404, errorMessage);
                                return;
                            }


                            // 3. send to router and obtain results
                            Future<JsonObject> resultFuture = Future.future();
                            routeResultFutures.add(resultFuture);
                            send(domain, version, matchedRoute.getString("type"), upptMessageImpl.correlationID(), new JsonObject()
                                            .put("operation", operation)
                                            .put("pathSteps", pathSteps)
                                            .put("pathPredicates", pathInfo.getJsonArray("predicates"))
                                            .put("elementOrigin", elementOrigin)
                                            .put("origin", upptMessageImpl.origin())
                                            .put("userID", upptMessageImpl.userID())
                                            .put("userRoles", upptMessageImpl.userRoles())
                                            .put("value", value == null ? "" : value)
                                    , reply -> {
                                        if (reply.failed()) {
                                            resultFuture.fail(reply.statusCode() + reply.cause().getMessage());
                                            return;
                                        } else if (reply.statusCode() != 200) {
                                            resultFuture.fail("Error code: " + reply.statusCode() + " Message: " + reply.getMessage());
                                            return;
                                        }

                                        JsonObject resultBody = reply.result().getBodyAsJsonObject();
                                        JsonArray paths = resultBody.getJsonArray("paths");
                                        JsonArray values = resultBody.getJsonArray("values");
                                        JsonArray origins = new JsonArray();

                                        if(!body.containsKey("origins")) {
                                            for(int a=0;a<values.size();a++) {
                                                origins.add(serviceName);
                                            }
                                        }
                                        else {
                                            origins = body.getJsonArray("origins");
                                        }

                                        resultFuture.complete(new JsonObject()
                                                .put("domain", domain)
                                                .put("version", version)
                                                .put("path", path)
                                                .put("operation", operation)
                                                .put("values", values)
                                                .put("origins", origins)
                                                .put("paths", paths)
                                                .put("origin", elementOrigin)
                                        );
                                    });


                        }
                        CompositeFuture.all(routeResultFutures).setHandler(routeResults -> {
                            try {
                                if (routeResults.failed()) {
                                    String errorMessage = "Failed to process requests by " + upptMessageImpl.userID() + " from " + upptMessageImpl.origin() + ": " + routeResults.cause().getMessage();
                                    logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.userID(), 500, "Failed to process request: " + errorMessage);
                                    upptMessageImpl.fail(500, errorMessage);
                                    return;
                                }
                                JsonArray results = new JsonArray();
                                for (int i = 0; i < routeResults.result().size(); i++) {
                                    JsonObject result = (JsonObject) routeResults.result().resultAt(i);
                                    if (result == null) {
                                        String errorMessage = "Failed to process requests by " + upptMessageImpl.userID() + " from " + upptMessageImpl.origin() + ": at least one result is empty";
                                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.userID(), 500, "Failed to process request: " + errorMessage);
                                        upptMessageImpl.fail(500, errorMessage);
                                        return;
                                    }
                                    JsonArray currentPaths = result.getJsonArray("paths");
                                    JsonArray currentValues = result.getJsonArray("values");
                                    if (currentPaths == null || currentValues == null || currentValues.size() != currentPaths.size()) {
                                        String errorMessage = "Failed to process requests by " + upptMessageImpl.userID() + " from " + upptMessageImpl.origin() + ": at least one result has no or mismatching paths/values arrays - " + currentValues.size() + " vs " + currentPaths.size();
                                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.userID(), 500, "Failed to process request: " + errorMessage);
                                        upptMessageImpl.fail(500, errorMessage);
                                        return;
                                    }
                                    results.add(result);
                                }
                                upptMessageImpl.getBodyAsJsonObject().put("results", results);

                                // 3. Invoke method with all the results
                                method.invoke(serviceInstance, upptMessageImpl);
                            } catch (IllegalAccessException e) {
                                upptMessageImpl.fail(500, "Internal router error");
                                logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed to invoke route " + method.getName() + " from " + serviceInstance.getClass().getName(), e);
                            } catch (InvocationTargetException e) {
                                upptMessageImpl.fail(500, "Internal router error");
                                logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed to invoke route " + method.getName() + " from " + serviceInstance.getClass().getName(), e);
                            } catch (Exception e) {
                                upptMessageImpl.fail(400, e.getMessage());
                                logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed to invoke route " + method.getName() + " from " + serviceInstance.getClass().getName(), e);
                            }
                        });
                    }
                    // is notification about new/lost session
                    else if(!"webUser".equals(upptMessageImpl.origin()) && body.containsKey("sessionInfo")) {
                        if (body.containsKey("isNewSessionAvailable") && body.getBoolean("isNewSessionAvailable")) {
                            serviceInstance.getClass().getMethod("handleSessionBecameAvailable", JsonObject.class).invoke(serviceInstance, body.getJsonObject("sessionInfo"));
                            upptMessageImpl.reply(200);
                        } else if (body.containsKey("isSessionNowAuthenticated") && body.getBoolean("isSessionNowAuthenticated")) {
                            serviceInstance.getClass().getMethod("handleSessionBecameAuthenticated", JsonObject.class).invoke(serviceInstance, body.getJsonObject("sessionInfo"));
                            upptMessageImpl.reply(200);
                        } else if (body.containsKey("isSessionUnavailable") && body.getBoolean("isSessionUnavailable")) {
                            serviceInstance.getClass().getMethod("handleSessionBecameUnavailable", JsonObject.class).invoke(serviceInstance, body.getJsonObject("sessionInfo"));
                            upptMessageImpl.reply(200);
                        }
                        else {
                            upptMessageImpl.fail(404, "Requested operation unknown");
                        }
                    }
                    // request for user application/session state - probably from a session preflight
                    else if(!"webUser".equals(upptMessageImpl.origin()) && body.containsKey("userID") && body.getBoolean("isSessionStateMissing")) {
                        Future<JsonObject> future = Future.future();
                        future.setHandler(result -> {
                           if(result.failed()) {
                               upptMessageImpl.fail(result.cause().getMessage());
                               return;
                           }
                           upptMessageImpl.reply(result.result());
                        });
                        serviceInstance.getClass().getMethod("processSessionStateMissing", String.class, Future.class).invoke(serviceInstance, body.getString("userID"), future);
                    }
                    else {
                        upptMessageImpl.fail(404, "Requested operation unknown");
                    }
                } catch (Exception e) {
                    message.fail(400, e.getMessage());
                    logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed to invoke route " + method.getName() + " from " + serviceInstance.getClass().getName(), e);
                }
            }
        });

        processorConsumers.add(consumer);

        // publish endpoint as service
        Record record = MessageSource.createRecord(address, address, JsonObject.class, routerRecord);
        serviceDiscovery.publish(record, publishResult -> {
            if (publishResult.failed()) {
                logger.fatal(serviceID, serviceName, 500, "Failed: publishing endpoint " + address + " to service discovery!", publishResult.cause());
            }
        });
        routers.put(address, routerRecord);
    }

    @Customer(
            domain = "bridge",
            version = "1",
            type = "bridgeMissing",
            description = "Notifies about a missing bridge required for a router"
    )
    private void requestRouterBridge(String domain, String version, String type, boolean isInbound, boolean isOutbound, JsonObject authorities) {
        send("bridge", "1", "bridgeMissing", new JsonObject()
                .put("domain", domain)
                .put("version", version)
                .put("type", type)
                .put("isInbound", isInbound)
                .put("isOutbound", isOutbound)
                .put("authorities", authorities), reply -> {
            if(reply.failed()) {
                logger.fatal(serviceID, serviceName, 500, "Failed to request bridge " + domain + "." + version + "." + type, reply.cause());
            }
            else if(reply.statusCode() != 200) {
                logger.fatal(reply.correlationID(), serviceName, 500, "Failed to request bridge " + domain + "." + version + "." + type);
            }
        });
    }

    private void registerRoute(Method method, Route routeAnnotation) {
        String type = "route:" + routeAnnotation.operation().toString() + ":" + routeAnnotation.path();
        String address = routeAnnotation.domain() + "." + routeAnnotation.version() + "." + type;
        String operation = routeAnnotation.operation().toString();

        JsonObject authorities = new JsonObject();
        JsonArray roles = new JsonArray();
        JsonArray users = new JsonArray();

        String[] userRoles = routeAnnotation.authorities().userRoles();
        String[] userIDs = routeAnnotation.authorities().userIDs();
        if(userRoles != null && userRoles.length > 0) {
            for(String role : userRoles) {
                roles.add(role);
            }
        }
        if(routeAnnotation.authorities().isAuthenticationRequired() && roles.isEmpty()) {
            roles.add("authenticated");
        }

        if(userIDs != null && userIDs.length > 0) {
            for(String user : userIDs) {
                users.add(user);
            }
        }
        if(!roles.isEmpty()) {
            authorities.put("roles", roles);
        }
        if(!users.isEmpty()) {
            authorities.put("users", users);
        }


        io.vertx.core.eventbus.MessageConsumer consumer = eventBus.consumer(address);
        consumer.handler(new Handler<io.vertx.core.eventbus.Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                MessageImpl upptMessageImpl;

                try {
                    upptMessageImpl = new MessageImpl(message);
                    JsonObject body = upptMessageImpl.getBodyAsJsonObject();
                    JsonArray pathSteps = body.getJsonArray("pathSteps");
                    JsonArray pathPredicates = body.getJsonArray("pathPredicates");
                    String userID = body.getString("userID");
                    JsonArray userRoles = body.getJsonArray("userRoles");

                    if(!authorities.isEmpty()) {
                        if(userID == null || userID.isEmpty()) {
                            upptMessageImpl.fail(401, "Must provide a valid userID!");
                            return;
                        }

                        boolean isAuthorized = false;

                        if(authorities.containsKey("roles") && userRoles != null) {
                            for (int i = 0; i < userRoles.size(); i++) {
                                JsonArray authorizedRoles = authorities.getJsonArray("roles");
                                if (authorizedRoles.contains(userRoles.getString(i))) {
                                    isAuthorized = true;
                                    break;
                                }
                            }
                        }

                        if(!isAuthorized && authorities.containsKey("users")) {
                            JsonArray authorizedUsers = authorities.getJsonArray("users");
                            if (authorizedUsers.contains(userID)) {
                                isAuthorized = true;
                            }
                        }

                        if(!isAuthorized) {
                            upptMessageImpl.fail(401, "Not authorized to access this route");
                            logger.security(upptMessageImpl.correlationID(), userID, 401, "Unauthorized attempt to access " + address + "!");
                            return;
                        }
                    }

                    String requestedOperation = upptMessageImpl.getBodyAsJsonObject().getString("operation");
                    if(!operation.equals(requestedOperation)) {
                        String errorMessage = "Requested operation " + requestedOperation + " does not match route definition " + operation;
                        upptMessageImpl.fail(404, errorMessage);
                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 404, errorMessage);
                        return;
                    }

                    if(pathSteps == null || pathSteps.isEmpty()) {
                        String errorMessage = "Missing required payload element JsonArray pathSteps: path to the requested data.";
                        upptMessageImpl.fail(400, errorMessage);
                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, errorMessage);
                        return;
                    }
                    if(pathPredicates == null || pathPredicates.isEmpty()) {
                        String errorMessage = "Missing required payload element JsonArray pathPredicates: predicates describing the path to the requested data.";
                        upptMessageImpl.fail(400, errorMessage);
                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, errorMessage);
                        return;
                    }
                    if(pathSteps.size() != pathPredicates.size()) {
                        String errorMessage = "Sizes of JsonArray pathPredicates: and JsonArray pathSteps do no match!";
                        upptMessageImpl.fail(400, errorMessage);
                        logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.origin(), 400, errorMessage);
                        return;
                    }

                    method.invoke(serviceInstance, upptMessageImpl);
                } catch (IllegalAccessException e) {
                    logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed: invoke route " + method.getName() + " from " + serviceInstance.getClass().getName() + " on message", e);
                    message.fail(500, "Illegal access exception");
                } catch (InvocationTargetException e) {
                    logger.error(message.headers().get("correlationID"), serviceName, 500, "Failed: invoke route " + method.getName() + " from " + serviceInstance.getClass().getName() + " on message", e);
                    message.fail(500, "Invocation target exception");
                }
            }
        });

        processorConsumers.add(consumer);

        // convert annotation into JSON Object
        JsonObject routeRecord = new JsonObject()
                .put("description", routeAnnotation.description())
                .put("domain", routeAnnotation.domain())
                .put("version", routeAnnotation.version())
                .put("operation", routeAnnotation.operation())
                .put("type", type)
                .put("category", "Route")
                .put("path", routeAnnotation.path())
                .put("operation", routeAnnotation.operation().toString())
                .put("customers", new JsonArray());

        for(Customer customer : routeAnnotation.customers()) {
            routeRecord.getJsonArray("customers").add(CustomerImpl.toJson(customer));
        }

        // publish endpoint as service
        Record record = MessageSource.createRecord(address, address, JsonObject.class, routeRecord);
        serviceDiscovery.publish(record, publishResult -> {
            if (publishResult.failed()) {
                logger.fatal(serviceID, serviceName, 500, "Failed: publishing endpoint " + address + " to service discovery!", publishResult.cause());
            }
        });
        routes.put(address, routeRecord);
    }

    protected void registerProcessor(JsonObject processorRecord, Method method, Object service) {
        // register endpoint
        String address = processorRecord.getString("domain") + "." + processorRecord.getString("version") + "." + processorRecord.getString("type");
        io.vertx.core.eventbus.MessageConsumer consumer = eventBus.consumer(address);
        consumer.handler(new Handler<io.vertx.core.eventbus.Message<JsonObject>>() {
            @Override
            public void handle(io.vertx.core.eventbus.Message<JsonObject> message) {
                MessageImpl upptMessageImpl;

                try {
                    upptMessageImpl = new MessageImpl(message);
                    JsonObject payload = upptMessageImpl.getBodyAsJsonObject();
                    JsonArray requirements = processorRecord.getJsonArray("requires");
                    if(requirements != null) {
                        for(int i=0;i<requirements.size();i++) {
                            if(!payload.containsKey(requirements.getJsonObject(i).getString("key"))) {
                                String errorMessage = "At address: " + consumer.address() + " missing required payload element " + requirements.getJsonObject(i).getString("type") + " " + requirements.getJsonObject(i).getString("key") + ": " + requirements.getJsonObject(i).getString("description");
                                logger.warn(upptMessageImpl.correlationID(), upptMessageImpl.userID(), 400, "Received invalid message for processing from " + upptMessageImpl.origin() + ": " + errorMessage);
                                upptMessageImpl.fail(400, errorMessage);
                                return;
                            }
                        }
                    }

                    method.invoke(service, upptMessageImpl);
                } catch (IllegalAccessException e) {
                    logger.error(message.headers().get("correlationID"), message.headers().get("userID"), 500, "Failed: invoke " + method.getName() + " from " + service.getClass().getName() + " on message from " + message.headers().get("origin"), e);
                    message.fail(500, "Illegal access exception");
                } catch (InvocationTargetException e) {
                    logger.error(message.headers().get("correlationID"), message.headers().get("userID"), 500, "Failed: invoke " + method.getName() + " from " + service.getClass().getName() + " on message from " + message.headers().get("origin"), e);message.fail(500, "Invocation target exception");
                } catch(IllegalArgumentException e) {
                    logger.error(message.headers().get("correlationID"), message.headers().get("userID"), 500, "Failed: invoke " + method.getName() + " from " + service.getClass().getName() + " on message from " + message.headers().get("origin"), e);message.fail(500, "Invocation argument exception");
                } catch(Exception e) {
                    logger.error(message.headers().get("correlationID"), message.headers().get("userID"), 500, "Failed: invoke " + method.getName() + " from " + service.getClass().getName() + " on message from " + message.headers().get("origin"), e);
                    message.fail(500, "Unknown exception");
                }

            }
        });

        processorConsumers.add(consumer);

        // publish endpoint as service
        Record record = MessageSource.createRecord(address, address, JsonObject.class, processorRecord);
        serviceDiscovery.publish(record, publishResult -> {
            if (publishResult.failed()) {
                logger.fatal(serviceID, serviceName, 500, "Failed: publishing endpoint " + address + " to service discovery!", publishResult.cause());
            }
        });
        // save annotation for later
        processors.put(address, processorRecord);
    }

    private void registerProcessor(Method method, Object service) {
        Processor processorAnnotation = method.getAnnotation(Processor.class);

        if(processorAnnotation == null) {
            Aggregator aggregatorAnnotation = method.getAnnotation(Aggregator.class);
            if(aggregatorAnnotation != null) {
                processorAnnotation = aggregatorAnnotation.processor();
            }
        }

        if(processorAnnotation.enablingConditionKey() != null
                && !processorAnnotation.enablingConditionKey().isEmpty()
                && !"none".equalsIgnoreCase(processorAnnotation.enablingConditionKey())
                && config().containsKey(processorAnnotation.enablingConditionKey())
                && config().getBoolean(processorAnnotation.enablingConditionKey()) == false) {
            logger.info(serviceID, serviceName, 201, "As configured via '" + processorAnnotation.enablingConditionKey() + "', skipping registering processor for " + processorAnnotation.domain() +"."+processorAnnotation.version()+"."+processorAnnotation.type());
            return;
        }

        // convert annotation into JSON Object
        JsonObject processorRecord = new JsonObject()
                .put("description", processorAnnotation.description())
                .put("domain", processorAnnotation.domain())
                .put("version", processorAnnotation.version())
                .put("type", processorAnnotation.type().toString())
                .put("category", "Processor")
                .put("provides", new JsonArray())
                .put("requires", new JsonArray())
                .put("optional", new JsonArray());
        for(Payload provided : processorAnnotation.provides()) {
            processorRecord.getJsonArray("provides").add(PayloadImpl.toJson(provided));
        }
        for(Payload required : processorAnnotation.requires()) {
            processorRecord.getJsonArray("requires").add(PayloadImpl.toJson(required));
        }
        for(Payload optional : processorAnnotation.optional()) {
            processorRecord.getJsonArray("optional").add(PayloadImpl.toJson(optional));
        }

        registerProcessor(processorRecord, method, service);
    }


    public String send(String domain, String version, String type, String origin, String correlationID, JsonObject payload) {
        return send(domain, version, type, null, origin, correlationID, payload, null, null);
    }

    public <R> String send(String domain, String version, String type, String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, null, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, String origin, String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, origin, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, Integer statusCode, String correlationID, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, statusCode, null, correlationID, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, null, null, null, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, Integer statusCode, JsonObject payload, Handler<Message<R>> replyHandler) {
        return send(domain, version, type, statusCode, null, null, payload, null, replyHandler);
    }

    public <R> String send(String domain, String version, String type, Integer statusCode, String origin, String correlationID, JsonObject payload, DeliveryOptions options, Handler<Message<R>> replyHandler) {
        if(payload == null) {
            payload = new JsonObject();
        }
        if(options == null) {
            options = new DeliveryOptions();
        }
        if(correlationID == null) {
            correlationID = UUID.randomUUID().toString();
        }
        if(origin == null) {
            origin = serviceName;
        }
        if(statusCode == null) {
            statusCode = 200;
        }
        String address = domain + "." + version + "." + type;
        String cID = correlationID;

        options.addHeader("statusCode", statusCode.toString())
                .addHeader("correlationID", cID)
                .addHeader("origin", origin);

        if(replyHandler != null) {
            eventBus.send(address, payload, options, handler -> {
                if(handler.failed()) {
                    logger.warn(cID, serviceName, 500, "Failed: sending message to " + address, handler.cause());
                    replyHandler.handle(new MessageImpl(cID, handler.cause()));
                }
                else if(handler.result() == null) {
                    replyHandler.handle(new MessageImpl(cID, new Exception("Reply is null")));
                }
                else {
                    replyHandler.handle(new MessageImpl(handler.result()));
                }
            });
        }
        else {
            eventBus.send(address, payload, options);
        }

        // make sure we know there is at least one customer for this address
         ensureCustomerAwareness(address, domain,version,type,payload);
        return cID;
    }

    public String publish(String domain, String version, String type, JsonObject payload) {
        return publish(domain, version, type, null, null, null, payload, null);
    }

    public String publish(String domain, String version, String type, String statusCode, JsonObject payload) {
        return publish(domain, version, type, statusCode, null, null, payload, null);
    }

    public String publish(String domain, String version, String type, String statusCode, String correlationID, JsonObject payload) {
        return publish(domain, version, type, statusCode, null, correlationID, payload, null);
    }

    public String publish(String domain, String version, String type, String statusCode, String origin, String correlationID, JsonObject payload, DeliveryOptions options) {
        if(payload == null) {
            payload = new JsonObject();
        }
        if(options == null) {
            options = new DeliveryOptions();
        }
        if(correlationID == null) {
            correlationID = UUID.randomUUID().toString();
        }
        if(origin == null) {
            origin = serviceName;
        }

        String address = domain + "." + version + "." + type;
        String cID = correlationID;

        if(statusCode == null || statusCode.isEmpty()) {
            statusCode = "200";
        }
        options.addHeader("statusCode", statusCode);

        options.addHeader("origin", origin);

        options.addHeader("correlationID", cID);

        eventBus.publish(address, payload, options);

        // make sure we know there is at least one customer for this address
        ensureCustomerAwareness(address, domain,version,type,payload);
        return cID;
    }


    private void ensureCustomerAwareness(String address, String domain, String version, String type, JsonObject payload) {
        // make sure we know there is at least one customer for this address
        if(customers.getJsonArray(address) == null) {
            JsonArray addressCustomers = new JsonArray();
            customers.put(address, addressCustomers);

            CustomerImpl customer = new CustomerImpl("Generated automatically", domain, version, type, false, "none");
            if (payload != null) {
                Payload[] provided = new Payload[payload.size()];
                Payload[] required = new Payload[0];
                int counter = 0;

                for (String field : payload.fieldNames()) {
                    Object value = payload.getValue(field);
                    DataType dataType;

                    if (value instanceof String) {
                        dataType = DataType.STRING;
                    } else if (value instanceof JsonObject) {
                        dataType = DataType.JSONObject;
                    } else if (value instanceof JsonArray) {
                        dataType = DataType.JSONArray;
                    } else if (value instanceof Long) {
                        dataType = DataType.LONG;
                    } else if (value instanceof Double) {
                        dataType = DataType.DOUBLE;
                    } else if (value instanceof Integer) {
                        dataType = DataType.INTEGER;
                    } else {
                        dataType = DataType.STRING;
                    }

                    provided[counter] = new PayloadImpl(dataType, field, "", "");
                    counter++;
                }
                customer.setProvides(provided);
                customer.setRequires(required);
            }
            addressCustomers.add(CustomerImpl.toJson(customer));
        }
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        // stop service discovery
        serviceDiscovery.close();

        // stop processing incoming messages
        for(MessageConsumer consumer : processorConsumers) {
            if(consumer != null && consumer.isRegistered()) {
                consumer.unregister();
            }
        }

        // execute the service's own shutdown/stop code
        vertx.executeBlocking(future -> {
            this.shutdown(future);
        }, shutdownResult -> {
            if(shutdownResult.failed()) {
                logger.error(serviceName, 500, "Failed to shutdown service properly!", shutdownResult.cause());
                stopFuture.fail("Failed to shutdown service " + serviceName + " properly!" + shutdownResult.cause());
            }
            else {
                if(database != null) {
                    // close the database connection
                    database.close();
                }
                else {
                    stopFuture.complete();
                }
            }
        });

    }

    @SuppressWarnings("unchecked")
    private <T extends AbstractService> T castClass() {
        return (T) this;
    }

    public abstract void prepare(Future<Object> prepareFuture);
    public abstract void startConsuming();
    public abstract void shutdown(Future<Object> shutdownFuture);
}
