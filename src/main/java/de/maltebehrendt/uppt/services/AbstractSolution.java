package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public abstract class AbstractSolution extends AbstractService {
    @Override
    public void startConsuming() {}

    abstract public void router(Message message);
    abstract public void handleSessionBecameAvailable(JsonObject sessionInfo);
    abstract public void handleSessionBecameAuthenticated(JsonObject sessionInfo);
    abstract public void handleSessionBecameUnavailable(JsonObject sessionInfo);

    abstract public void processSessionStateMissing(String userID, Future<JsonObject> sessionStateFuture);

    public void getAvailableSessionsOfUser(String userID, Future<JsonArray> future) {
        // TODO: use user service
    }
}
