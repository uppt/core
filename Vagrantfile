# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.

  config.vm.define "dev", primary: true, autostart: true do |dev|
    dev.vm.hostname = "upptDev"
    dev.vm.box = "ubuntu/xenial64"
    dev.vm.network "private_network", ip: "192.168.50.5"

    dev.vm.provider "virtualbox" do |vb|
      vb.memory = 3072
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
    end

    dev.vm.provision "shell", inline: $provisioningDev
  end

  config.vm.define "run", autostart: false do |run|
    run.vm.hostname = "upptRun"
    run.vm.box = "ubuntu/xenial64"
    run.vm.network "private_network", ip: "192.168.50.6"
    run.vm.provision "shell", inline: $provisioningRun
  end

  config.vm.define "graylog2", autostart: false do |graylog2|
    graylog2.vm.hostname = "upptGraylog2"
    graylog2.vm.box = "ubuntu/trusty64"
    graylog2.vm.network "private_network", ip: "192.168.50.7"
    graylog2.vm.network :forwarded_port, guest: 80, host: 8080
    graylog2.vm.network :forwarded_port, guest: 12201, host: 12201, protocol: 'udp'
    graylog2.vm.network :forwarded_port, guest: 12201, host: 12201, protocol: 'tcp'

    graylog2.vm.provider "virtualbox" do |vb|
      vb.memory = 2048
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
    end

    graylog2.vm.provision "shell", inline: $provisioningGraylog2
  end

  $provisioningDev = <<SCRIPT
    apt-add-repository ppa:webupd8team/java
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    apt-get -y install git oracle-java8-installer nodejs nodejs-legacy npm
    chmod 1777 /tmp
    su ubuntu
    npm install -g bower
    #npm install -g yo generator-polymer
    npm install -g polymer-cli@next
    exit
SCRIPT

  $provisioningRun = <<SCRIPT
    apt-add-repository ppa:webupd8team/java
    apt-get update
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
    apt-get -y install oracle-java8-installer
SCRIPT

  $provisioningGraylog2 = <<SCRIPT
    apt-get update
    curl -S -s -L -O https://packages.graylog2.org/releases/graylog-omnibus/ubuntu/graylog_latest.deb
    dpkg -i graylog_latest.deb
    rm graylog_latest.deb
    graylog-ctl set-external-ip http://127.0.0.1:8080/api
    graylog-ctl reconfigure
SCRIPT


  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
